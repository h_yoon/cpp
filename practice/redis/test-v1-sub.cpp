

/*
g++ -std=c++17 -o b1sub test-v1-sub.cpp -I./redis-plus-plus/install/include -I./hiredis/install/include  ./redis-plus-plus/install/lib/libredis++.a ./hiredis/install/lib/libhiredis.a -pthread
*/
#include <sw/redis++/redis++.h>
#include <iostream>
using namespace sw::redis;

int main()
{
  ConnectionOptions opts1;
  opts1.host = "127.0.0.1";
  opts1.port = 6379;
  opts1.socket_timeout = std::chrono::milliseconds(100);

  auto redis1 = Redis(opts1);

  auto sub = redis1.subscriber();

  // Set callback functions.
  sub.on_message([](std::string channel, std::string msg)
  {
    std::cout << "on message, channel " << channel << " msg " << msg << std::endl;
  });

  sub.on_pmessage([](std::string pattern, std::string channel, std::string msg) 
  {
    std::cout << "on pmessage, channel " << channel << " msg " << msg << std::endl;
  });

  sub.on_meta([](Subscriber::MsgType type, OptionalString channel, long long num) 
  {
    std::cout << "on meta" << " channel " << channel.value() << " num " << num<< std::endl;
  });

  // Subscribe to channels and patterns.
  sub.subscribe("channel1");    // 1개 채널 구독 등록
  sub.subscribe({"channel2", "channel3"});  // 2개 채널 구독 등록.

  sub.psubscribe("pattern1*");  // 채널이름을 패턴으로 구독 등록

  // Consume messages in a loop.
  while (true) 
  {
    try 
    {
      sub.consume();
      std::cout <<"consume" << std::endl;
    } 
    catch (const Error &err) 
    {
      // Handle exceptions.
      std::cout << "Handle exceptions" << std::endl;
    }
  }
}
