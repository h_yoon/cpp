

/*
 g++ -std=c++17 -o b1pub test-v1-pub.cpp -I./redis-plus-plus/install/include -I./hiredis/install/include  ./redis-plus-plus/install/lib/libredis++.a ./hiredis/install/lib/libhiredis.a -pthread
*/
#include <sw/redis++/redis++.h>
#include <iostream>
#include <unistd.h>
#include <vector>
#include <map>

using namespace sw::redis;

int main()
{
    ConnectionOptions opts1;
    opts1.host = "127.0.0.1";
    opts1.port = 6379;
    opts1.socket_timeout = std::chrono::milliseconds(100);

    auto redis1 = Redis(opts1);

    // Subscribe to channels and patterns.

    const std::string &roomid = "Map1";
    for (int i=0; i<100; ++i)
    {
        const std::string &userid = "Id하이" + std::to_string(i);
        const std::string &value = "유져속성" + std::to_string(i);
        redis1.hset(roomid, userid, value);
    }
    uint32_t sec = 6;
    std::cout << roomid << " set expired after " << sec << "s" << std::endl;
    redis1.expire(roomid, sec);

    std::vector<std::string> keys, vals;
    redis1.hkeys(roomid, std::back_inserter(keys));
    redis1.hvals(roomid, std::back_inserter(vals));

    auto kiter = keys.begin();
    auto viter = vals.begin();
    for (size_t i =0; i < keys.size(); ++i)
    {
        std::cout << *kiter << ":" << *viter << std::endl;
        ++kiter;
        ++viter;
    }
    
    std::unordered_map<std::string, std::string> hashs;
    redis1.hgetall(roomid,std::inserter(hashs, hashs.end()));

    std::cout << "hget ALL" << std::endl;
    for (auto hash : hashs)
        std::cout << hash.first << ":" << hash.second << std::endl;
}
