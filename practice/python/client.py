
from autobahn.asyncio.websocket import WebSocketClientProtocol, WebSocketClientFactory

import asyncio

import wssclient


if __name__ == "__main__":
	login_mesg =  ("{\"from\":\"1_zzz1201_04\", "
									"\"sequence\":1729666609832001111, "
									"\"index\":0, "
									"\"next\":false, "
									"\"path\":\"/v1/login\", "
									"\"direction\":\"request\", "
									"\"payload\":{ "
									"\"pno\":\"zzz1201\", "
									"\"fmc-ext\":\"*1*57718-04\", "
									"\"passwd\":\"9Hqz4teGFcJ30R/ZxTolfQ==\""
								"}}")

	create_room_mesg = ("{\"from\":\"1_zzz1201_04\", "
											"\"sequence\":1729666609832001111, "
											"\"index\":0, "
											"\"next\":false, "
											"\"path\":\"/v1/chat/room/create\", "
											"\"direction\":\"request\", "
											"\"payload\":{ "
													"\"type\":\"crc\", " 
													"\"message-id\": 0, "
													"\"room-id\": 0, "
													"\"sender\": \"zzz1201\"," 
											
													"\"group\": true, "
													"\"sender-name\":\"홍길동 과장\","
													"\"members\":[\"zzz1201\", \"zzz1234\", \"zzzinique\", \"zzzzz759\"], "
													"\"room-name\":\"그룹채팅방이름\","
													"\"message\": \"안녕하세요~~~\","
													"\"createdAt\": \"2024-09-26T12:42:41.384Z\""
											"}}")
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('--ssl', dest="usessl", help="use ssl.", action='store_true')
	parser.add_argument('--debug', dest="debug", help="turn on debugging.", action='store_true')
	parser.add_argument('address', help="address", default="192.168.3.180", nargs="?")
	parser.add_argument('port', help="port", default=59600, nargs="?")
	args = parser.parse_args()

	loop = asyncio.get_event_loop()
	client = Client(retry=True, loop=loop)
	time_value = 0

	def RecvHandler(msg):
		print("text recv", msg)
		global time_value
		doc = parse_json(msg)
		if doc["code"] == 200 :
			print(doc["code"])
			if time_value == 0:
				time_value = 1
			elif time_value == 1:
				time_vlue = 2
		
		if time_value == 1:
			client.sendTextMsg(create_room_mesg)
			time_value = 2

	def binaryHandler(msg):
		print("bin recv", msg)

	def opened():
		print("connected")
		client.sendTextMsg(login_mesg)

		#client.sendTextMsg("{\"from\":\"01-userid11-01\", \"sequencce\":1729666609832001111, \"index\":0, \"next\":false, \"path\":\"/v1/login\", \"direction\":\"request\", \"payload\":{ \"type\":\"log\", \"room-id\":0, \"sender\":\"userid11\", \"receiver\":\"\", \"device-name\":\"dev-test-client\"}")

#packet
#client.sendTextMsg("{'foo' : 'bar'}")

	def closed():
		print("connection closed")

	client.debug = args.debug
	
	client.setRecvHandler(RecvHandler)
	client.setBinaryHandler(binaryHandler)
	client.setOpenHandler(opened)
	client.setCloseHandler(closed)
	
	client.connectTo(args.address, args.port, useSsl=args.usessl)

	loop.run_forever()
	