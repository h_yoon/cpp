# client 연동 준비.
# wssclient.py : address, port 설정.
# protocol.py : self.applyMask = False  #마스크 처리 해제... 
# run : python3 wssclient.py --ssl  # 실행시 ssl 옵션 추가.

from autobahn.asyncio.websocket import WebSocketClientProtocol, WebSocketClientFactory

import asyncio

import json
import ssl
import traceback, sys
import base64
import argparse

import json

def parse_json(json_str):
	parsed_data = json.loads(json_str)
	# payload 내부도 JSON 형식이므로 다시 파싱
	if "payload" in parsed_data:
			try:
					parsed_data["payload"] = json.loads(parsed_data["payload"])
			except json.JSONDecodeError:
					pass  # 
	return parsed_data

class DebugPrinter:
	def __init__(self, debug = False):
		self.debug = debug

	def print_debug(self, msg):
		if self.debug:
			print(msg)

class ReconnectAsyncio(DebugPrinter):

	def __init__(self, retry = False, loop=None, debug = False):
		DebugPrinter.__init__(self, debug)
		self.address = None
		self.retry = retry
		self.loop = loop

		if not loop:
			self.loop = asyncio.get_event_loop()

	def _connect(self):
		raise Exception("_connect() is an abstract class method.  You must implement this")

	def _do_connect(self):		
		if self.retry:
			self.loop.create_task(self._connect_retry())
		else:
			self.loop.create_task(self._connect_once())
		
	#@asyncio.coroutine
	async def _connect_once(self):
		try:
			#yield from self._connect()
			await self._connect()

		except ConnectionRefusedError:
			self.print_debug("connection refused ({})".format(self.address))
 
		except OSError:
			self.print_debug("connection failed ({})".format(self.address))
			
		except:
			exc_type, exc_value, exc_traceback = sys.exc_info()
			traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
			traceback.print_exception(exc_type, exc_value, exc_traceback,
					file=sys.stdout)
			self.print_debug("connection failed ({})".format(self.address))

	#@asyncio.coroutine
	async def _connect_retry(self):
		timeout = 5
		maxtimeout = 60

		while True:
			try:
				self.print_debug("connecting...")
				#yield from self._connect()
				await self._connect()

				self.print_debug("connected!")
				return

			except ConnectionRefusedError:
				self.print_debug("connection refused ({}). retry in {} seconds...".format(self.address, timeout))
				#yield from asyncio.sleep(timeout)
				await asyncio.sleep(timeout)
				if timeout < maxtimeout:
					timeout += 2

				continue

			except OSError:
				self.print_debug("connection failed ({}). retry in {} seconds...".format(self.address, timeout))
				#yield from asyncio.sleep(timeout)
				await asyncio.sleep(timeout)

				if timeout < maxtimeout:
					timeout += 2

				continue

			except:
				exc_type, exc_value, exc_traceback = sys.exc_info()
				traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
				traceback.print_exception(exc_type, exc_value, exc_traceback,
						file=sys.stdout)
				self.print_debug("connection failed ({})".format(self.address))

class Client(ReconnectAsyncio):

	def __init__(self, retry=False, loop = None):
		ReconnectAsyncio.__init__(self, retry=retry)

		if not loop:
			loop = asyncio.get_event_loop()

		self.retry = retry
		self.loop = loop
		self.handle = None
		self.debug = False
		self.binaryHandler = None
		self.textHandler = None
		self.openHandler = None
		self.closeHandler = None
		self.client = None

	def connectTo(self, addy, port, useSsl = True, url=None, protocols=None):
		ws = "ws"
		self.address = addy
		self.port = port
		self.useSsl = useSsl
		
		self.sslcontext = None

		if useSsl:
			ws = "wss"
			self.sslcontext = ssl.SSLContext(ssl.PROTOCOL_SSLv23)

		if url:
			self.wsaddress = url
		else:
			self.wsaddress = "{0}://{1}:{2}".format(ws, addy, port)

		self.print_debug("connectTo: " + self.wsaddress)

		self.factory = WebSocketClientFactory(self.wsaddress, protocols=protocols)
		self.factory.client = self
		self.factory.protocol = MyClientProtocol

		MyClientProtocol.onCloseHandler = self.onClose

		self._do_connect()

	def _connect(self):
		return self.loop.create_connection(self.factory, self.address, self.port, ssl=self.sslcontext)

	def setBinaryHandler(self, binaryHandlerCallback):
		self.binaryHandler = binaryHandlerCallback

	def setRecvHandler(self, textHandlerCallback):
		self.textHandler = textHandlerCallback

	def setOpenHandler(self, openHandlerCallback):
		self.openHandler = openHandlerCallback

	def setCloseHandler(self, closeHandlerCallback):
		self.closeHandler = closeHandlerCallback

	def sendTextMsg(self, msg, encoding='utf-8'):
		self.sendMessage(msg.encode(encoding), False)

	def sendBinaryMsg(self, msg):
		self.sendMessage(msg, True)

	def sendMessage(self, msg, isBinary=False):
		if not self.client:
			return
		self.client.sendMessage(msg, isBinary)

	def onClose(self, wasClean, code, reason):
		if self.retry:
			self._do_connect()

	def close(self, code=WebSocketClientProtocol.CLOSE_STATUS_CODE_NORMAL):
		if self.client:
			self.client.sendClose(code=code)

	def registerClient(self, clientHndl):
		self.client = clientHndl
		self.client.onCloseHandler = self.onClose
		self.client.binaryHandler = self.binaryHandler
		self.client.textHandler = self.textHandler

		try:
			if self.openHandler:
				self.openHandler()
		except:
			exc_type, exc_value, exc_traceback = sys.exc_info()
			traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
			traceback.print_exception(exc_type, exc_value, exc_traceback,
			      limit=6, file=sys.stdout)

	@property
	def connected(self):
		if self.client:
			return self.client.connected
	


class MyClientProtocol(WebSocketClientProtocol):
	
	def __init__(self):
		WebSocketClientProtocol.__init__(self)
		self.binaryHandler = None
		self.textHandler = None
		self.onCloseHandler = None
		self.onMessageHandler = None
		self.connected = False
		
	def onConnect(self, response):
		self.factory.client.print_debug("Server connected: {0}".format(response.peer))

	def onOpen(self):
		self.connected = True
		self.factory.client.print_debug("WebSocket connection open.")
		self.factory.client.registerClient(self)


	def onMessage(self, payload, isBinary):
		if isBinary:
			try:
				self.binaryHandler(payload)
			except KeyboardInterrupt:
				raise KeyboardInterrupt()

			except:
				exc_type, exc_value, exc_traceback = sys.exc_info()
				traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
				traceback.print_exception(exc_type, exc_value, exc_traceback,
						limit=6, file=sys.stdout)
		else:
			try:
				if self.textHandler:
					self.textHandler(payload)
			except KeyboardInterrupt:
				raise KeyboardInterrupt()
			except:
				exc_type, exc_value, exc_traceback = sys.exc_info()
				traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
				traceback.print_exception(exc_type, exc_value, exc_traceback,
						limit=6, file=sys.stdout)
		
	def onClose(self, wasClean, code, reason):
		self.connected = False

		if self.onCloseHandler:
			self.onCloseHandler(wasClean, code, reason)



class WssClient:
  
	def __init__(self):
		login_mesg =  ("{\"from\":\"1_zzz1201_04\", "
										"\"sequence\":1729666609832001111, "
										"\"index\":0, "
										"\"next\":false, "
										"\"path\":\"/v1/login\", "
										"\"direction\":\"request\", "
										"\"payload\":{ "
										"\"pno\":\"zzz1201\", "
										"\"fmc-ext\":\"*1*57718-04\", "
										"\"passwd\":\"9Hqz4teGFcJ30R/ZxTolfQ==\""
									"}}")

		create_room_mesg = ("{\"from\":\"1_zzz1201_04\", "
												"\"sequence\":1729666609832001111, "
												"\"index\":0, "
												"\"next\":false, "
												"\"path\":\"/v1/chat/room/create\", "
												"\"direction\":\"request\", "
												"\"payload\":{ "
														"\"type\":\"crc\", " 
														"\"message-id\": 0, "
														"\"room-id\": 0, "
														"\"sender\": \"zzz1201\"," 
												
														"\"group\": true, "
														"\"sender-name\":\"홍길동 과장\","
														"\"members\":[\"zzz1201\", \"zzz1234\", \"zzzinique\", \"zzzzz759\"], "
														"\"room-name\":\"그룹채팅방이름\","
														"\"message\": \"안녕하세요~~~\","
														"\"createdAt\": \"2024-09-26T12:42:41.384Z\""
												"}}")
	
		parser = argparse.ArgumentParser()
		parser.add_argument('--ssl', dest="usessl", help="use ssl.", action='store_true')
		parser.add_argument('--debug', dest="debug", help="turn on debugging.", action='store_true')
		parser.add_argument('address', help="address", default="192.168.3.180", nargs="?")
		parser.add_argument('port', help="port", default=59600, nargs="?")
		args = parser.parse_args()

		loop = asyncio.get_event_loop()
		client = Client(retry=True, loop=loop)
		time_value = 0
		actions = [1,2,3,4,5]
		
		client.debug = args.debug
		
		client.setRecvHandler(self.RecvHandler)
		client.setBinaryHandler(self.binaryHandler)
		client.setOpenHandler(self.opened)
		client.setCloseHandler(self.closed)

	def Start(self):
		self.client.connectTo(self.args.address, self.args.port, useSsl=self.args.usessl)

		self.loop.run_forever()
		
	def RequestProcess(self,document):
		print("hi")


	def ResponseProcess(self,document):
		print("hi")

  
	def RecvHandler(self, msg):
		print("text recv", msg)
		global time_value
		doc = parse_json(msg)
		req_direction = doc["direction"]
		if req_direction == "request":
			RequestProcess(doc)
		elif req_direction == "response":
			ResponseProcess(doc)
		if doc["code"] == 200 :
			print(doc["code"])
			if time_value == 0:
				time_value = 1
			elif time_value == 1:
				time_vlue = 2
		
		if time_value == 1:
			self.client.sendTextMsg(self.create_room_mesg)
			time_value = 2

	def binaryHandler(self, msg):
		print("bin recv", msg)

	def opened(self):
		print("connected")
		self.client.sendTextMsg(self.login_mesg)

		#client.sendTextMsg("{\"from\":\"01-userid11-01\", \"sequencce\":1729666609832001111, \"index\":0, \"next\":false, \"path\":\"/v1/login\", \"direction\":\"request\", \"payload\":{ \"type\":\"log\", \"room-id\":0, \"sender\":\"userid11\", \"receiver\":\"\", \"device-name\":\"dev-test-client\"}")

#packet
#client.sendTextMsg("{'foo' : 'bar'}")

	def closed():
		print("connection closed")

if __name__ == "__main__":
	wss_client = WssClient()
	wss_client.Start()
