
// server cpp
/// 심플한 api 로 구성된 나만의 server
/// 목표: 고성능 tcp https 
// 2021.07.04 hsy

#ifndef H_SERVER_
#define H_SERVER_

#include "Reactor.h"
#include "SslCtx.h"


// #include "common/CThread.h"
#include "common/CLogger.h"
#include "common/CMtxLockGuard.h"
#include "common/StaticConfig.h"

#include <iostream>
#include <string.h> // for bzero
#include <stdio.h>
#include <arpa/inet.h> 
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <deque>


class Server : public CThread
{

public:
  Server() = default;
  virtual ~Server() = default;
  
  bool     init(StaticConfig* static_config);   // create Socket

protected:
  bool reactorInit(const bool &ssl_enable = true);

  virtual bool ReactorInit() = 0;
  virtual void SetConfig() = 0; 
  virtual void run();

  StaticConfig *static_config_ = nullptr;

  std::string   ip_;
  int           port_;

  int           sockfd_;
  int           acceptor_count_;
  int           reactor_count_;

private:
  bool socketInit();
  bool logInit();

  int           sock_max_connection_ = 300;
  CMtxLockGuard waiter_;

  std::deque<std::shared_ptr<Reactor>> reactors_;
  Reactor  reactor_;

  struct sockaddr_in my_addr_;

};

class HttpServer : public Server
{
public:
  HttpServer() = default;
  virtual ~HttpServer() = default;

protected:
  void SetConfig() 
  {
    static_config_->get("HTTP_SERVER_PORT", port_);
    static_config_->get("HTTP_ACCEPTOR_COUNT", acceptor_count_);
    static_config_->get("HTTP_REACTOR_COUNT",  reactor_count_);       
  }

  bool ReactorInit()
  {
    return reactorInit(false);
  }
};

class HttpsServer : public Server
{
public:
  HttpsServer() = default;
  virtual ~HttpsServer() = default;

protected:
  void SetConfig() 
  {
    static_config_->get("HTTPS_SERVER_PORT", port_);
    static_config_->get("HTTPS_ACCEPTOR_COUNT", acceptor_count_);
    static_config_->get("HTTPS_REACTOR_COUNT",  reactor_count_);   
  }

  bool ReactorInit()
  {
    return reactorInit(true);
  }
};

#endif