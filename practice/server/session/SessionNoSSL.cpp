
#include "SessionNoSSL.h"
#include "SslCtx.h"

#include <unistd.h>

size_t SessionNoSSL::read_message_length_ = 512;

bool
SessionNoSSL::init()
{
  return true;
}

int
SessionNoSSL::connect()
{
  return 1;
}

int
SessionNoSSL::disconnect()
{
  return 1;
}

int 
SessionNoSSL::read(std::string &message)
{
  char buf[SessionNoSSL::read_message_length_] = {0,};

  size_t read_length = ::read(fd_sock_, buf, read_message_length_);
  
  ILOG << "Received message("<< read_length << ")\n"<< buf;

  if(read_length)
    message.assign(buf);

  return read_length;
}

int 
SessionNoSSL::write(const int &resultCode, const std::string &message)
{
  std::string msg;

  msg  = "HTTP/1.1 "+ std::to_string(resultCode);
  msg += "\ncontent-length: "+ std::to_string(message.length()+4)+"\r\n";
  msg += "Content-Type: application/json\r\n\r\n";
  msg += message + "\r\n\r\n";

  ILOG << "Write message("<< msg.length() << ")\n"<< message;

  return ::write(fd_sock_, msg.c_str(), msg.length());
}

