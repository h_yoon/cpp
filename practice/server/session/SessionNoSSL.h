// Acceptor.h
// 2021.07.14 hsy


#ifndef H_SessionNoSSL_
#define H_SessionNoSSL_

#include "Session.h"
#include "CSsl.h"

class SslCtx;

class SessionNoSSL : public Session
{
public:
  SessionNoSSL(int fd_sock) : Session(fd_sock) {}
  virtual ~SessionNoSSL() = default;

  bool  init();
  bool  isAccepted() { return accepted_; }
  int   connect();
  int   disconnect();
  int   read(std::string &message);
  int   write(const int &resultCode, const std::string &message);

private:
  bool  inited_   = false;
  bool  accepted_ = false;

  static size_t read_message_length_;
};

#endif