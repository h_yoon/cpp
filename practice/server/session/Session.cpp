

#include "Session.h"
#include "httpProc/Http1Proc.h"
#include "ProcessorFactory.h"
#include "common/CUtil.h"
#include "CStreamLogger.h"

#include <deque>
#include <iostream>

int
Session::request_process(const std::string &recv_message, std::string &send_message)
{
  Http1Proc httpParser;

  if(httpParser.HttpParse(recv_message, httpInfo_) == false)
    return 402;

  std::shared_ptr<Processor> processor = ProcessorFactory::CreateProcessor(httpInfo_);

  if(processor.get() == nullptr)
    return 404;

  return processor->process(send_message);
}
