// Acceptor.h
// 2021.07.14 hsy


#ifndef H_SessionSSL_
#define H_SessionSSL_

#include "Session.h"
#include "CSsl.h"

class SslCtx;

class SessionSSL : public Session
{
public:
  SessionSSL(const int &fd_sock) : Session(fd_sock) {}
  virtual ~SessionSSL() { SSL_free(ssl_ctx_); }

  void  ctxSetter(SslCtx *ctx) { ssl_ctx_maker_ = ctx; }
  bool  init();
  bool  isAccepted() { return accepted_; }
  int   connect();
  int   disconnect();
  int   read(std::string &message);
  int   write(const int &resultCode, const std::string &message);

private:
  
  CSsl  ssl_;
  SslCtx *ssl_ctx_maker_;
  SSL    *ssl_ctx_;
  bool  inited_   = false;
  bool  accepted_ = false;
};

#endif