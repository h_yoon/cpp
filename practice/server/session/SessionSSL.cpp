
#include "SessionSSL.h"
#include "SslCtx.h"

bool
SessionSSL::init()
{
  
  if ( inited_ == false) 
  {
    ssl_ctx_ = ssl_ctx_maker_->create(fd_sock_);
    inited_  = ssl_.Init(fd_sock_, ssl_ctx_);
  }
  return inited_;
}

int
SessionSSL::connect()
{
  init();
  return ssl_.Accept();
}

int
SessionSSL::disconnect()
{
  return ssl_.Clear();
}

int 
SessionSSL::read(std::string &message)
{
  bool ret = ssl_.Read(message);
  ILOG << "Received message("<< message.length() << ")\n"<< message;
  return ret;
}

int 
SessionSSL::write(const int &resultCode, const std::string &message)
{
  std::string msg;

  msg  = "HTTP/1.1 "+ std::to_string(resultCode);
  msg += "\ncontent-length: "+ std::to_string(message.length()+4)+"\r\n";
  msg += "Content-Type: application/json\r\n\r\n";
  msg += message + "\r\n\r\n";  /// '\r\n\r\n' is needed to close 
  
  return ssl_.Write(msg);
}

