#!/bin/sh

sudo tcpdump -nn -ilo host 127.0.0.1 -vv

#[S] Syn
#[.] Ack
#[F] FIN
#[F.] Fin Ack
#[P] PSH (push Data)
#[R] RST (Reset Connection)
