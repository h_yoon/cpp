// CEpoll Object
// 2021.07.10 hsy

#ifndef H_EPOLL_
#define H_EPOLL_

#include <sys/epoll.h>
#include <string.h>
#include <iostream>

#define MAX_EPOLL_EVENTS  1024

class CEpoll
{
public:
  CEpoll(const size_t &max_connection = MAX_EPOLL_EVENTS);
  virtual ~CEpoll() {};
  
  bool    init    ();
  int     wait    (const int &timeout = 100);
  void    copy    (const int &count, struct epoll_event *events);
  int     operator()() { return epoll_fd_; }
  
public:
  bool add         (const int &fd, 
                    const uint32_t &event_type = EPOLLIN | EPOLLOUT | EPOLLERR ); 

  bool add         (const int &fd, 
                    struct epoll_event &event,
                    const uint32_t &event_type = EPOLLIN | EPOLLOUT | EPOLLERR); 
                    /* EPOLLPRI   중요데이터 발생
                     * EPOLLRDHUP 연결종료 또는 half-close 발생
                     * EPOLLERR   에러발생
                     */
                    
  bool ctl         (const int &fd,
                    struct epoll_event &event,
                    const uint32_t &event_type);
                    
  bool remove      (const int &fd, struct epoll_event &event);

private:
  bool   create();
  static void nonblock_setter(const int &fd);

public:
  struct epoll_event& getfd(const int &num);
  struct epoll_event* geteb() { return &events_[0]; }

private:
  size_t max_connection_;     // EPOLL 최대 연결수 
  struct epoll_event events_[MAX_EPOLL_EVENTS];
  int   epoll_fd_;

};

#endif
