

#include "SslCtx.h"
#include "CStreamLogger.h"

#include <iostream>
bool
SslCtx::init()
{
  SSL_library_init();

  SSLeay_add_ssl_algorithms();
  SSL_load_error_strings();

  //meth_ = TLSv1_2_client_method();  /// client method
  //meth_ = TLSv1_2_server_method();  /// server method  // prefer TLS than SSL

      // SSL_CTX_new(TLS_server_method());     /// openssl version > 1.1.1
      // SSL_CTX_new(TLSv1_2_server_method()); /// openssl version < 1.1.1
  ctx_ = SSL_CTX_new(TLS_server_method());  

  SSL_CTX_set_cipher_list(ctx_,
#if OPENSSL_VERSION_NUMBER >= 0x10101000L // tlsv 1.3
                          TLS_DEFAULT_CIPHERSUITES
#endif
                          "AES128-GCM-SHA256:"
                          "ECDHE-RSA-AES128-SHA:"
                          "ECDHE-RSA-AES128-SHA:"
                          "AES256-GCM-SHA384:"
                          "AES256-SHA256:"
                          "ECDHE-RSA-AES128-GCM-SHA256:"
                          "ECDHE-RSA-AES128-SHA256:"
                          "ECDHE-RSA-AES128-SHA:"
                          "AES128-GCM-SHA256:"
                          "AES128-SHA256:"
                          "!CAMELLIA256:"
                          "!CAMELLIA128:"
                          "!CAMELLIA"
                          "HIGH:"
                          "!LOW:"
                          "!MEDIUM:"
                          "!EXP:"
                          "!NULL:"
                          "!aNULL@STRENGTH");

  SSL_CTX_set_options(ctx_,
                      (SSL_OP_ALL & ~SSL_OP_DONT_INSERT_EMPTY_FRAGMENTS) |
                      SSL_OP_NO_SSLv2 |
                      SSL_OP_NO_SSLv3 |
                      SSL_OP_NO_COMPRESSION |
                      SSL_OP_NO_SESSION_RESUMPTION_ON_RENEGOTIATION |
                      SSL_OP_SINGLE_ECDH_USE |
                      SSL_OP_NO_TICKET |
                      SSL_OP_CIPHER_SERVER_PREFERENCE);

  SSL_CTX_set_session_cache_mode(ctx_,
                              SSL_SESS_CACHE_NO_AUTO_CLEAR |
                              SSL_SESS_CACHE_SERVER);

  SSL_CTX_set_mode(ctx_, SSL_MODE_AUTO_RETRY);
  SSL_CTX_set_mode(ctx_, SSL_MODE_RELEASE_BUFFERS);

  EC_KEY *ecdh = EC_KEY_new_by_curve_name(NID_X9_62_prime256v1);
  if (!ecdh)
  {
    ERR_print_errors_fp(stderr);
    SSL_CTX_free(ctx_);
    return NULL;
  }

  SSL_CTX_set_tmp_ecdh(ctx_, ecdh);
  EC_KEY_free(ecdh);

  if ( SSL_CTX_use_certificate_file(ctx_, CERTF, SSL_FILETYPE_PEM ) <= 0 )    // set certificate to be used
  {
    ILOG << "fail SSL_CTX_use_certificate_file" ;
    ERR_print_errors_fp(stderr);
    return false;
  }

  if ( SSL_CTX_use_PrivateKey_file(ctx_, KEYF, SSL_FILETYPE_PEM) <= 0)     // set private key to be used for private communication
  {
    ILOG << "fail SSL_CTX_use_PrivateKey_file" ;
    ERR_print_errors_fp(stderr);
    return false;
  }

  if ( SSL_CTX_check_private_key(ctx_) == 0)
  {
    fprintf(stderr, "Private key does not match the certificate public keyn");
    ILOG << "fail SSL_CTX_check_private_key" ;
    return false;
  }
  return true;
}

SSL* 
SslCtx::create(const int &fd)
{
  SSL *ssl= SSL_new(ctx_);
  
  SSL_set_fd(ssl, fd);    // 연결시 한번만 수행.
  //SSL_set_connect_state(ssl_);  // sets ssl to work in client mode.
  //SSL_connect(ssl_);            // do connect as client
  //SSL_set_accept_state(ssl);     // sets ssl to work in server mode.
  
  return ssl;  
}
