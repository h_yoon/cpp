
// SslCtx.h
// 2021.09.05 hsy


#ifndef H_SslCtx_
#define H_SslCtx_


#include <openssl/ssl.h>
#include <openssl/crypto.h>
#include <openssl/err.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/crypto.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/conf.h>


#include <iostream>

#define CERTF "cert/server.crt"
#define KEYF  "cert/server.key"

class SslCtx
{
public:
  SslCtx()  = default;
  virtual ~SslCtx() = default;

  bool  init();
  SSL*  create(const int& fd);
  
private:
  SSL_CTX   *ctx_ = nullptr;
};


#endif