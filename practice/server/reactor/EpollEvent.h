// EpollEvent.h
// 2021.09.28 hsy

#ifndef EpollEvent_H_
#define EpollEvent_H_

#include <sys/epoll.h>
#include <set>
#include <map>
#include <memory>

class EventHandler;

struct eventCompare
{
  bool operator()(const epoll_event &lhs, const epoll_event &rhs) const
  {
    return lhs.data.fd < rhs.data.fd;
  }
};

using EventQueue = std::map<int, epoll_event>;
using EventMap   = std::map<int, std::shared_ptr<EventHandler>>;

#endif
