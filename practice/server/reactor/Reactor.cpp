
#include "Reactor.h"

#include "common/CStreamLogger.h"
#include "common/CLogger.h"

#include <memory>
#include <iostream>

bool
Reactor::init(const int &server_sock, const bool &ssl_enable)
{
 
  if (acceptor_.init(server_sock)==false)
  {
    ILOG << "Error reactor init";
    return false;      
  }
  
  acceptor_.start();

  if (demuxer_.init() == false)
  {
    ILOG << "Error io_handler init";
    return false;      
  }

  ssl_enable_ = ssl_enable;

  if (ssl_enable == false)
  {
    ILOG << "Disabled SSL";
    return true;
  }

  if (ssl_ctx_.init() == false)
  {
    ILOG << "Error ssl_ctx_ init";
    return false;   
  }
  else
  {
    ILOG << "SSL Enabled";
  }

  return true;
}

int
Reactor::event_receive(EventQueue &reactor_event)
{
  int count = acceptor_.event_receive(reactor_event);

  if (count > 0) 
  {
    for (const auto &iter : reactor_event)
    {
      const epoll_event &event = iter.second;
      const int &fd = event.data.fd;
      std::shared_ptr<EventHandler> event_ptr = std::make_shared<EventHandler>(event, ssl_enable_);

      EventProcessor::HandlerInit(*event_ptr, &ssl_ctx_);

      event_map_[fd] = event_ptr;
 
      if (demuxer_.event_register(fd, event_ptr->event()) == false)
      {
        ILOG << "ERROR event_register fd:" <<fd;
        count--;    /// epoll 등록 실패시 count -= 1
      }

      tps_calculater_.add();
    }

    return count;
  }

  demuxer_.event_receive(reactor_event, event_map_); // event_map_에서 events만 업데이트

  return reactor_event.size();
}

void 
Reactor::run()
{
  gCTpsManager->register_calculater(tps_calculater_);
  
  while (is_run())
  {
    process();
  }
}

void
Reactor::process()
{
  EventQueue local_event = {};
  ActionType epoll_action;
  
  if (event_receive(local_event) == 0) /// new connection count > 0 ? return : next;
  {
    usleep(100);
  } 

  // EventQueue::iterator iter = local_event.begin();
  
  int client_fd;
  std::set<int> remove_fd;

  while(local_event.empty() == false)
  {  
    remove_fd.clear();

    for (const auto &iter: local_event)
    {
      const epoll_event &event = iter.second;
      client_fd = event.data.fd;
      EventHandler *EventHandler = event_map_[client_fd].get();

      if (react(EventHandler, epoll_action)==false)
      {
        remove_fd.emplace(client_fd);
      }
    
      struct epoll_event &event_ref = EventHandler->event();
      
      if (demuxer_.update(client_fd, event_ref, epoll_action) == false)
      {
        ILOG << "ERROR demuxer_.update() fd:" << client_fd 
                                        << "action:"<< epoll_action
                                        << "types:" << event.events;
        close(client_fd);  /// 반영 실패시 소켓 종료, TODO: 추후 handler에서 호출하여 close 할 수 있게 검토
      }
    }

    for (const int &fd : remove_fd)
    {
      local_event.erase(fd);
    }
  }
}

bool
Reactor::react(EventHandler* handler, ActionType &result)
{
  return handler->process(result);
}

