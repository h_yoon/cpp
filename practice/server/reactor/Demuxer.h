// Demuxer.h
// 2021.09.07 hsy

#ifndef H_Demuxer_
#define H_Demuxer_

#include "EventHandler.h"
#include "CEpoll.h"

#include "EpollEvent.h"

#include <deque>
#include <map>
#include <set>

class Demuxer
{
public:

    Demuxer() = default;
    virtual ~Demuxer() = default;
   
    bool init();
    bool update(const int &fd, struct epoll_event &event, const ActionType &action);
    int  event_receive(EventQueue &events, EventMap &event_map);

    bool event_register(const int &fd, struct epoll_event &event);
    bool event_add     (const int &fd, struct epoll_event &event, const uint32_t &type);
    bool event_del     (const int &fd, struct epoll_event &event, const uint32_t &type);
    bool event_ctl     (const int &fd, struct epoll_event &event, const uint32_t &type);
    bool event_remove  (const int &fd, struct epoll_event &event);

private:
    std::set<int> fd_set_ = {};
    CEpoll epoll_;
};

#endif
