// Acceptor.h
// 2021.07.10 hsy

#ifndef _H_ACCEPTOR_
#define _H_ACCEPTOR_

#include "SslCtx.h"
#include "CEpoll.h"
#include "EpollEvent.h"

#include "common/CThread.h"
#include "common/CSpinlock.h"

#include <netinet/in.h>
#include <sys/socket.h>
#include <map>
#include <deque>
#include <iostream>
#include <memory>

class Acceptor : public CThread
{
public:
  Acceptor();
  virtual ~Acceptor() = default;

  bool  init(const int &server_sock);
  int   accept_connection();
  int&  server_sockect() { return server_sock_; }
  int   event_receive(EventQueue &reactor_buffer);

protected:
  virtual void run();

private:
  int    server_sock_ = 0;
  struct sockaddr_in client_addr_;
  socklen_t client_addr_len_ = (socklen_t)sizeof(struct sockaddr_in);

  EventQueue events_;
  CEpoll    epoll_;

  CSpinlock spin_;
  // SslCtx    ssl_ctx_;
};

#endif
