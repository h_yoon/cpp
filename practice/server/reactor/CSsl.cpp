// CSsl.cpp
// 2021.07.17 hsy


#include "CSsl.h"
#include "SslCtx.h"
#include "CStreamLogger.h"

CSsl::CSsl()
{
 
}

int 
CSsl::Clear()
{ 
  /// ILOG << __FILE__ << __LINE__ << " SSL_shutdown" ;
  SSL_shutdown(ssl_);
  SSL_free(ssl_);
  return 1;
}

bool
CSsl::Init(const int& io_handle, SSL *ssl)
{
  // ILOG <<__func__<<io_handle;
 
  client_cert_ = NULL;
  fd_sock_ = io_handle;
  ssl_ = ssl;
  return true;
}

/*
 * return -1: fail
 *         0: retry
 *         1: success
 */ 
int
CSsl::Accept()
{
  // SSL_set_fd(ssl_, fd_sock_);    // 연결시 한번만 수행.

   if (SSL_accept(ssl_) == -1)               // do handshake with client
  {
    //ILOG << " ERROR fail SSL_accept " << fd_sock_ ;
    int ret=0;
    int err = SSL_get_error(ssl_, ret);

    if (err == SSL_ERROR_WANT_READ)
    {
        // ILOG << "SSL_accept ERROR Wait for data to be read:" <<  ERR_error_string(err,NULL);
        /* Wait for data to be read */
        return 0;
    }
    else if (err == SSL_ERROR_WANT_WRITE)
    {
      // ILOG << "SSL_accept ERROR Write data to continue:" << ERR_error_string(err,NULL);
        /* Write data to continue */
        return 0;
    }
    else if (err == SSL_ERROR_SYSCALL)
    {
      ILOG << "SSL_accept ERROR SSL_ERROR_SYSCALL:" << ERR_error_string(err,NULL)<< "ret:"<<ret ;
        /* Hard error */
         return -1;
    }
    else if ( err == SSL_ERROR_SSL)
    {
      ILOG << "SSL_accept ERROR SSL_ERROR_SSL:" << ERR_error_string(err,NULL)<< "ret:"<<ret ;
        /* Hard error */
         return -1;
    }
    else if (err == SSL_ERROR_ZERO_RETURN)
    {
      ILOG << "SSL_accept ERROR  Same as error:" << ERR_error_string(err,NULL);
        /* Same as error */
        return -1;
    }
    else
    {
       ILOG << "SSL_accept ERROR retry: " << ERR_error_string(err,NULL);
       return 0;
        /* Continue */
    }
  }
  
  // client_cert_ = SSL_get_peer_certificate(ssl_);
  // if (client_cert_ != NULL)
  // {
  //   ILOG << " Client has certificate" ;
  // } 
  // else 
  // {
  //  // ILOG << " Client has no certificate" ;
  // }
  return 1;
}

int
CSsl::Read(std::string &message)
{
  char buffer[MAX_RECV_SIZE] = {0,};

  int err = SSL_read(ssl_, buffer, MAX_RECV_SIZE-1);

  if (err <= 0)
  {
    switch (SSL_get_error(ssl_, err) )
    {
      case SSL_ERROR_WANT_READ   :
        err = 0;
        ILOG << "SSL_ERROR_WANT_READ";
        break;
      case SSL_ERROR_WANT_WRITE  :
        err = 0;
        ILOG << "SSL_ERROR_WANT_WRITE";
        break;
      case SSL_ERROR_ZERO_RETURN :    // epoll_in case
        err = -1;
        break;
      case SSL_ERROR_SYSCALL :
        ILOG << "error SSL_ERROR_SYSCALL " << err << ERR_error_string(err,NULL);
        err = -1;
        break;
      case SSL_ERROR_SSL :
        ILOG << "error SSL_ERROR_SSL " << err << ERR_error_string(err,NULL);
        err = -1;
        break;
      default:
        ILOG << "error default " << err << ERR_error_string(err,NULL);
        err = -1;
        break;
    }
    return err;
  }

  message.assign(buffer);

  //ILOG << "SSL_read length("<< message.length() <<")\n"<< message;

  return message.length();
}

/*
 * result > 0 : Success
 *         = 0 : retrysss
 *         < 0  : error
 */
int
CSsl::Write(const std::string& message)
{
  int write_result = SSL_write(ssl_, message.c_str(), message.length() );

  if(write_result <= 0)
  {
    switch (SSL_get_error(ssl_, write_result) )
    {
      case SSL_ERROR_WANT_READ :
      case SSL_ERROR_WANT_WRITE:
         write_result = 0;
         break;
      case SSL_ERROR_ZERO_RETURN :    //ERR, need to delete
      case SSL_ERROR_SYSCALL :
      case SSL_ERROR_SSL :
        ILOG << "Failed Send error_code:"<< write_result;
        write_result = -1;
        break;
      default:
        ILOG << "default case error_code:"<< write_result;
        ERR_print_errors_fp(stderr);
        write_result = -1;
        break;
    }
  }
  // ILOG << "Write result"<< write_result<< message;
  return write_result;
}

