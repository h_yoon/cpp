// Reactor.h
// 2021.07.18 hsy

#ifndef H_REACTOR_
#define H_REACTOR_

//#include "Session.h"
#include "Acceptor.h"
#include "Demuxer.h"
#include "EventHandler.h"
#include "EpollEvent.h"

#include "common/CTpsManager.h"
#include "common/CTpsCalculater.h"
#include "common/CThread.h"
#include "common/CSpinlock.h"
#include "common/CMtxLockGuard.h"

#include <unistd.h>
#include <map>
#include <deque>
#include <iostream>

class Reactor: public CThread
{
public:
  Reactor() = default;
  virtual ~Reactor()	= default;
  bool  init(const int &server_sock, const bool &ssl_enable = true);

protected:
  virtual void run();

private:
  bool  react(EventHandler *handler, ActionType &result);
  void  process();
  int   event_receive(EventQueue &event_local);

private:
  Acceptor  acceptor_;
  Demuxer   demuxer_;
  SslCtx    ssl_ctx_;
  bool      ssl_enable_ = true;

  EventMap  event_map_;
  CTpsCalculater tps_calculater_;
  CMtxLockGuard waiter_;
};

#endif
