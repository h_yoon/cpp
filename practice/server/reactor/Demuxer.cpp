
#include "Demuxer.h"
#include "EpollAction.h"
#include "CStreamLogger.h"
#include <unistd.h>
bool
Demuxer::init()
{
  if (epoll_.init()==false)
    return false;
  
  return true;
}

bool
Demuxer::update(const int &fd, struct epoll_event &event, const ActionType &action)
{
  const uint32_t &types = event.events;
  switch (action)
  {
    case ActionType::DEL_ALL  : {::close(fd); return true; } /// if close(fd), linux automatically deregister itself from epoll.
    case ActionType::DEL_OUT  : return event_del (fd, event, types - EPOLLOUT); 
    case ActionType::CTL_WAIT : return event_ctl (fd, event, EPOLLRDHUP | EPOLLHUP); // | EPOLLIN);
    case ActionType::CTL_IN   : return event_ctl (fd, event, EPOLLIN | EPOLLPRI); 
    case ActionType::CTL_OUT  : return event_ctl (fd, event, EPOLLOUT); 
    case ActionType::CTL_INOUT: return event_ctl (fd, event, EPOLLIN | EPOLLPRI | EPOLLOUT); 
    case ActionType::CTL_RDHUP: return event_ctl (fd, event, EPOLLRDHUP | EPOLLHUP); 
    case ActionType::CTL_WANT : return true; // inter process to retry
    default:
       ILOG << "ERROR need to handle step:" <<action;
    break;
  }
  return false;
}

int
Demuxer::event_receive(EventQueue &events, EventMap &event_map)
{
  int recv_cnt  = 0;
  recv_cnt = epoll_.wait(0);

  for ( int i=0; i<recv_cnt; ++i)
  {
    epoll_event &eve = epoll_.getfd(i);

    int client_fd = eve.data.fd;

    events.emplace(client_fd, eve);

    (event_map[client_fd].get())->update(eve);

  }
  return recv_cnt;
}

// used in acceptor
bool
Demuxer::event_register(const int &fd, struct epoll_event &event)
{
  fd_set_.insert(fd);
  
  return epoll_.add(fd, event, (EPOLLIN));
}

bool
Demuxer::event_add(const int &fd, struct epoll_event &event, const uint32_t &type)
{
  return epoll_.add(fd, event, type);
}

bool
Demuxer::event_del(const int &fd, struct epoll_event &event, const uint32_t &type)
{
  return epoll_.ctl(fd,event,type);
}

bool
Demuxer::event_ctl(const int &fd, struct epoll_event &event, const uint32_t &type)
{
  return epoll_.ctl(fd,event,type);
}

bool
Demuxer::event_remove(const int &fd, struct epoll_event &event)
{
  ILOG << fd <<"remove";
  fd_set_.erase(fd);
  return epoll_.remove(fd,event);
}

