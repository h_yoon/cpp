

#include "Acceptor.h"

#include "Reactor.h"
#include "CEpoll.h"
#include "SessionSSL.h"

#include "CStreamLogger.h"
#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include <string.h>

Acceptor::Acceptor()
{
  memset(&client_addr_,0x0, sizeof(struct sockaddr_in));
}

bool
Acceptor::init(const int &server_sock)
{
  server_sock_ = server_sock; 

  if (epoll_.init() == false)
  {
    ILOG << "fail epoll init";
    return false;   
  }

  if (server_sock_ == 0)
  {
    ILOG << "failed set a server_sock_";
    return false;
  }

  if(epoll_.add(server_sock_,(EPOLLIN|EPOLLPRI) ) == false) 
  {
    ILOG <<"fail epoll add";
    return false;
  }

  return true;
}

int
Acceptor::accept_connection()
{
  int recv_cnt = 0;
  int accepted_cnt = 0;
  recv_cnt = epoll_.wait(-1);
  
  if(recv_cnt < 0)
    return -1;
  
  epoll_event &eve = epoll_.getfd(0);
 
  while(true)
  {
    // new connection
    int client_fd = accept(server_sock_, (struct sockaddr*)&client_addr_, &client_addr_len_);
    if ( client_fd > 0)
    {
      accepted_cnt++; 
      eve.data.fd = client_fd; // change client socket 
      spin_.Lock();
      events_.emplace(client_fd,eve);
      spin_.unLock();
      // ILOG << "new connection " << client_fd;
      continue;
    }
    else if (client_fd < 0 && errno == EWOULDBLOCK) /// no more connection 
    {
      break;
    }
    else
    {
      ILOG << "::accept Error" << errno;
      return -1;
    }
  }
  return accepted_cnt;
}

int
Acceptor::event_receive(EventQueue &reactor_buffer)
{
  spin_.Lock();
  if (events_.size() > 0)
    reactor_buffer.swap(events_);
  spin_.unLock();
  return reactor_buffer.size();
}

void
Acceptor::run()
{
  while(true)
  {
    accept_connection();
  }
}

