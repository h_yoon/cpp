

#ifndef H_EpollAction_
#define H_EpollAction_

struct EpollAction
{
  enum Action 
  {
    ADD_ALL = 0,
    ADD_IN,
    ADD_OUT,  
    ADD_INOUT,
    ADD_ERR,
    DEL_ALL,    // 5
    DEL_IN,     // 6
    DEL_OUT,    // 7
    DEL_INOUT,  // 8
    CTL_IN,     // 9
    CTL_OUT,    // 10
    CTL_INOUT,  // 11
    CTL_ERR,
    CTL_RDHUP,
    CTL_WAIT,   // IN and RDHUP
    CTL_WANT,   // 15
    ACTION_MAX
  };

  enum Step
  {
    INIT = 0,
    CONNECT,
    READ,
    PROCESS,
    WRITE,
    TIME_WAIT,
    CLOSE,
    SHUTDOWN,
    DONE
  };
};

using ActionType   = EpollAction::Action;
using HandlerPhase = EpollAction::Step;
#endif