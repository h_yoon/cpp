#include "Server.h"
#include "MainManager.h"

#include <signal.h>

using namespace std;

int main(int argc, char** args)
{
  MainManager manager;
  signal(SIGPIPE, SIG_IGN);

  if (manager.init() == 0) 
     std::cout <<__FILE__<<__LINE__<< "fail init MainManager" <<std::endl;

  Server *http = new HttpServer();
  Server *https = new HttpsServer();
  
  (void)argc;

  StaticConfig *config = &(manager.getConfig());
 
  if (http->init(config) == 0) 
  {
    std::cout <<__FILE__<<__LINE__<< "fail http server init" <<std::endl;
    return 0;
  }

  if (https->init(config) == 0) 
  {
    std::cout <<__FILE__<<__LINE__<< "fail https server init" <<std::endl;
    return 0;
  }

  http->start();
  https->start();

  std::cout <<__FILE__<<__LINE__<< "START!!!"<< std::endl;
  
  manager.run();
  std::cout <<__FILE__<<__LINE__<< "end"<< std::endl;
  //server->join();
  return 0;
}

