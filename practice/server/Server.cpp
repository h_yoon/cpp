
#include "Server.h"
#include "CStreamLogger.h"

bool
Server::init(StaticConfig* static_config)
{

  if(static_config == nullptr)
    return false;
  
  static_config_ = static_config;

  SetConfig();

  static_config_->get("SERVER_IP", ip_);

  if (socketInit() == false)
  {
    std::cout <<  "fail socketInit() " << ip_ <<":"<< port_ << std::endl;
    return false;
  }

  ReactorInit();

  return true;
}

bool 
Server::reactorInit(const bool &ssl_enable)
{
  for (int i=0; i<reactor_count_; ++i) 
  {
    std::shared_ptr<Reactor> reactor = std::make_shared<Reactor>();

    if (reactor->init(sockfd_, ssl_enable) == false)
    {
      std::cout <<  "fail reactor.init()" << std::endl;
      return false;
    }
    
    reactor->start();
    reactors_.emplace_front(reactor);
  }

  return true;
}

void 
Server::run()
{
  while (is_run())
  {
    waiter_.wait_ms(1000);
  }
  return;
}

bool 
Server::socketInit()
{
  sockfd_ = socket(AF_INET, SOCK_STREAM, 0); // address type, tcp

  int opt = 1;

  if (setsockopt(sockfd_, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int)) < 0) 
  {
    std::cout << "failed setsockopt" <<std::endl;
    return false;
  }
  
  my_addr_.sin_family = AF_INET;       // host byte order 
  my_addr_.sin_port   = htons(port_);  // network byte order
  my_addr_.sin_addr.s_addr = inet_addr(ip_.c_str());
  memset(&(my_addr_.sin_zero), 0x00, sizeof(my_addr_.sin_zero)); /* zero the rest of the struct */
  //bzero(&(my_addr.sin_zero),8);

  if (::bind(sockfd_, (struct sockaddr*) &my_addr_, sizeof(my_addr_)) < 0) 
  {
    if ( errno == 99 )
      std::cout << "failed bind, check ip " << errno  << std::endl;  // errno 99: ip may not own ip.
    else
      std::cout << "failed bind " << errno  << std::endl;
    
    return false;
  }

  if (::listen(sockfd_, sock_max_connection_) < 0)
  {
    std::cout << "failed listen:" << errno << "fd:" << sockfd_ << "backlog:" << sock_max_connection_ << std::endl;
    return false;
  }

  int io_flag = fcntl(sockfd_, F_GETFL, 0);

  if (fcntl(sockfd_, F_SETFL, io_flag | O_NONBLOCK) == -1)
    std::cout << "Error non-block setting"<< sockfd_ << std::endl;

  ILOG << "server init fd:" << sockfd_ << "port:" << port_ << "backlog:" << sock_max_connection_;
  
  return true;
}

