
// ProcessorFactory.h
// 2022.08.19 hsy

#ifndef _H_PROCESSOR_FACTORY_ 
#define _H_PROCESSOR_FACTORY_

#include "Processor.h"
#include "HttpInfo.h"
#include <memory>

class ProcessorFactory
{
public:
  static std::shared_ptr<Processor> CreateProcessor(HttpInfo &httpInfo)
  {
    std::string method = httpInfo.method();
    std::string url = httpInfo.url();
    
    std::shared_ptr<Processor> ptr = nullptr;

    if(method.compare("POST") == 0)
    {
      if(url.compare("/test") == 0)
      {
        ptr = std::make_shared<Processor>();
      }
    }
    else if(method.compare("GET") == 0)
    {
      if(url.compare("/test") == 0)
      {
        ptr = std::make_shared<Processor>();
      }
    }
    else if(method.compare("PUT") == 0)
    {
      
    }
    else
    {

    }
    return ptr;
  }
  
};

#endif