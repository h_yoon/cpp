// EventHandler.h
// 2021.09.08 hsy

#ifndef H_EventHandler_
#define H_EventHandler_

#include "EventProcessor.h"
#include "CEpoll.h"
#include <utility>
#include "CStreamLogger.h"

class EventHandler : public EventProcessor
{
public:
  using epollEvent = struct epoll_event;

public:
  EventHandler(const epollEvent &event, 
               const bool       &ssl_enable = true): EventProcessor(event.data.fd, ssl_enable), // 상속 먼저 초기화
                                                     fd_event_(event) 
  {
    event_fd_    = fd_event_.data.fd;
    event_types_ = fd_event_.events;
  }
  virtual ~EventHandler() = default;

public:
  void init();
  struct epoll_event& event() { return fd_event_; }
  void update(const epollEvent &event) 
  {
    fd_event_ = event;
    event_fd_    = fd_event_.data.fd;
    event_types_ = fd_event_.events;
  }
  bool process(ActionType &result);

protected:
  int      event_fd_;
  uint32_t event_types_;
  struct epoll_event fd_event_;
 
};

#endif

