// EventProcessor.cpp
// 2021.09.08 hsy

#include "EventProcessor.h"

#include "SslCtx.h"
#include "CStreamLogger.h"
#include <memory>
#include <sys/socket.h> /// shutdown()

EventProcessor::EventProcessor(const int &fd, const bool &ssl_enable) 
{
  fd_sock_ = fd;
  
  if (ssl_enable == true)    
    eventHandlerInit = &EventProcessor::HttpsHandlerInit;
  else              
    eventHandlerInit = &EventProcessor::HttpHandlerInit;
} 

void
EventProcessor::HttpHandlerInit(SslCtx *sslCtx)
{
  sslCtx = nullptr; // not used
  std::shared_ptr<SessionNoSSL> session_no_ssl;

  session_no_ssl = std::make_shared<SessionNoSSL>(fd_sock_);
  session_ = std::static_pointer_cast<Session>(session_no_ssl);
}

void
EventProcessor::HttpsHandlerInit(SslCtx *sslCtx)
{
  std::shared_ptr<SessionSSL> session_ssl;

  session_ssl = std::make_shared<SessionSSL>(fd_sock_);
  session_ssl->ctxSetter(sslCtx);
  session_ = std::static_pointer_cast<Session>(session_ssl);
}

/* return
 * true : 성공  action:
 * false: 실패 or 재시도
 */
bool
EventProcessor::handle_connect()
{
  // ILOG;
  int connection = session_->connect();
  bool result = false;

  switch (connection)
  {
    case 1:
    {
      action_        = ActionType::CTL_WANT;        //상태 변화없음
      handler_phase_ = HandlerPhase::READ;          //연결 성공
      result = true;
      break;
    }
    case 0:
    {
      action_ = ActionType::CTL_WANT;  //상태 유지, 재시도
      result = false;                   //queue 제거
      break;
    }
    default:
    {
      action_        = ActionType::CTL_RDHUP; // 연결 실패
      handler_phase_ = HandlerPhase::SHUTDOWN; 
      result = false;                   //queue 제거
      break;
    }
  }

  return result;
}

bool
EventProcessor::handle_read()
{
  // ILOG;
  int result = session_->read(recv_message_);
  if (result > 0)
  {
    action_        = ActionType::CTL_WANT;   //상태 유지
    handler_phase_ = HandlerPhase::PROCESS;  //연결 성공
    return true;                             //queue 
  }
  else if (result == 0)
  {
    action_ = ActionType::CTL_WANT;  //상태 유지, 재시도
    return false;                   //queue 제거
  }
  else
  {
    action_ = ActionType::CTL_RDHUP;   //리드 실패, 출력스트림 제거.
    handler_phase_ = HandlerPhase::SHUTDOWN; 
    //ILOG << "READ result Fail" << result;
    return false;                  //queue 제거
  }
}

bool
EventProcessor::handle_process()
{
   //ILOG;
  result_code_ = session_->request_process(recv_message_, send_message_);
  action_        = ActionType::CTL_OUT;    //출력상태로 변경
  handler_phase_ = HandlerPhase::WRITE;    //처리 성공
  return false;                            //queue제거

  // {
  //   action_        = ActionType::DEL_OUT;   //처리 실패, 출력스트림 제거
  //   handler_phase_ = HandlerPhase::SHUTDOWN; 
  //   return false;                   //queue 제거
  // } 
}

bool
EventProcessor::handle_write()
{
  // ILOG;
  int result = session_->write(result_code_, send_message_);

  if (result > 0)
  {
    action_ = ActionType::CTL_WAIT;
    handler_phase_ = HandlerPhase::TIME_WAIT;
    timer_key_ = gObjectTimer->add(1,(TimerObject*)this);

    return false;                   //false: queue제거 epoll_ctl 수행
  }
  else if (result == 0)
  {
    ILOG << "Write retry";

    action_ = ActionType::CTL_WANT; //재시도 필요, 상태 변화없음
    return true;                   //false: queue제거 epoll_ctl 수행
  }
  else
  {
    ILOG << "Write Error, Out delete";

    action_= ActionType::CTL_RDHUP;   //쓰기 실패, 출력스트림 제거
    handler_phase_ = HandlerPhase::SHUTDOWN; 

    return false;                   //false: queue제거 epoll_ctl 수행
  }
}

bool
EventProcessor::handle_timeout() // 미완성, 호출 하는곳 없음.
{
 // ILOG;

  action_        = ActionType::CTL_IN;
  handler_phase_ = HandlerPhase::READ; //origin
  // gObjectTimer->add(1,(TimerObject*)this);

  return false;    //false: queue제거 epoll_ctl 수행
}

bool
EventProcessor::handle_shutdown()
{
  //ILOG;

  action_ = ActionType::CTL_RDHUP;
  handler_phase_ = HandlerPhase::CLOSE;

  ::shutdown(fd_sock_,SHUT_RD);

  return false;                      //false: queue 제거
}


bool
EventProcessor::handle_close()
{
  //ILOG;

  action_ = ActionType::DEL_ALL; ///  DEL_ALL: just close, epoll will automatically deregitser itself from epoll.

  return false;                      //false: queue제거 epoll_ctl 제거
}

