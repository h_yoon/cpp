// EventProcessor.h
// 2021.09.08 hsy

#ifndef H_EventProcessor_
#define H_EventProcessor_

#include "EpollAction.h"
#include "SessionSSL.h"
#include "SessionNoSSL.h"
#include "ObjectTimer.h"
#include <map>
#include <unistd.h>
#include <memory>

class SslCtx;

class EventProcessor : public TimerObject
{
public:
  EventProcessor(const int &fd, const bool &ssl_enable = true);
  virtual ~EventProcessor() 
  {
    if (timer_key_) 
      gObjectTimer->remove(timer_key_); 
  }

public:
  void (EventProcessor::*eventHandlerInit)(SslCtx *sslCtx); // used function pointer.
  static void HandlerInit(EventProcessor &proc, SslCtx *sslCtx) { (proc.*proc.eventHandlerInit)(sslCtx); }  // used case of function pointer

  bool  handle_connect();
  bool  handle_read();
  bool  handle_write();
  bool  handle_process();
  bool  handle_timeout();
  bool  handle_shutdown();
  bool  handle_close();

  virtual void timeout() 
  {
    handle_close();
  }

  void getAction(ActionType &action) { action = action_; }

private:
  void  HttpHandlerInit (SslCtx *sslCtx);
  void  HttpsHandlerInit(SslCtx *sslCtx);

protected:
  HandlerPhase  handler_phase_ = HandlerPhase::CONNECT;

private:
  ActionType action_;

  std::shared_ptr<Session> session_;
  std::string recv_message_;
  std::string send_message_;

  bool  connected_ = false;

  int   fd_sock_ = 0;
  int   result_code_ = 0;
  int   timer_key_ = 0;
};

#endif
