
#include "EventHandler.h"
#include "CStreamLogger.h"
void
EventHandler::init()
{
}

/* return 
 *  true : queue에 유지,
 *  false: queue에서 제거후, epoll 재 등록
 */
bool
EventHandler::process(ActionType &epoll_action)
{
  bool remain = false;

  switch (handler_phase_)
  {
    case HandlerPhase::CONNECT:
      remain = handle_connect();
    break;
    case HandlerPhase::READ:
      remain = handle_read();   // no break
    case HandlerPhase::PROCESS:
      remain = handle_process();
    break;
    case HandlerPhase::WRITE:
      remain = handle_write();
    break;
    case HandlerPhase::TIME_WAIT: // response 완료후 다음 동작 대기 (연결종료/송신대기)
    {
      switch (fd_event_.events) 
      {
        case EPOLLIN: {
          remain = handle_read();     // 그래서 EPOLLIN은 뺐고, 서버는 response후 EPOLLRDHUP을 설정해서 세션을 종료한다. -> 현재는 하나의 연결로 여러개 요청 전송 불가. Head Of Lock때문에 http1.1에서는 안됨
        }                             
        break;
        case EPOLLRDHUP :
        case EPOLLHUP : 
          remain = handle_close();
        break;
      }
    }
    break;
    case HandlerPhase::CLOSE:
      remain = handle_close();
    break;
    case HandlerPhase::SHUTDOWN:
      remain = handle_shutdown();
    break;
    default:
    break;
  }

  getAction(epoll_action);
  // ILOG  << "phase-"            << handler_phase_ 
  //       << "keep_in_queue-"    << remain==true?"yes":"no"
  //       << "next_epoll_action-"<< epoll_action
  //       << "current_types-"    << types;
  return remain;
}
