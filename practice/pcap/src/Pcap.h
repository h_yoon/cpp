
#ifndef PCAP_H_
#define PCAP_H_

#include "CaptureNic.h"
#include "CaptureInterface.h"

#include <pcap.h>
#include <string>
#include <memory>

class Pcap;
using PcapPtr = std::shared_ptr<Pcap>;

class Pcap
{
public:
  Pcap() = default;
  virtual ~Pcap() = default;

  bool init(const CaptureInfo &info, CaptureResult &error_code);
  void capture();
  int32_t close();

private:
  pcap_t        *handle_ = nullptr;
  pcap_dumper_t *dumper_ = nullptr;
  struct bpf_program dump_fp_;

  std::string pcap_file_name_;

  uint64_t packet_count_ = 0;
};

#endif /* PCAP_H_  */