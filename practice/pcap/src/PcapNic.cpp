
#include "PcapNic.h"
#include "CaptureNic.h"

#include <deque>
#include <iostream>
#include <arpa/inet.h>
#include <ctime>
#include <cctype>     // for std::toupper
#include <algorithm>  // for std::trasnform

/// pcap 을 위한 인터페이스 리스트업.
bool 
PcapNic::lookup_network_interface(std::map<std::string, NicInfo> &nic_list)
{
  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_if_t *alldevs;

  // Find all network devices
  if (pcap_findalldevs(&alldevs, errbuf) == -1) 
  {
    E_LOG << "Couldn't find default device: " << errbuf << std::endl;
    return false;
  }

  // Select the first device
  if (alldevs == nullptr) 
  {
    E_LOG << "No devices found" << std::endl;
    return false;
  }

  for (auto &dev = alldevs; dev != NULL; dev = dev->next)
  {
    for (pcap_addr_t *addr = dev->addresses; addr != nullptr; addr = addr->next) 
    {
      if (addr->addr->sa_family != AF_INET) // IPv4 주소만 처리
        continue;

      char ip_str[INET_ADDRSTRLEN];
      struct sockaddr_in *sockaddr_ipv4 = (struct sockaddr_in *)addr->addr;
      inet_ntop(AF_INET, &(sockaddr_ipv4->sin_addr), ip_str, INET_ADDRSTRLEN);

      bpf_u_int32 localnet, netmask;
      if (pcap_lookupnet(dev->name, &localnet, &netmask, errbuf) == -1)
      {
        E_LOG << "Couldn't get netmask for device " << dev->name << ": " << errbuf << std::endl;
        continue;
      }
      
      NicInfo nic;
      nic.if_name  = dev->name;
      nic.ip_str   = ip_str;
      nic.localnet = localnet;
      nic.netmask  = netmask;
      
      I_LOG << nic.if_name << " " << nic.ip_str << std::endl;

      nic_list[nic.if_name] = nic;
    }
  }
  return (nic_list.size() > 0);
}

/// 요청된 정보 유효성 검사. IP, PORT 수행
bool 
PcapNic::validate_info(const std::string &src_ip,
                       const std::string &dst_ip,
                       const std::string &direction,
                       CaptureResult     &error_code)
{
  struct sockaddr_in sa;
  error_code = CaptureResult::OK;

  if (src_ip != "" && (src_ip == dst_ip))
  {
    E_LOG << "src_ip same with a dst_ip " << src_ip << " " << dst_ip << std::endl;
    error_code = CaptureResult::INVALID_IP;
    return false;
  }

  if (src_ip != "")
  {
    if (inet_pton(AF_INET, src_ip.c_str(), &(sa.sin_addr)) != 1)
    {
      E_LOG << "Invalid src ip " << src_ip << std::endl;
      error_code = CaptureResult::INVALID_IP;
      return false;
    }
  }

  if (dst_ip != "")
  {
    if (inet_pton(AF_INET, dst_ip.c_str(), &(sa.sin_addr)) != 1)
    {
      E_LOG << "Invalid dst ip " << dst_ip << std::endl;
      error_code = CaptureResult::INVALID_IP;
      return false;
    }
  }

  if (direction == "")
    return true;

  const std::string &dir = PcapNic::to_upper_case(direction);
  if ((dir != "IN") && (dir != "OUT") && (dir != "INOUT"))
  {
    E_LOG << "Invalid direction '" << direction << "'" << std::endl;
    error_code = CaptureResult::INVALID_DIRECTION;
    return false;
  }
  
  return true;
}

/// 필터 조합 
std::string 
PcapNic::get_filter_exp(const CaptureInfo &info)
{
/*  "src host 192.168.1.1 and dst host 192.168.1.2 and "
    "tcp src portrange 1000-2000 and "
    "tcp dst portrange 3000-4000 "; */

  std::deque<std::string> filters = {get_filter_host("src", info.src_ip), 
                                     get_filter_host("dst", info.dst_ip),
                                     get_filter_port("src", info.protocol, info.src_port_begin, info.src_port_end),
                                     get_filter_port("dst", info.protocol, info.dst_port_begin, info.dst_port_end)};

  std::string filter;
  for (uint64_t i = 0; i < filters.size(); ++i)
  {
    if (filters[i] == "")
      continue;
    
    if (filter.length() > 0)
      filter += " and ";
    
    filter += filters[i];
  }

  return filter;
}

std::string 
PcapNic::get_filter_host(const std::string &location,
                         const std::string &host_ip)
{
  if (host_ip == "")
    return "";
    
  return location + " host " + host_ip;
}

/// 포트 필터 생성
std::string 
PcapNic::get_filter_port(const std::string &location,
                         const std::string &protocol,
                         const uint16_t    &port_begin, 
                         const uint16_t    &port_end)
{
  uint16_t end_port = port_end;
  if ((port_end   <= port_begin && port_end > 0)||  // begin 5000, end 3000
      (port_begin == 0          && port_end > 0))   // begin 0   , end 3000
  {
    I_LOG << "Change " << location << " end_port " << port_end << " -> 0" << std::endl;
    end_port = 0;
  }

  std::string port_str;
  if (port_begin > 0 && end_port > 0)
    port_str = "portrange " + std::to_string(port_begin) + "-" + std::to_string(end_port);
  else if (port_begin > 0)
    port_str = "port " + std::to_string(port_begin);
  else if (end_port > 0)
    port_str = "port " + std::to_string(end_port);
  else
    return "";

  return protocol + " " + location + " " + port_str;
}

pcap_direction_t
PcapNic::get_capture_dir(const CaptureDirection &direction)
{
  switch (direction)
  {
    case CaptureDirection::IN:  return PCAP_D_IN;
    case CaptureDirection::OUT: return PCAP_D_OUT;
    default:
      return PCAP_D_INOUT;
  }
}


CaptureDirection
PcapNic::to_direction_string(const std::string &dir)
{
  const auto &uppder_dir = PcapNic::to_upper_case(dir);

  if      (uppder_dir == "IN")  return CaptureDirection::IN;
  else if (uppder_dir == "OUT") return CaptureDirection::OUT;
  else                          return CaptureDirection::INOUT;
}


std::string
PcapNic::to_string_direction(const CaptureDirection &direction)
{
  switch (direction)
  {
    case CaptureDirection::IN:    return "IN";
    case CaptureDirection::OUT:   return "OUT";
    case CaptureDirection::INOUT: 
    default:
      return "INOUT";
  }
}

/// 덤프파일명의 datetime 생성.
std::string 
PcapNic::to_string_current_datetime(const std::string &format)
{
  std::time_t now = std::time(nullptr);
  
  char buffer[80];
  std::strftime(buffer, 80, format.c_str(), std::localtime(&now));
  
  return buffer;
}

/// 파일 close 이후, 임시 확장자 제거.
bool 
PcapNic::remove_tmp_ext(const std::string &filename)
{
  const std::string &tmp_name  = filename + ".tmp";

  if (std::rename(tmp_name.c_str(), filename.c_str()) != 0)
  {
    E_LOG << "Fail to rename. " << tmp_name << std::endl;
    return false;
  }
  return true;
}

std::string
PcapNic::to_upper_case(const std::string &input_str)
{
  std::string result = input_str; // 입력 문자열을 복사하여 결과 문자열 생성
  std::transform(result.begin(), result.end(), result.begin(), ::toupper);
  return result;
}
