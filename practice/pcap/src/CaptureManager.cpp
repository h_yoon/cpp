
#include "CaptureManager.h"
#include "CaptureWorker.h"
#include "Pcap.h"
#include "PcapNic.h"

#include <sys/stat.h>
#include <unistd.h>
#include <iostream>
#include <memory>

CaptureManager::~CaptureManager()
{
  for (auto worker : workers_)
    worker->stop();
}

bool
CaptureManager::init(const std::string &dump_base_directory,
                     const uint8_t     &worker_thread_count,
                     CaptureResult     &error_code,
                     const int64_t     &read_timeout_ms)
{
  if (check_base_directory(dump_base_directory) == false)
  {
    error_code = CaptureResult::INVALID_DUMP_DIRECTORY;
    return false;
  }

  dump_read_timeout_ms_ = read_timeout_ms;

  if (PcapNic::lookup_network_interface(nic_list_) == false)
  {
    E_LOG << "Fail loopkup network interface" << std::endl;
    error_code = CaptureResult::NOT_FOUND_NIC;
    return false; 
  }
  
  for (uint8_t cnt = 0; cnt < worker_thread_count; ++cnt)
  {
    CaptureWorkerPtr worker = std::make_shared<CaptureWorker>(read_timeout_ms);
    worker->start();
    workers_.push_back(worker);
  }

  I_LOG << "worker count " << workers_.size() << std::endl;
  if (workers_.size() != worker_thread_count)
  {
    error_code = CaptureResult::FAIL_WORKER_ALLOC;
    return false;
  }

  error_code = CaptureResult::OK;
  return true;
}

bool 
CaptureManager::start_capture(const uint64_t     &capture_id,
                              const std::string  &local_nic_name,
                              const std::string  &src_ip,
                              const std::string  &dst_ip,
                              const std::string  &dump_middle_directory,
                              const std::string  &dump_filename,
                              const std::string  &protocol,
                              const uint16_t     &src_capture_port_begin,
                              const uint16_t     &src_capture_port_end,
                              const uint16_t     &dst_capture_port_begin,
                              const uint16_t     &dst_capture_port_end,
                              const std::string  &direction,
                              CaptureResult      &error_code)
{

  NicInfo nic;
  if (find_nic_name(nic, local_nic_name) == false)
  {
    E_LOG << "Cannot found NIC " << local_nic_name << std::endl;
    error_code = CaptureResult::NOT_FOUND_NIC;
    return false;
  }

  const std::string &base_path = dump_base_directory_ + "/"+ dump_middle_directory;
  if (is_directory_exist(base_path) == false)
  {
    if (create_directory(base_path) == false)
    {
      E_LOG << "Fail create dump-file folder "<< base_path << std::endl;
      error_code = CaptureResult::INVALID_DUMP_DIRECTORY;
      return false;
    }
  }

  if (PcapNic::validate_info(src_ip, dst_ip, direction, error_code) == false)
    return false;

  CaptureInfo info;
  info.id               = capture_id;
  info.nic              = nic;
  info.read_timeout_ms  = dump_read_timeout_ms_;
  info.pcap_file_path   = base_path;
  info.pcap_file_prefix = dump_filename;
  info.protocol         = protocol;
  info.src_ip           = src_ip;
  info.dst_ip           = dst_ip;
  info.src_port_begin   = src_capture_port_begin;
  info.src_port_end     = src_capture_port_end;
  info.dst_port_begin   = dst_capture_port_begin;
  info.dst_port_end     = dst_capture_port_end;
  info.direction        = PcapNic::to_direction_string(direction);

  return init_new_capture(info, error_code);
}

bool 
CaptureManager::stop_capture(const uint64_t &capture_id)
{
  const int32_t &worker_id = remove_worker_id(capture_id);
  if (worker_id == -1)
  {
    E_LOG << "Cannot found capture id " << capture_id<< std::endl;
    return false;
  }

  const auto &worker = workers_[worker_id];
  worker->stop_capture(capture_id);

  return true;
}

const CaptureWorkerPtr&
CaptureManager::select_worker(uint32_t &worker_id) 
{
  worker_id = (++add_count_) % workers_.size();
  return workers_[worker_id]; 
}

bool
CaptureManager::init_new_capture(const CaptureInfo &info, CaptureResult &error_code)
{
  std::shared_ptr<Pcap> pcap_ptr = std::make_shared<Pcap>();

  if (pcap_ptr->init(info, error_code) == false)
    return false;
  
  error_code = CaptureResult::DUPLICATE_CAPTURE_ID;
  const uint64_t &cid = info.id;
  if (is_exist_capture_id(cid) == true)
  {
    E_LOG << "Already exist capture ID " << cid << std::endl;
    return false;
  }

  uint32_t worker_id = 0;  
  const CaptureWorkerPtr &worker = select_worker(worker_id); /// round-robin 선택
  if (worker->insert_capture(cid, pcap_ptr) == false)
  {
    E_LOG << "Fail insert Capture " << cid << " " << info.nic.if_name << std::endl;
    return false;
  }

  if (insert_worker_id(cid, worker_id) == false)
  {
    E_LOG << "Fail insert_worker_id " << cid << std::endl;
    worker->stop_capture(cid);
    return false;
  }

  error_code = CaptureResult::OK;
  return true;
}

bool
CaptureManager::find_nic_name(NicInfo &info, const std::string &nic_name)
{
  const auto &itr = nic_list_.find(nic_name);
  if (itr == nic_list_.end())
    return false;

  info = itr->second;
  return true;
}

bool   
CaptureManager::insert_worker_id(const uint64_t &capture_id, const int32_t &worker_id)
{
  std::lock_guard<std::mutex> lock_guard(mtx_mapping_);
  auto itr = worker_id_by_capture_id_.find(capture_id);

  if (itr != worker_id_by_capture_id_.end())
  {
    E_LOG << "Fail Set capture. Already Exist capture ID " << capture_id << std::endl;
    return false;
  }

  I_LOG << capture_id << "is wokring on worker[" << worker_id << "]";
  worker_id_by_capture_id_[capture_id] = worker_id;

  return true;
}

int32_t
CaptureManager::remove_worker_id(const uint64_t &capture_id)
{
  std::lock_guard<std::mutex> lock_guard(mtx_mapping_);

  const auto &itr = worker_id_by_capture_id_.find(capture_id);
  if (itr == worker_id_by_capture_id_.end())
  {
    E_LOG << "Cannot found capture ID " << capture_id << std::endl;
    return -1;
  }

  return itr->second;
}


bool
CaptureManager::is_exist_capture_id(const uint64_t &capture_id)
{
  std::lock_guard<std::mutex> lock_guard(mtx_mapping_);
  auto itr = worker_id_by_capture_id_.find(capture_id);

  if (itr == worker_id_by_capture_id_.end())
    return false;

  return true;
}

bool
CaptureManager::check_base_directory(const std::string &path)
{

  if (is_directory_exist(path) == false)
  {
    if (create_directory(path) == false)
    {
      E_LOG << "Fail to create directory. " << path << std::endl;
      return false;
    }
  }

  if (access(path.c_str(), W_OK) != 0)  // 쓰기 권한 확인.
  {
    E_LOG << "directory access. directory is not writable. " << path << std::endl;
    return false;
  }

  if (path.back() == '/') dump_base_directory_ = path.substr(0, path.size() - 1);
  else                    dump_base_directory_ = path;

  I_LOG << "base directory save path " << dump_base_directory_ << std::endl;
  return true;
}

bool
CaptureManager::is_directory_exist(const std::string &path)
{
  struct stat fileStat;
  if (stat(path.c_str(), &fileStat) != 0) // 파일 상태 가져오기
    return false;

  if (S_ISDIR(fileStat.st_mode) == false) // 디렉토리인지 확인.
    return false;

  return true;
}

bool
CaptureManager::create_directory(const std::string &path, const mode_t &mode)
{
  if (mkdir(path.c_str(), mode) != 0) 
  {
    E_LOG << "Fail creating Directory " << path << std::endl;
    return false;
  } 

  return true;
}
