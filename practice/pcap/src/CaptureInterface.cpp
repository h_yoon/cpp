
#include "CaptureInterface.h"
#include "CaptureManager.h"

CaptureInterface* CaptureInterface::instance_ = nullptr;
std::mutex CaptureInterface::mutex_;

CaptureInterface* 
CaptureInterface::instance() 
{ 
  if (instance_ == nullptr)
  {
    std::lock_guard<std::mutex> guard(mutex_);
    if (instance_ == nullptr)
      instance_ = ::new CaptureManager(); 
  }

  return instance_;
}

CaptureInterface& 
CaptureInterface::ref() 
{ 
  return *instance();
}
