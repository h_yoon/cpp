/*
 * build.h
 *
 *  Created on: 2022. 1. 24.
 *      Author: tys
 */

#ifndef BUILDNUMBER_H_
#define BUILDNUMBER_H_

#include <string>

int         defined_number();
std::string defined_date();

inline std::string
build_number()
{
  return std::to_string(defined_number()) + "." + defined_date();
}

#endif /* SRC_BUILD_H_ */
