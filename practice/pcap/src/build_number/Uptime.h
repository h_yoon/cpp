/*
 * Uptime.h
 *
 *  Created on: 2022. 2. 12.
 *      Author: tys
 */

#ifndef UPTIME_H_
#define UPTIME_H_

#include <abc/system/SysDateTime.h>
#include <abc/pattern/Singleton.h>
#include <abc/string/Assistant.h>

#define uptime Uptime::ref()

class Uptime : public abc::Singleton<Uptime>
{
public:
  Uptime() { start_time_.set_current(); }

  int64_t duration_days    (const SysDateTime &now = SysDateTime::now()) const { return duration<std::chrono::hours>(now) / 24; }
  int64_t duration_hours   (const SysDateTime &now = SysDateTime::now()) const { return duration<std::chrono::hours>(now); }
  int64_t duration_minutes (const SysDateTime &now = SysDateTime::now()) const { return duration<std::chrono::minutes>(now); }
  int64_t duration_seconds (const SysDateTime &now = SysDateTime::now()) const { return duration<std::chrono::seconds>(now); }

  int64_t days    (const SysDateTime &now = SysDateTime::now()) const;
  int64_t hours   (const SysDateTime &now = SysDateTime::now()) const;
  int64_t minutes (const SysDateTime &now = SysDateTime::now()) const;
  int64_t seconds (const SysDateTime &now = SysDateTime::now()) const;

  std::string       to_string (const SysDateTime &now = SysDateTime::now()) const;
  const SysDateTime start     () const { return start_time_; }

protected:
  template<typename T> int64_t duration(const SysDateTime &now = SysDateTime::now()) const
  {
    return std::chrono::duration_cast<T>(
        now.time_point() - start_time_.time_point()).count();
  }

protected:
  SysDateTime start_time_;
};

inline std::string
Uptime::to_string(const SysDateTime &now) const
{
  std::string uptime_str;
  uptime_str += abc::to_string(uptime.days    (now), "%02lld ");
  uptime_str += abc::to_string(uptime.hours   (now), "%02lld:");
  uptime_str += abc::to_string(uptime.minutes (now), "%02lld:");
  uptime_str += abc::to_string(uptime.seconds (now), "%02lld");

  return uptime_str;
}

inline int64_t
Uptime::days(const SysDateTime &now) const
{
  return duration_days(now);
}

inline int64_t
Uptime::hours(const SysDateTime &now) const
{
  return duration_hours(now) - duration_days(now) * 24;
}

inline int64_t
Uptime::minutes(const SysDateTime &now) const
{
  return duration_minutes(now) - duration_hours(now) * 60;
}

inline int64_t
Uptime::seconds(const SysDateTime &now) const
{
  return duration_seconds(now) - duration_minutes(now) * 60;
}

#endif /* UPTIME_H_ */
