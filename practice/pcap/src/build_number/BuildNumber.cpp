#include "BuildNumber.h"

#ifndef __BUILD_NUMBER
#define __BUILD_NUMBER 1
#endif

#ifndef __BUILD_DATE
#define __BUILD_DATE "YYYYMMDD"
#endif

int defined_number()
{
  return __BUILD_NUMBER;
}

std::string defined_date()
{
  return __BUILD_DATE;
}
