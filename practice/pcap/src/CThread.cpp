
#include "CThread.h"

#include <chrono>
#include <iostream>

void* 
CThread::launch(void *arg) // 포인터 함수
{
  void *ret = nullptr;
  CThread *obj = (CThread*)arg;
  obj->run(); 
  return ret;
}

bool
CThread::is_run()
{
  return is_run_;
}

void
CThread::start()
{
  is_run_ = true;
  if ( pthread_create(&thread_, NULL, CThread::launch, (void*)this) < 0 )
  {
    std::cout << "error pthread creating:" << std::endl;
    is_run_ = false;
    return;
  }
  else 
  {
   std::cout << "start" << std::endl;
  }
  return;
}

void
CThread::stop()
{
  if ( is_run_ == true )
  {
    is_run_ = false;
    int status = 0;
    std::cout << "end thread " << status << std::endl; 
    join();
  } 
  else
  {
    std::cout << "thread not running" << std::endl;
  }
  return;
}

void
CThread::join()
{
  int status = 0;
  pthread_join(thread_, (void**)&status);
}
