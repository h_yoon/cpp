
#ifndef CAPTURE_INTERFACE_H_
#define CAPTURE_INTERFACE_H_

#include <string>
#include <mutex>

class CaptureManager;


#define capture_interface CaptureInterface::ref()
#define CaptureResult CaptureInterface::PcapResult 

class CaptureInterface
{
public:
  enum class PcapResult
  {
    OK = 0,
    FAIL_WORKER_ALLOC,      // fail alloc thread
    LIB_FAIL_SET_FILTER,    // fail library pcap_setfilter()
    FAIL_LIB_FILE_OPEN,     // fail library pcap_dump_open() 
    FAIL_SET_NON_BLOCKING,  // fail library pcap_setnonblock()
    INVALID_FILTER,         // fail library pcap_compile()
    INVALID_DUMP_DIRECTORY,
    NOT_FOUND_NIC,
    DUPLICATE_CAPTURE_ID,
    INVALID_IP,
    INVALID_PORT,
    INVALID_DIRECTION
  };

public:
  static CaptureInterface* instance();
  static CaptureInterface& ref();

public:
  virtual bool init(const std::string &dump_base_directory,
                    const uint8_t     &worker_thread_count,
                    CaptureResult     &error_code,
                    const int64_t     &read_timeout_ms = 1000) = 0;

  virtual bool start_capture(const uint64_t      &capture_id,            // 캡처 id // 중복시 실패 리턴 
                                const std::string   &local_nic_name,        // NIC NAME "eth1"
                                const std::string   &src_ip,                // src IP   "192.168.3.111" // "" 이면 조건 포함하지 않음
                                const std::string   &dst_ip,                // dst IP   "192.168.3.111" // "" 이면 조건 포함하지 않음 
                                const std::string   &dump_middle_directory, // 덤프파일 저장 중간 디렉토리경로.
                                const std::string   &dump_filename,         // "dump_filename" -> "dump_filename_IN_YYMMDD_HHMMSS.pcap"
                                const std::string   &protocol,              // protocol, "udp" or "tcp" or "icmp" ...
                                const uint16_t      &src_capture_port_start,// 캡처할 src 시작 포트  20000 // 0 이면 src port는 캡쳐 안함 // portrange 20000-20004
                                const uint16_t      &src_capture_port_end,  // 캡처할 src 끝 포트  20004 // 0 이면 src port는 캡쳐 안함
                                const uint16_t      &dst_capture_port_start,// 캡처할 dst 시작 포트  20000 /// 0 이면 dst port는 캡쳐 안함
                                const uint16_t      &dst_capture_port_end,  // 캡처할 dst 끝 포트  20004 // 0 이면 dst port는 캡쳐 안함
                                const std::string   &direction,
                                CaptureResult       &error_code) = 0;        // "IN", "OUT", "INOUT"

  virtual bool stop_capture(const uint64_t &capture_id) = 0;

private:
  static CaptureInterface* instance_;
  static std::mutex mutex_;
};

#endif /* CAPTURE_INTERFACE_H_ */
