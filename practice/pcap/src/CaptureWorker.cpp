

#include "CaptureWorker.h"
#include <iostream>
#include <chrono>

void
CaptureWorker::run()
{
  while (is_run() == true)
  {
    const auto &start = get_time_point();
    process();
    const auto &end   = get_time_point();

    const int64_t &millisec = get_time_diff(start, end);
    if (millisec < period_ms_)
    {
      //D_LOG << "wait_ms (period_ms_ - millisec) " << period_ms_ - millisec << std::endl;
      wait_ms(period_ms_ - millisec);
    }
    else
    {
      //D_LOG << "no wait. process time ms " << millisec << "period_ms_ " << period_ms_<< std::endl;
    }
  }
}

bool
CaptureWorker::insert_capture(const uint64_t &idx, std::shared_ptr<Pcap> pcap)
{
  std::lock_guard<std::mutex> buffer_lock_guard(mtx_start_);
  if (start_capture_buffer_.find(idx) != start_capture_buffer_.end())
  {
    E_LOG << idx << " already in capture." << std::endl;
    return false;
  }

  start_capture_buffer_[idx] = pcap;
  return true;
}

bool
CaptureWorker::stop_capture(const uint64_t &idx)
{
  std::lock_guard<std::mutex> lock_guard(mtx_close_);
  close_idx_buffer_.insert(idx);
  return true;
}

void
CaptureWorker::process()
{
  D_LOG << "worker process " << pcap_by_id_.size() << std::endl;
  remove_closed_capture();
  add_new_capture();

  for (auto &itr : pcap_by_id_)
    itr.second->capture();
}

void
CaptureWorker::add_new_capture()
{
  std::map<uint64_t, std::shared_ptr<Pcap>> buffer;
  std::lock_guard<std::mutex> buffer_lock_guard(mtx_start_);
  {
    if (start_capture_buffer_.size() <= 0)
      return;

    start_capture_buffer_.swap(buffer);
  }

  for (const auto &itr : buffer)
  {
    pcap_by_id_.emplace(std::make_pair(itr.first, itr.second));
    //D_LOG << "add_new_capture. Capture ID " << itr.first << std::endl;
  }
}

void
CaptureWorker::remove_closed_capture()
{
  std::set<uint64_t> close_id_list;
  std::lock_guard<std::mutex> lock_guard(mtx_close_);
  {
    if (close_idx_buffer_.size() <= 0)
      return;

    close_idx_buffer_.swap(close_id_list);
  }

  for (const auto& id : close_id_list)
  {
    const auto &itr = pcap_by_id_.find(id);
    if (itr == pcap_by_id_.end())  // id 없음.
    {
      E_LOG << "cannot found capture_id " << id << std::endl;
      continue;
    }
    
    if (itr->second->close() == -1)
      E_LOG << "Fail Pcap close";

    pcap_by_id_.erase(itr);
  }
}
