
#ifndef CTHREAD_H_
#define CTHREAD_H_

#include <thread>

class CThread
{
public:
  CThread() = default;
  virtual ~CThread() = default;

  void start();
  void stop();
  void join();
  bool is_run();
  void wait_ms(const uint32_t &ms) { std::this_thread::sleep_for(std::chrono::milliseconds(ms)); }
  
protected:
  virtual void run() = 0;

private:
  static void* launch(void *arg); // 포인터 함수

private:
  bool        is_run_   = false;
  pthread_t   thread_;
};

#endif /* CTHREAD_H_ */