
#ifndef PCAP_NIC_H_
#define PCAP_NIC_H_

#include "Pcap.h"
#include <map>
enum class CaptureDirection;
struct NicInfo;

class PcapNic
{
public:
  static bool lookup_network_interface   (std::map<std::string, NicInfo> &nic_list);
  static pcap_direction_t get_capture_dir(const CaptureDirection &dir);
  static std::string      to_string_direction (const CaptureDirection &dir);
  static CaptureDirection to_direction_string (const std::string &dir_str);
  static std::string to_string_current_datetime(const std::string &format="%Y%m%d_%H%M%S");
  static void packet_handler(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) { pcap_dump(args, header, packet); }
  static std::string  get_filter_exp (const CaptureInfo &info);
  static bool validate_info(const std::string &src_ip,
                            const std::string &dst_ip,
                            const std::string &direction,
                            CaptureResult     &error_code);
  static bool remove_tmp_ext(const std::string &filename);
  static std::string to_upper_case(const std::string &input_str);

private:
  static std::string  get_filter_host(const std::string &location,
                                      const std::string &ip);
  static std::string  get_filter_port(const std::string &location,    // "src" or "dst" or ""
                                      const std::string &protocol,    // "udp" or "tcp" or ""
                                      const uint16_t    &port_begin,  // 5656 or 0
                                      const uint16_t    &port_end);   // 5656 or 0

private:
  //static std::deque<std::string> caputre_protocols_ = {"TCP", "UDP", "RTP", }
};

#endif /* PCAP_NIC_H_ */
