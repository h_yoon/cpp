

#include "CaptureInterface.h"

#include <deque>
#include <iostream>
#include <csignal>
#include <unistd.h>


void signal_handler(int signum) 
{
  exit(signum);
}

void add_capture(const  int32_t &cnt, const std::string &nic_name, std::deque<int> &list)
{
  int port = 5600;
  std::deque<std::string> dirs = {"in", "out"};
  for (int i=1; i<=cnt; ++i)
  {
    for (int j=0; j<2; ++j)
    {
      const auto &idx = (i*10) + j;
      CaptureResult ret;
      if (capture_interface.start_capture(idx,
                                    nic_name,
                                    "",   // src ip
                                    "",   // dst ip
                                    "dumps" + std::to_string(i),  // middle dir
                                    "dumpfile_name",  // dump filename prefix
                                    "tcp",            // protocol
                                    0, 0,             // src port begin, end
                                    0, 0,             // dst port begin ,end
                                    dirs[j], 
                                    ret) == false)         // capture direction
      {
        std::cout << "Fail to start capture, return " << (int32_t)ret << std::endl;
        continue;
      }
      
      list.push_back(idx);
    }
    ++port;
  }
}

void stop_capture(const std::deque<int> &list)
{
  for (auto &itr : list)
    capture_interface.stop_capture(itr);
}

int main(int argc, char** argv)
{
  signal(SIGINT, signal_handler);

  int capture_cnt = 1;
  int thread_cnt = 1;
  std::string base_dir = "./";
  std::string nic_name = "eth1";

  if (argc == 1)
  {
    std::cout << "ex) sudo ./portCapture ${capture_cnt} ${thread_cnt} ${base_dir} ${nic_name}" << std::endl;
    return 0;
  }

  if (argc > 1)
    capture_cnt = atoi(argv[1]);
  
  if (argc > 2)
    thread_cnt  = atoi(argv[2]);

  if (argc > 3)
    base_dir = argv[3];

  if (argc > 4)
    nic_name = argv[4];

  std::cout << "capture count " << capture_cnt << ", thread count " << thread_cnt << ", base_dir " << base_dir << ", nic_name " << nic_name<< std::endl;
  usleep(1000000);

  std::deque<int> list;
  CaptureResult error_code;
  if (capture_interface.init(base_dir, thread_cnt, error_code, 1000) == false)
    return 0;
  
  add_capture(capture_cnt, nic_name, list);

  for(int i=0; i<20; ++i)
  {
    usleep(1000000);
  }

  stop_capture(list);

  for(int i=0; i<10; ++i)
    usleep(300000);

  std::cout << "END" << std::endl;
  return 0;
}

