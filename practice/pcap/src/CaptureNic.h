

#ifndef CAPTURE_NIC_H_
#define CAPTURE_NIC_H_

#include <string>
#include <iostream>

#define E_LOG std::cout << "ERR " <<__FILE__<<":"<<__LINE__<<" "<<__func__<<" : "
#define I_LOG std::cout << "INF " <<__FILE__<<":"<<__LINE__<<" "<<__func__<<" : "
#define D_LOG std::cout << "DBG " <<__FILE__<<":"<<__LINE__<<" "<<__func__<<" : "

enum class CaptureDirection
{
  INOUT = 0,
  IN,
  OUT
};

struct NicInfo
{
  NicInfo() {};
  ~NicInfo() {};
  std::string if_name; // network interface name. ex) eth1. eth0
  std::string ip_str;
  uint32_t    localnet;
  uint32_t    netmask;

  void operator=(const NicInfo &rhs) {
    if_name   = rhs.if_name;
    ip_str    = rhs.ip_str;
    localnet  = rhs.localnet;
    netmask   = rhs.netmask;
  }
};

struct CaptureInfo
{
  CaptureInfo() {};
  ~CaptureInfo() {};
  uint64_t    id;
  NicInfo     nic;

  std::string pcap_file_path;
  std::string pcap_file_prefix;

  std::string protocol  = "";
  std::string src_ip    = "";
  std::string dst_ip    = "";
  uint16_t    src_port_begin = 0;
  uint16_t    src_port_end   = 0;
  uint16_t    dst_port_begin = 0;
  uint16_t    dst_port_end   = 0;
  CaptureDirection direction;

  int32_t     read_timeout_ms     = 1000;
  int8_t      promiscious_mode    = 1;
  int32_t     max_snapshot_length = BUFSIZ;
  int32_t     optimize = 0;
};


#endif /* CAPTURE_NIC_H_ */