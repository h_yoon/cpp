#ifndef CAPTURE_MANAGER_H_
#define CAPTURE_MANAGER_H_

#include "CaptureInterface.h"
#include "CaptureNic.h"

#include <map>
#include <deque>
#include <memory>
#include <mutex>

class CaptureWorker;
using CaptureWorkerPtr = std::shared_ptr<CaptureWorker>;

class CaptureManager : public CaptureInterface
{
public:
  CaptureManager() = default;
  virtual ~CaptureManager();

  bool init(const std::string &dump_base_directory,
            const uint8_t     &worker_thread_count,
            CaptureResult     &error_code,
            const int64_t     &read_timeout_ms = 1000);

  bool start_capture(const uint64_t      &capture_id,            // 캡처 id // 중복시 실패 리턴 
                           const std::string   &local_nic_name,        // NIC NAME "eth1"
                           const std::string   &src_ip,                // src IP   "192.168.3.111" // "" 이면 조건 포함하지 않음
                           const std::string   &dst_ip,                // dst IP   "192.168.3.111" // "" 이면 조건 포함하지 않음 
                           const std::string   &dump_middle_directory, // 덤프파일 저장 중간 디렉토리경로.
                           const std::string   &dump_filename,         // "dump_filename" -> "dump_filename_IN_YYMMDD_HHMMSS.pcap"
                           const std::string   &protocol,              // protocol, "udp" or "tcp" or "icmp" ...
                           const uint16_t      &src_capture_port_start,// 캡처할 src 시작 포트  20000 // 0 이면 src port는 캡쳐 안함 // portrange 20000-20004
                           const uint16_t      &src_capture_port_end,  // 캡처할 src 끝 포트  20004 // 0 이면 src port는 캡쳐 안함
                           const uint16_t      &dst_capture_port_start,// 캡처할 dst 시작 포트  20000 /// 0 이면 dst port는 캡쳐 안함
                           const uint16_t      &dst_capture_port_end,  // 캡처할 dst 끝 포트  20004 // 0 이면 dst port는 캡쳐 안함
                           const std::string   &direction,             // "IN", "OUT", "INOUT"
                           CaptureResult       &error_code);

  bool stop_capture(const uint64_t &capture_id);

private:
  bool    is_exist_capture_id(const uint64_t &capture_id);
  bool    insert_worker_id(const uint64_t &capture_id, const int32_t &worker_id);
  int32_t remove_worker_id(const uint64_t &capture_id);

  bool init_new_capture(const CaptureInfo &info, CaptureResult &error_code);
  bool find_nic_name(NicInfo &info, const std::string &nic_name);
  
  bool check_base_directory(const std::string &path);
  bool create_directory(const std::string &path, const mode_t &mode = 0755);
  bool is_directory_exist(const std::string &path);

  const CaptureWorkerPtr& select_worker(uint32_t &worker_id);

private:
  std::deque<CaptureWorkerPtr> workers_;
  std::map<std::string, NicInfo> nic_list_;

  std::mutex  mtx_mapping_;
  std::map<uint64_t, int32_t> worker_id_by_capture_id_;

  std::string dump_base_directory_;
  int64_t     dump_read_timeout_ms_ = 0;

  uint32_t    add_count_ = 0;
};

#endif /* CAPTURE_MANAGER_H_ */