
#include "Pcap.h"
#include "CaptureNic.h"

#include "CThread.h"

#include <map>
#include <set>
#include <deque>
#include <mutex>
#include <atomic>
#include <chrono>

using time_point = std::chrono::high_resolution_clock::time_point;

class CaptureWorker : public CThread
{
public:
  CaptureWorker(const int64_t &period_ms) : period_ms_(period_ms) {}
  virtual ~CaptureWorker() {};

  // bool init();
  bool insert_capture(const uint64_t &idx, std::shared_ptr<Pcap> pcap);
  bool stop_capture(const uint64_t &idx);

protected:
  void run();

private:
  void process();
  void add_new_capture();
  void remove_closed_capture();

  static int64_t get_time_diff(const time_point &start, const time_point &end);
  static time_point get_time_point();

private:
  std::map<uint64_t, std::shared_ptr<Pcap>> pcap_by_id_; // capture_id, Pcap

  std::map<uint64_t, std::shared_ptr<Pcap>> start_capture_buffer_;  
  std::set<uint64_t>                        close_idx_buffer_;
  std::mutex  mtx_start_;
  std::mutex  mtx_close_;

  int64_t period_ms_;
};

inline int64_t 
CaptureWorker::get_time_diff(const time_point &start, const time_point &end)
{
  const std::chrono::milliseconds &ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  return ms.count();
}

inline time_point 
CaptureWorker::get_time_point()
{
  return std::chrono::high_resolution_clock::now();
}
