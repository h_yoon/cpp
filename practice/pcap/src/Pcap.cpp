

#include "Pcap.h"
#include "PcapNic.h"

#include <iostream>
#include <unistd.h>

bool
Pcap::init(const CaptureInfo &info, CaptureResult &error_code)
{
  char errbuf[PCAP_ERRBUF_SIZE];

  const NicInfo &nic = info.nic;
  handle_ = pcap_open_live(nic.if_name.c_str(), 
                           info.max_snapshot_length, 
                           info.promiscious_mode, 
                           info.read_timeout_ms, 
                           errbuf);

  if (handle_ == nullptr) 
  {
    E_LOG << "Couldn't open device " << nic.if_name << ": " << errbuf << std::endl;
    error_code = CaptureResult::FAIL_LIB_FILE_OPEN;
    return false;
  }

  const pcap_direction_t &capture_dir = PcapNic::get_capture_dir(info.direction);
  if (pcap_setdirection(handle_, capture_dir) != 0)
  {
    E_LOG << "Couldn't set direction to"<< capture_dir << std::endl;
    pcap_close(handle_);
    error_code = CaptureResult::INVALID_DIRECTION;
    return false;
  }
  
  // Set the capture to non-blocking mode
  if (pcap_setnonblock(handle_, 1, errbuf) == -1)
  {
    E_LOG << "Couldn't set nonblocking mode: " << errbuf << std::endl;
    pcap_close(handle_);
    error_code = CaptureResult::FAIL_SET_NON_BLOCKING;
    return false;
  }

  // Compile and apply the filter
  const std::string &filter = PcapNic::get_filter_exp(info);

  if (pcap_compile(handle_, &dump_fp_, filter.c_str(), info.optimize, nic.netmask) == -1) 
  {
    E_LOG << "Couldn't parse filter " << filter << std::endl;
    error_code = CaptureResult::INVALID_FILTER;
    return false;
  }

  if (pcap_setfilter(handle_, &dump_fp_) == -1) 
  {
    E_LOG << "Couldn't install filter " << filter << std::endl;
    error_code = CaptureResult::LIB_FAIL_SET_FILTER;
    return false;
  }

  const std::string &protocol = (info.protocol!=""?"_"+info.protocol:"");
  pcap_file_name_ = info.pcap_file_path   + "/" +
                    info.pcap_file_prefix +
                    protocol + "_" + 
                    PcapNic::to_string_direction(info.direction) + "_" +
                    PcapNic::to_string_current_datetime() + ".pcap";

  const std::string &tmp_dump_filename = pcap_file_name_ + ".tmp"; 

  // Open dump file
  dumper_ = pcap_dump_open(handle_, tmp_dump_filename.c_str());
  if (dumper_ == nullptr) 
  {
    E_LOG << "Couldn't open dump file: " << pcap_geterr(handle_) << std::endl;
    error_code = CaptureResult::FAIL_LIB_FILE_OPEN;
    return false;
  }

  I_LOG << "init capture\n nic name '" << nic.if_name << "' \n filter '" << filter  << "'\n filename '" << tmp_dump_filename << "'" << std::endl;
  error_code = CaptureResult::OK;
  return true;
}

void
Pcap::capture()
{
  const int32_t &cnt = pcap_dispatch(handle_, 0, PcapNic::packet_handler, (u_char *)dumper_);

  if (cnt)
  {
    packet_count_ += cnt;
    //I_LOG << "dispatch cnt " << cnt << " " << pcap_file_name_<< std::endl;
  }
}

int32_t
Pcap::close()
{
  pcap_dump_close(dumper_);
  pcap_freecode(&dump_fp_);
  pcap_close(handle_);

  if (PcapNic::remove_tmp_ext(pcap_file_name_) == false)
  {
    E_LOG << "Fail to close. capture_id " << pcap_file_name_ << std::endl;
    return -1;
  }

  I_LOG << pcap_file_name_ << " " << packet_count_ <<std::endl;
  return packet_count_;
}

