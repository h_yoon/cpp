

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>

void* new_capture_obj();
int   init_capture_obj(void *void_mgr, 
                       const char *dump_base_directory,
                       int worker_thread_count,
                       int read_timeout_ms);
int   start_nic_capture(void *mgr,
                        uint32_t capture_id,
                        const char *local_nic_name,
                        const char *src_ip,
                        const char *dst_ip,
                        const char *dump_millde_directory,
                        const char *dump_filename,
                        const char *protocol,
                        uint16_t src_capture_port_start,
                        uint16_t src_capture_port_end,
                        uint16_t dst_capture_port_start,
                        uint16_t dst_capture_port_end,
                        const char *direction);

int   stop_nic_capture(void *mgr,
                       uint32_t capture_id);  

#ifdef __cplusplus
}
#endif
