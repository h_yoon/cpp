

#include "CaptureManager.h"

#ifdef __cplusplus
extern "C" {
#endif

void* new_capture_obj()
{
  return (void*)(CaptureManager::instance());
}

int init_capture_obj(void *void_mgr, 
                     const char *dump_base_directory,
                     int worker_thread_count,
                     int read_timeout_ms)
{
  if (!void_mgr)
    return CaptureResult::NULL_POINTER;

  CaptureManager *mgr = (CaptureManager*)void_mgr;
  return mgr->init(dump_base_directory, worker_thread_count, read_timeout_ms);
}

int
start_nic_capture(void *void_mgr,
                  uint32_t capture_id,
                  const char *local_nic_name,
                  const char *src_ip,
                  const char *dst_ip,
                  const char *dump_millde_directory,
                  const char *dump_filename,
                  const char *protocol,
                  uint16_t src_capture_port_start,
                  uint16_t src_capture_port_end,
                  uint16_t dst_capture_port_start,
                  uint16_t dst_capture_port_end,
                  const char *direction)
{
  if (!void_mgr)
    return CaptureResult::NULL_POINTER;

  CaptureManager *mgr = (CaptureManager*)void_mgr;
  return mgr->start_capture(capture_id,
                            local_nic_name,
                            src_ip,
                            dst_ip,
                            dump_millde_directory,
                            dump_filename,
                            protocol,
                            src_capture_port_start,
                            src_capture_port_end, 
                            dst_capture_port_start,
                            dst_capture_port_end,
                            direction);
}

int
stop_nic_capture(void *void_mgr, uint32_t capture_id)
{
  if (!void_mgr)
    return CaptureResult::NULL_POINTER;

  CaptureManager *mgr = (CaptureManager*)void_mgr;
  if (mgr->stop_capture(capture_id) == false)
    return CaptureResult::INVALID_CAPTURE_ID;
  
  return CaptureResult::OK;
}
  
#ifdef __cplusplus
}
#endif