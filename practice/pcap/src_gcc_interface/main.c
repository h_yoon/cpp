/*
#library, interface 
1 /pcap-stt/src]$ make clean; make; make install             /// libcapture.a 생성, libpcap을 래핑한 cpp 라이브러리 
2 /pcap-stt/src]$ g++ -c CppInterface.cpp -std=c++1y         /// C 에서 사용하기 위한 interface 컴파일 

# C module
3 /pcap-stt/src-c]$ gcc -c main.c -std=c11 -I../src          /// libcapture.a를 사용한 C 모듈 컴파일 
4 /pcap-stt/src-c]$ gcc -o mix main.o   ../src/CppInterface.o  /// 최종 C 모듈 컴파일 
                                        -std=c11 
                                        -lstdc++ 
                                        -I../include    /// libcapture.a Include  
                                        -L../lib        /// libcapture.a, libpcap.a 경로
                                        -lcapture       
                                        -lpcap 
                                        -lpthread
                                        */
#include "CppInterface.h"
#include <stdio.h>
#include <unistd.h>

int main() 
{
  void *mgr = new_capture_obj();

  int ret = init_capture_obj(mgr, "./dump_file", 1, 1000);
  if (ret)
  {
    printf("Fail capture init, return code %d\n", ret);
    return 0;
  }

  printf("init ok.\n");

  ret = start_nic_capture(mgr, 
                          1, 
                          "eth1", 
                          "",
                          "",
                          "dump_file",
                          "capture",
                          "udp",
                          0, 0,
                          0, 0,
                          "in");

  for(int i=0; i<10; ++i)
  {
    sleep(1);
    printf("sleep.\n");
  }

  stop_nic_capture(mgr, 1);

  sleep(2);

  return 0;
}
