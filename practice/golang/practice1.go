
package main

import "fmt"

type car interface {
  get_type() string
  get_price() int64
}

type Tussan struct {
  ctype string
  price int64
}

func (tussan Tussan) get_type() string {
  return tussan.ctype
}

func (tussan Tussan) get_price() int64 {
  return tussan.price
}


func check(c car) {
  fmt.Println(c.get_type())
  fmt.Println(c.get_price())
}


func main() {
  c1 := Tussan{ctype:"suv", price:1000}
  c2 := Tussan{ctype:"midun", price:2000}

  check(c1)
  check(c2)
}
