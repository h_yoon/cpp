
package main

import "fmt"

func main() {
  messages := make(chan string,2)
  signals := make(chan bool,2)

  messages <- "test mesg"

  select {
  case msg := <- messages :
      fmt.Println("received message", msg)
  default:
      fmt.Println("no message received")
  }


  msg := "hi"
  select {
  case messages <- msg:
    fmt.Println("sent message", msg)
  default:
    fmt.Println("no message sent")
  }

  select {
  case msg := <- messages:
    fmt.Println("received message", msg)
  case sig := <- signals:
    fmt.Println("received signal", sig)
  default:
    fmt.Println("no activity")
  }

  ///-----------

  queue := make(chan string, 2)
  queue <- "one"
  queue <- "two"
  close(queue)

  for elem := range queue {
    fmt.Println(elem)
  }

  t1 := make(chan string, 100)
}


