package main

func map1() {

	var m map[int]string

	m = make(map[int]string)

	m[100] = "apple"
	m[1] = "Grape"

	str := m[100]
	println(str)

	noStr := m[1111]
	println(noStr)

	delete(m, 100)
}

type Device struct {
  name string
}

func map2() {

  var m map[int]Device

  m = make (map[int]Device)

  m[1] = Device{name: "Phone1"}
  m[2] = Device{name: "Phone2"}
  m[3] = Device{name: "Phone3"}

  dev := m[3]
   
  println(dev.name)

  delete(m, 3)

  dev2, ok := m[3]
  
  if ok {
    println(dev2.name)
  } else {
    println("cannot found 3")
  }
}

func main() {

  map2()  
	map1()
}
