package main

import (
    "database/sql"
    "fmt"
    "log"
    _ "github.com/go-sql-driver/mysql"
)

func main() {
    // sql.DB 객체 생성
    db, err := sql.Open("mysql", "core:core@tcp(192.168.15.92:30586)/CP")
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    var id string
    var name string
    rows, err := db.Query("SELECT ADMIN_ID, ADMIN_GROUP_ID FROM T_ADMIN")
    if err != nil {
        log.Fatal(err)
    }
    defer rows.Close() //반드시 닫는다 (지연하여 닫기)

    for rows.Next() {
        err := rows.Scan(&id, &name)
        if err != nil {
            log.Fatal(err)
        }
        fmt.Println("test", id, name)
    }
}

