// reliation 'has a'

package main

import (
	"fmt"
)

type Person struct {
	name string
	age  int
}

func (p *Person) greeting() {
	fmt.Println("Hello Person")
}

func (s *Student) greeting() {
  fmt.Println("Hello Student")
}

type Student struct {
	p      Person // embeding
	school string
	grade  int
}

func main() {
	var s Student
	s.p.greeting()
  s.greeting()
}
