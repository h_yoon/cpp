package main

import (
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/eventials/go-tus"
)

type MockStore struct {
	m map[string]string
}

type Store interface {
	Get(fingerprint string) (string, bool)
	Set(fingerprint, url string)
	Delete(fingerprint string)
	Close()
}

func NewMockStore() Store {
	return &MockStore{
		make(map[string]string),
	}
}

func (s *MockStore) Get(fingerprint string) (string, bool) {
	url, ok := s.m[fingerprint]
	return url, ok
}

func (s *MockStore) Set(fingerprint, url string) {
	s.m[fingerprint] = url
}

func (s *MockStore) Delete(fingerprint string) {
	delete(s.m, fingerprint)
}

func (s *MockStore) Close() {
	for k := range s.m {
		delete(s.m, k)
	}
}

func main() {

	var wg sync.WaitGroup
	wg.Add(1)
	f, err := os.Open("/APP/go/src/tus.io/client/my-file.txt")

	if err != nil {
		panic(err)
	}

	defer f.Close()

	config := tus.Config{
		ChunkSize:           2 * 1024 * 1024,
		Resume:              true,
		OverridePatchMethod: false,
		Store:               NewMockStore(),
		Header: map[string][]string{
			"X-Extra-Header": []string{"somevalue"},
		},
	}
	// create the tus client.
	client, err := tus.NewClient("http://192.168.14.6:7070/files/", &config)

	if err != nil {
		fmt.Println("fail NewClient:", err)
		return
	}
	// create an upload from a file.
	upload, err := tus.NewUploadFromFile(f)
	if err != nil {
		fmt.Println("fail NewUploadFromFile:", err)
		return
	}
	// create the uploader.
	uploader, err := client.CreateUpload(upload)

	if err != nil {
		fmt.Println("fail CreateUpload:", err)
		return
	}
	go func() {
		defer wg.Done()
		time.Sleep(1 * time.Second)
		uploader.Abort()

		fmt.Println("Force Abort")

		time.Sleep(3 * time.Second)
		fmt.Println("Start again. ResumeUpload.")
		uploader, err = client.ResumeUpload(upload)
		time.Sleep(1 * time.Second)
		if err != nil {
			fmt.Println("ResumeUpload error:", err)
			return
		}

		err = uploader.Upload()
		fmt.Println("Transfer End")

	}()
	// start the uploading process.
	uploader.Upload()

	fmt.Println("Waiting go()")
	wg.Wait()

	fmt.Println("Process Done")
}
