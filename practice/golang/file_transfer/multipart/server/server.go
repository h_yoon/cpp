package main

import (
	"fmt"
	"io"
	// "mime/multipart"
	"net/http"
	"os"
	// "time"
	// "sync"
)
func main() {
	createEmployeeHanlder := http.HandlerFunc(createEmployee)
	http.Handle("/upload", createEmployeeHanlder)
	http.ListenAndServe(":13344", nil)
}

func createEmployee(w http.ResponseWriter, request *http.Request) {
	err := request.ParseMultipartForm(32 << 20) // maxMemory 32MB
	if err != nil {
		fmt.Println("bad req of ParseMultipartForm\nerr:",err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	file, _, err := request.FormFile("docpdf")
	if err != nil {
		fmt.Println("bad req of FormFile\nerr:",err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	defer file.Close()

	err = os.MkdirAll("./upload", os.ModePerm)
	if err != nil {
		fmt.Fprintf(w, "error MkdirAll\nerr:",err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	dst, err := os.Create(fmt.Sprintf("./upload/%s.%s","docpdf", "ext"))
	if err != nil {
		fmt.Fprintf(w, "error os.Create\nerr:",err)
		http.Error(w,err.Error(), http.StatusInternalServerError)
		return
	}

	defer dst.Close()

	// Copy file
	fmt.Fprintf(w, "data copy")
	_, err = io.Copy(dst, file)
	if err != nil {
		fmt.Fprintf(w, "error io.Copy\nerr:",err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprintf(w, "Upload successful!")

	// // saveFile(h, "formfile")
	// /// Access the photo key - Second Approach
	// for _, h := range request.MultipartForm.File["photo"] {
	// 	// err := saveFile(h, "mapaccess")
	// 	if err != nil {
	// 		w.WriteHeader(http.StatusBadRequest)
	// 		return
	// 	}
	// }
  w.WriteHeader(200)
	return
}