package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	// "path/filepath"
)

type MultipartClient struct {
	remote_host         string
	remote_port         string
	remote_file_bytes   uint
	remote_file_version string
	local_file_path     string
	local_file_name     string
	local_file_bytes    uint
	local_file_version  string
}

func (obj *MultipartClient) local_file_check() bool {
	// file exist check

	return false
	// get and save of file size
	obj.local_file_bytes = 9999
	return true
}

func (obj *MultipartClient) init(_host string, _path string, _name string) bool {
	obj.remote_host = _host
	obj.local_file_path = _path
	obj.local_file_name = _name

	if obj.local_file_check() == false {
		fmt.Println("file not exists")
		return false
	}
	return true
}

/* 원격의 파일을 확인해보고 응답받은 크기를 리턴한다.
 * 0: 원격에 파일 없음
 * 0>: 현재 원격에 있는 파일크기
 */
func (obj *MultipartClient) remote_file_query() uint {

	request_uri := obj.remote_host + "/image"
	body := &bytes.Buffer{}

	request, err := http.NewRequest("GET", request_uri, body)
	request.Header.Set("Content-Type", "Applicatoin-json")

	if err != nil {
		log.Fatal(err)
	}

	response := obj.http_request(body, request)

	if response.StatusCode == 200 {
		return true
	}

}

/* 원격의 파일이 있는데, 로컬의 파일보다 작은경우(전송중 오류 케이스)
 * 로컬파일을 열어 원격파일크기까지 seek
 */
func (obj *MultipartClient) file_seek() bool {

	return true
}

/* multipart 파일 복사
 *
 */
func (obj *MultipartClient) fileUploadRequest(uri string) (*http.Request, error) {

	file, err := os.Open(obj.local_file_path + obj.local_file_name)
	if err != nil {
		fmt.Println("Open error\nerr:", err)
		return nil, err
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile(obj.local_file_name, obj.local_file_path+obj.local_file_name)

	if err != nil {
		fmt.Println("read error\nerr:", err)
		return nil, err
	}
	_, err = io.Copy(part, file)

	// for key, val := range params {
	// 	_ =  .WriteField(key, val)
	// }

	err = writer.Close()
	if err != nil {
		fmt.Println("write error")
		return nil, err
	}

	req, err := http.NewRequest("POST", uri, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	fmt.Println("uri:", uri)

	return req, err
}

/* http를 통한 업로드 수행
 */
func (obj *MultipartClient) upload() bool {

	request_uri := obj.remote_host + "/upload"
	body := &bytes.Buffer{}

	request, err := obj.fileUploadRequest(request_uri)

	if err != nil {
		log.Fatal(err)
	}

	response := obj.http_request(body, request)

	if response.StatusCode == 200 {
		return true
	}

	return false
}

func (obj *MultipartClient) http_request(body *bytes.Buffer, request *http.Request) *http.Response {

	client := &http.Client{}
	resp, err := client.Do(request)

	if err != nil {
		log.Fatal(err)
	} else {
		_, err := body.ReadFrom(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		resp.Body.Close()
		fmt.Println(resp.StatusCode)
		fmt.Println(resp.Header)

		fmt.Println(body)
	}
	return resp
}

/*
 * gorutine
 */
func (obj *MultipartClient) run() {

	remote_bytes := obj.remote_file_query()
	bytes := obj.local_file_bytes
	if bytes == remote_bytes {
		fmt.Println("image already transfered")

	} else if bytes > 0 && bytes < remote_bytes { // 전송 중단되었던 상황

	} else {

	}

	return
}

func main() {
	remote_host_addr := "http://192.168.14.6:13344"
	local_file_path, _ := os.Getwd()
	local_file_name := "test_image.ios"

	multipart := MultipartClient{}
	inited := multipart.init(remote_host_addr, local_file_path+"/", local_file_name)

	if inited == false {
		fmt.Println("multipart init fail")
		return
	}

	if multipart.upload() == false {
		fmt.Println("multipart upload fail")

	} else {
		fmt.Println("multipart upload Successful")
	}

	return
}
