/* 
 * go test for db access
 * go mysql module install 
 * pre command: 
    export GOPATH=/usr/bin/
    go get -u github.com/go-sql-driver/mysql
 *
 * mysql user access
 * command: CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
 *          GRANT ALL PRIVILEGES ON database_name.* TO 'newuser'@'localhost';
 */
package main

import (
    // db access
    "database/sql"
    "fmt"
    "log"
    _ "github.com/go-sql-driver/mysql"

    // file io
    "bufio"
    "os"
    "strings"

    "container/list"
)


func ErrorCheck(e error) {
  if e != nil {
    panic(e)
  }
}

type VmInfo struct {
  table map[string]int
}

func (users *VmInfo) get(info DbInfo) int {

  vm_list := list.New()
  // connect_str := info.id +":"+info.pw +"@tcp("+info.host+":"+info.port+")/"+info.db_name
  // db, err := sql.Open("mysql", "hyoon:hs@tcp(localhost)/test_base")
  db, err := sql.Open("mysql", info.connect_string)
  if err != nil {
    log.Fatal(err)
  }
  defer db.Close()

  var vm_id string
  rows, err := db.Query("SELECT VM_ID FROM T_RSC_GROUP_VM")
  // rows, err := db.Query("call TEST_PROC()")

  if err != nil {
      log.Fatal(err)
      return 0
  }
  defer rows.Close() //반드시 닫는다 (지연하여 닫기)

  cnt := 0
  for rows.Next() {
      err := rows.Scan(&vm_id)
      if err != nil {
          log.Fatal(err)
      }
      vm_list.PushBack(vm_id)
      fmt.Println("test2", vm_id)
      cnt++
  }
  return cnt
}

// watcher 정보 관리
type WatcherInfo struct {
  ps_id   string
  rsc_type string
} 
type WatcherList struct {
  table map[string]WatcherInfo
}
func (users *WatcherList) init() {
  users.table = make(map[string]WatcherInfo)
}
func (users *WatcherList) get(info DbInfo) int {

  db, err := sql.Open("mysql", info.connect_string)
  if err != nil {
    log.Fatal(err)
  }
  defer db.Close()

  var ps_id string
  var vm_id string
  var rsc_type string
  rows, err := db.Query("SELECT PS_ID, VM_ID, RSC_TYPE FROM T_RSC_GROUP_WATCHER_VM")
  // rows, err := db.Query("call TEST_PROC()")

  if err != nil {
      log.Fatal(err)
      return 0
  }
  defer rows.Close() //반드시 닫는다 (지연하여 닫기)

  cnt := 0
  for rows.Next() {
      err := rows.Scan(&ps_id, &vm_id, &rsc_type)
      if err != nil {
          log.Fatal(err)
      }
      users.table[vm_id] = WatcherInfo{ps_id,rsc_type}
      // fmt.Println("test", ps_id, vm_id, rsc_type)
      cnt++
  }
  return cnt
}

// config 정보 
type DbInfo struct {
  host    string
  port    string
  id      string
  pw      string
  db_name  string
  proc_name string
  connect_string string
}
func (dbinfo *DbInfo) set_config(datas map[string]string) bool {
  dbinfo.host   = datas["CP_HOST"]
  dbinfo.port   = datas["CP_PORT"]
  dbinfo.id     = datas["CP_ID"]
  dbinfo.pw     = datas["CP_PW"]
  dbinfo.db_name   = datas["CP_DBNAME"]
  dbinfo.proc_name = datas["PROC_NAME"]

  dbinfo.connect_string = dbinfo.id +":"+dbinfo.pw +"@tcp("+dbinfo.host+":"+dbinfo.port+")/"+dbinfo.db_name
  
  return true
}
func (dbinfo *DbInfo) get_config() bool {
  
  file, err := os.Open("./batch.conf")
  ErrorCheck(err)

  datas := make(map[string]string) 

  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    line := scanner.Text()
    parted := strings.Index(line,":")
    if parted < 0 {
      continue;
    }
    key := line[:parted]
    value := line[parted+1:]
    datas[key] = value
   
    if err := scanner.Err(); err != nil {
      fmt.Println("Error while reading file:%s", err)
      return false
    }
  }
  dbinfo.set_config(datas)
  return true
}

// 와쳐 서비스 정보
type ServiceInfo struct {
  table map[string]int
}
func (services *ServiceInfo) init() {
  services.table = make(map[string]int)
}
func (Service *ServiceInfo) get(info DbInfo) bool {
  db, err := sql.Open("mysql", info.connect_string)
  if err != nil {
    log.Fatal(err)
  }
  defer db.Close()

  var ps_id    string
  var keep_day int
  rows, err := db.Query("SELECT PS_ID, KEEP_DAY FROM T_PRICE_PLAN_SAAS_DETAIL")
  // rows, err := db.Query("call TEST_PROC()")

  if err != nil {
      log.Fatal(err)
      return false
  }
  defer rows.Close() //반드시 닫는다 (지연하여 닫기)

  for rows.Next() {
      err := rows.Scan(&ps_id, &keep_day)
      if err != nil {
          log.Fatal(err)
      }
      Service.table[ps_id]=keep_day
      fmt.Println("test", ps_id, keep_day)
  }
  return true
}


func main() {

  dbinfo := DbInfo{}
  dbinfo.get_config()

  services := ServiceInfo{} // 서비스 정보 로딩
  services.init()
  services.get(dbinfo)

  watcherlist := WatcherList{}
  watcherlist.init()
  cnt := watcherlist.get(dbinfo)
  fmt.Println("user count is ",cnt)

  vmlist := VmInfo{}
  cnt = vmlist.get(dbinfo)
}

