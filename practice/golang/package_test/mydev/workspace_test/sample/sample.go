/// 참고 사이트 https://jeremyko.blogspot.com/2022/03/golang-118-workspace-mode.html

/*
  cd {module_folder_name}
  go mod init github.com/hyoon/{module_folder_name}

  cd {workspace}
  go work init
  go work use ./{module_folder_name}

  디렉토리 구조
  {workspace}
   -{main_folder_name}
   -{module_folder_name}
*/

package main

import (
	"fmt"

	"github.com/hyoon/my_mod"
)

func main() {
	message := my_mod.MyModTest()
	fmt.Println(message)
}
