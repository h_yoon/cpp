/*
 * timer sample
 * timer는 블록 방식 된다. 
 * timer는 취소가 가능하여 timer2가 go루틴에서 2초뒤 만료되지 않고 취소 된다.
 */
package main

import "time"
import "fmt"

func main() {

	fmt.Println("nomal case")
	timer1 := time.NewTimer(time.Second *2)

 	<-timer1.C
	fmt.Println("Timer 1 expired")
	

	timer2 := time.NewTimer(time.Second)
	go func() {
		<-timer2.C
		fmt.Println("Timer 2 expired")
	}()
	stop2 := timer2.Stop()
	if stop2 {
		fmt.Println("Timer 2 stopped")
	}
}

