/// blocking queue testing.

package BlockingQueue

import (
	"fmt"
	"time" /// for sleep
)

type BlockingQueue struct {
	queue chan (string) // 채널 선언
}

func (q *BlockingQueue) start() {
	q.queue = make(chan string) // 채널 할당.

	fmt.Println("BlockingQueue start")
	go q.run()
}

func (q *BlockingQueue) run() string {

	for {
		select {
		case msg := <-q.queue:
			fmt.Println("recv message", msg)
		default:
		}
		time.Sleep(1 * time.Second)
	}
}
