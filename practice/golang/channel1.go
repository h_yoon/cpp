

package main

import (
    "fmt"
)

type MyStruct struct {
    Channel chan string
}

func main() {
    m := MyStruct{
        Channel: make(chan string),
    }

    m.Channel <- "Hello, world!"

    fmt.Println(<-m.Channel)
}

type sender struct {
  Channel chan string
}


