package reactor

import (
	"fmt"
	"io"
	"net"
)

type Handler interface {
	handle_connect() bool
	handle_disconnect(addr string) bool
	handle_error(addr string) bool
	handle_close() bool
	handle_write(msg string) bool
	handle_read(msg string) string
}

type TcpReactor struct {
	conn    net.Conn
	send    chan ([]byte)
	handler *Handler

	addr   string
	is_run bool
}

var tcp_reactor TcpReactor

func Reactor(h *Handler) *TcpReactor { /// 모듈 import 시 init() 자동 호출.
	tcp_reactor.handler = h
	tcp_reactor.init(":8080")

	return &tcp_reactor
}

func write(msg string) bool {
	tcp_reactor.send <- []byte(msg)
	return true
}

func (r *TcpReactor) init(addr string) {
	r.addr = addr

	r.is_run = true
	go r.run()
}

func (r *TcpReactor) process_read(conn net.Conn) {
	recv := make([]byte, 4096)

	for {
		n, err := conn.Read(recv)

		addr := conn.RemoteAddr().String()
		if err != nil {
			if err == io.EOF {
				r.handler.handle_disconnect(addr)
			} else {
				r.handler.handle_error(addr)
			}
			break
		}

		if n > 0 {
			r.handler.handle_read(string(recv[:n]))
			// conn.Write(recv[:n])
		}
	}
}

func (r *TcpReactor) process_write(conn net.Conn) {
	r.send = make(chan []byte)

	for {
		select {
		case msg := <-r.send:
			conn.Write(msg)
		}
	}
}

func (r *TcpReactor) react(conn net.Conn) {
	go r.process_write(conn)
	go r.process_read(conn)
}

func (r *TcpReactor) run() bool {
	l, err := net.Listen("tcp", r.addr)
	if err != nil {
		fmt.Println("Fail to Listen", err)
		return false
	}
	defer l.Close()

	for {

		if r.is_run == false {
			break
		}

		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Fail to Accept:", err)
			continue
		}

		r.handler.handle_connect()

		r.react(conn)
	}

	return true
}
