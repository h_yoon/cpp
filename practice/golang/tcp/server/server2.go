package main

import (
	"fmt"
	"io"
	"net"
	"time"
)

type Handler interface {
	on_connect() bool
}

type ReactHandler interface {
	handle_connect() bool
	handle_disconnect() bool
	handle_error() bool
	handle_close() bool
	handle_write(msg string) bool
	handle_read(msg string) string
}

type Reactor struct {
	conn    net.Conn
	handler *Handler
	send    chan ([]byte)
}

func (r *Reactor) handle_connect() bool {

	return true
}

func (r *Reactor) handle_disconnect(addr string) bool {

	fmt.Println("connection is closed from client:", addr)
	return true
}

func (r *Reactor) handle_error(addr string) bool {

	fmt.Println("connection addr ", addr)
	return true
}

func (r *Reactor) handle_read(msg string) bool {

	fmt.Println("received ", msg)
	r.handle_write("send!" + msg)
	return true
}

func (r *Reactor) handle_write(msg string) bool {

	r.send <- []byte(msg)
	return true
}

func (r *Reactor) process_read(conn net.Conn) {
	recv := make([]byte, 4096)

	for {
		n, err := conn.Read(recv)

		addr := conn.RemoteAddr().String()
		if err != nil {
			if err == io.EOF {
				r.handle_disconnect(addr)
			} else {
				r.handle_error(addr)
			}
			break
		}

		if n > 0 {
			r.handle_read(string(recv[:n]))
			//			conn.Write(recv[:n])
		}
	}
}

func (r *Reactor) process_write(conn net.Conn) {
	r.send = make(chan []byte)

	for {
		select {
		case msg := <-r.send:
			conn.Write(msg)
		}
	}
}

func (r *Reactor) react(conn net.Conn) {
	go r.process_write(conn)
	go r.process_read(conn)
}

func (r *Reactor) run() {

	l, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Println("Fail to Listen", err)
	}
	defer l.Close()

	for {
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Fail to Accept:", err)
			continue
		}

		r.handle_connect()

		go r.react(conn)
	}
}

func main() {

	reactor := Reactor{}
	go reactor.run()

	for {
		time.Sleep(1 * time.Second)
	}
}
