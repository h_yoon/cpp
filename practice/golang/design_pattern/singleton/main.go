package main

import "fmt"

// singleton구현을 원하는 struct
// 여기서는 간단한 int형의 count를 구현
type myclass struct {
	count int
}

var instance *myclass

func GetInstance() *myclass {

	if instance == nil {
		instance = new(myclass)
	}

	return instance
}

func (s *myclass) AddOne() int {
	s.count++
	return s.count
}

func main() {

	c1 := GetInstance()

	if c1 == nil {
		fmt.Println("error new is nil")
	}

	count := c1.AddOne()

	fmt.Println(count)

	c2 := GetInstance()

	if c1 == c2 {
		fmt.Println("c1 and c2 is same")
	}

}
