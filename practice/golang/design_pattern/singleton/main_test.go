package main

import "testing"

func TestGetInstance(t *testing.T) {

	c1 := GetInstance()

	if c1 == nil {
		t.Error("GetInstance() is nil")
	}

	currCount := c1.AddOne()

	if currCount != 1 {
		t.Errorf("Call first time to count %d\n", currCount)
	}

	c2 := GetInstance()

	if c2 != c1 {
		t.Error("not same object")
	}

	currCount = c2.AddOne()

	if currCount != 2 {
		t.Errorf("call second time to count %d\n", currCount)
	}
}
