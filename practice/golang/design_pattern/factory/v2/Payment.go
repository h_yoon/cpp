package Payment

import (
	"errors"
	"fmt"
)

type PayMethod interface {
	Pay(amount float32) string
}

const (
	watch = 1
	card  = 2
)

func FoundMethod(pay_type int32) (PayMethod, error) {

	switch pay_type {
	case watch:
		return new(Watch), nil
	case card:
		return new(Card), nil
	default:
		return nil, errors.New("Error Method type")
	}
}

type Watch struct{}
type Card struct{}

func (w *Watch) Pay(amount float32) string {
	return fmt.Sprintf("%0.2f payed using Watch\n", amount)
}

func (c *Card) Pay(amount float32) string {
	return fmt.Sprintf("%0.2f payed using Card\n", amount)
}
