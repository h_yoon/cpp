package Payment

import (
	"fmt"
	"testing"
)

func TestPayMethod(t *testing.T) {

	method, err := FoundMethod(watch)

	if err != nil {
		t.Fatal("error method watch")
	}

	msg := method.Pay(2000.2)

	fmt.Println(msg)

}
