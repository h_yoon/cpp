package factory

import (
	"strings"
	"testing"
)

func TestGetPaymentMethodCash(t *testing.T) {

	card, err := GetPaymentMethod(Cash)

	if err != nil {
		t.Fatal("A payment method of type 'Cash' must exist")
	}

	msg := card.Pay(10.30)

	if !strings.Contains(msg, "payed using cash") {
		t.Error("the cash payment method message wasn't correct")
	}

	t.Log("Log:", msg)
}

func TestGetPaymentMethodDebitCash(t *testing.T) {

	card, err := GetPaymentMethod(DebitCard)
	if err != nil {
		t.Fatal("A payment mehtod of type 'DebitCard' must exist")
	}

	msg := card.Pay(10.30)

	if !strings.Contains(msg, "payed using debig card") {
		t.Error("the cash payment mehtod message wasn't correct")
	}

	t.Log("Log:", msg)
}

// 정의 되지 않은 상수값을 넘겨서 에러를 처리?
func TestGetPaymentMethodNonExistent(t *testing.T) {

	_, err := GetPaymentMethod(20)
	if err != nil {
		t.Fatal("A payment mehtod of type 'DebitCard must exist")
	}
	t.Log("Log:", err)
}
