/*
https://nghttp2.org/documentation/libnghttp2_asio.html#receive-server-push-and-enable-ssl-tls
ssl 참고 https://stackoverflow.com/questions/72795727/boost-asio-ssl-handshake-failure

"api.push.apple.com"
submit_request example. c version > https://nghttp2.org/documentation/tutorial-client.html
https://stackoverflow.com/questions/42837218/nghttp2-asio-connecting-to-apple-push-notification-times-out
*/


#include <iostream>
#include <nghttp2/asio_http2_client.h>
using boost::asio::ip::tcp;

using namespace nghttp2::asio_http2;
using namespace nghttp2::asio_http2::client;

int main(int argc, char *argv[]) {
  boost::system::error_code ec;
  boost::asio::io_service io_service;

  boost::asio::ssl::context tls(boost::asio::ssl::context::sslv23);

  // tls.set_verify_mode(boost::asio::ssl::context::verify_peer);
  tls.set_default_verify_paths();
  tls.use_private_key_file("Apns_Cert_DK_202400711.pem", boost::asio::ssl::context::pem);
  tls.use_certificate_file("Apns_Cert_DK_202400711.pem", boost::asio::ssl::context::pem);
  
  configure_tls_context(ec, tls); // ssl negotiated

  boost::asio::detail::throw_error( ec );

  std::cout << "start" << std::endl;

  std::string url = "api.push.apple.com";

  session sess(io_service, tls, url, "443");

  sess.on_connect([&sess, &io_service, &url](tcp::resolver::iterator endpoint_it)
  {
      boost::system::error_code ec;

      std::cout << "connected" << std::endl;

      header_map headers;

      std::string path_tail = "/3/device/4d9c38ac5475f64406d6de846ee50e486dfab7cb9d5b1a106be7dfcfcb99088f";
      std::string path = "https://" + url + path_tail;
      std::string data = R"({"aps":{"sound":"default","alert":"쪽지: 쪽지전송","badge":1},"message":"reason=mnote;idx=33593;"})";

      /* headers.emplace(":method",          (nghttp2::asio_http2::header_value)  {"POST"}   );
       headers.emplace(":path",            (nghttp2::asio_http2::header_value)  {path_tail}); */
      headers.emplace("apns-push-type",   (nghttp2::asio_http2::header_value)  {"alert"});
      headers.emplace("apns-topic",       (nghttp2::asio_http2::header_value)  {"com.dreamket.testfmc"});
      headers.emplace("apns-id",          (nghttp2::asio_http2::header_value)  {"abcdef12-3456-7890-abcd-000000000002"});
      headers.emplace("apns-expiration",  (nghttp2::asio_http2::header_value)  {"0"});
      /* headers.emplace(std::pair<std::string, header_value>("apns-priority", {"10", false})); */
      
      auto req = sess.submit(ec, "POST", path, data, headers);

      std::cout << "request " << data.length() << std::endl;
      req->on_response([&sess, &io_service](const response &res) 
      {
        std::cerr << "responsed " << res.status_code() << std::endl;
        for (auto &hv : res.header()) {
            std::cerr << hv.first << ": " << hv.second.value << "\n";
        }

        res.on_data([&sess, &io_service](const uint8_t *data, std::size_t len) 
        {
          std::cout << "data len " << len;
          std::cerr.write(reinterpret_cast<const char *>(data), len);
          std::cerr << std::endl;
          io_service.stop();
        });
      });

      req->on_push([](const request &push) 
      {
        std::cerr << "on push" << std::endl;

        push.on_response([](const response &res) 
        {
            std::cerr << "push response received!" << std::endl;
            res.on_data([](const uint8_t *data, std::size_t len) 
            {
                std::cerr.write(reinterpret_cast<const char *>(data), len);
                std::cerr << std::endl;
            });
        });
      });

    req->on_close( []( uint32_t error_code ){
        std::cerr << "Request close: " << error_code << std::endl;
    } );  
  });

  sess.on_error([](const boost::system::error_code &ec) 
  {
    std::cerr << "error: " << ec.message() << std::endl;
  });

  io_service.run();
}


