#include <iostream>

#include <nghttp2/asio_http2_client.h>

using boost::asio::ip::tcp;

using namespace nghttp2::asio_http2;
using namespace nghttp2::asio_http2::client;

int main(int argc, char *argv[]) {
  boost::system::error_code ec;
  boost::asio::io_service io_service;

  // connect to localhost:3000
  session sess(io_service, "localhost", "3000");

  sess.on_connect([&sess](tcp::resolver::iterator endpoint_it) {
    boost::system::error_code ec;

    auto printer = [](const response &res) {
      res.on_data([](const uint8_t *data, std::size_t len) {
        std::cerr.write(reinterpret_cast<const char *>(data), len);
        std::cerr << std::endl;
      });
    };

    std::size_t num = 3;
    auto count = std::make_shared<int>(num);

    for (std::size_t i = 0; i < num; ++i) {
      auto req = sess.submit(ec, "GET",
                             "http://localhost:3000/" + std::to_string(i + 1));

      req->on_response(printer);
      req->on_close([&sess, count](uint32_t error_code) {
        if (--*count == 0) {
          // shutdown session after |num| requests were done.
          sess.shutdown();
        }
      });
    }
  });

  sess.on_error([](const boost::system::error_code &ec) {
    std::cerr << "error: " << ec.message() << std::endl;
  });

  io_service.run();
}
