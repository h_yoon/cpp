//https://nghttp2.org/documentation/libnghttp2_asio.html#receive-server-push-and-enable-ssl-tls

#include <iostream>

#include <nghttp2/asio_http2_client.h>

using boost::asio::ip::tcp;

using namespace nghttp2::asio_http2;
using namespace nghttp2::asio_http2::client;

class Http2Client
{

public:
  Http2Client(boost::asio::io_service &service) 
  {
    boost::system::error_code ec;

    boost::asio::ssl::context tls_context(boost::asio::ssl::context::sslv23);
    tls_context.load_verify_file("Apns_Cert_ICE_202400714.pem");

    configure_tls_context(ec, tls_context);
    
    this->session_ = new session(service, tls_context, "api.push.apple.com", "443");  // api.push.apple.com
  }
  virtual ~Http2Client() = default;

  virtual bool client_connect() = 0;
  virtual void client_request() = 0;
  virtual bool client_response(const std::string &data) = 0;

  void connect() 
  {
    std::cout << __func__ << std::endl;

    session_->on_connect([&](tcp::resolver::iterator endpoint_it) 
    {
      connected = true;
      std::cout << "connected" << std::endl;

      request();
    });
  }

  void request() 
  {
    std::cout << __func__ << std::endl;

    if (connected == false) 
    {
      std::cout << "not connected" << std::endl;
      return;
    }

    request_ = session_->submit(error_code_, "POST",
                        "https://api.push.apple.com:443/" + std::to_string(++sequnce_id_));
    std::cout << "submit" << std::endl;

    //nghttp2_submit_request(session_, )    // nghttp2_submit_request = with stream data.
    request_->on_response([&](const response &res) 
    {
      std::cout << "on_response" << std::endl;

      res.on_data([=](const uint8_t *data, std::size_t len) 
      {

        if (len <= 0) 
        {
          std::cout << "recv data len 0" << std::endl;
          return;
        }

        std::string str((char*)data,len);

        client_response(str);
      });
    }
    );                    
  }

  void close();
  void on_error();
  bool connected = false;
  session *session_ = nullptr;
  bool connect_ = false;
  const nghttp2::asio_http2::client::request *request_;
  boost::system::error_code error_code_;
  int32_t sequnce_id_ = 1;

};

class Http2EvnetHandler : public Http2Client
{
public:
  Http2EvnetHandler(boost::asio::io_service &service) : Http2Client(service) {}
  virtual ~Http2EvnetHandler() = default;

  bool client_connect() { connect();}
  void client_request() { request();}
  bool client_response(const std::string &data) { std::cout << "get response:" << data << " len:" << data.length() << std::endl; return true;}
};

class Http2AsyncClient
{
public:
  Http2AsyncClient() { }
  virtual ~Http2AsyncClient() = default;

  Http2Client* client() 
  {
    Http2Client* client = new Http2EvnetHandler(io_service_);
    return client;
  }

  void start() {
    io_service_.run();
  }
private:
  boost::asio::io_service io_service_;
};


int main(int argc, char *argv[]) {

  Http2AsyncClient http2_async_client;


  auto client = http2_async_client.client();

  client->client_connect();
  
  //client->client_request();

  http2_async_client.start();

  sleep(1);
}

