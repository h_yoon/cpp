//build g++ mycoroutine1.cpp -o coro --std=c++20;./coro

#include <coroutine>
#include <iostream>

// 코루틴 개체
class MyCoroutine  // #2
{ 
public:
    struct MyPromise;
    using promise_type = MyPromise; // #2. 명칭이 promise_type인 종속 타입이 있어야 합니다.
private:
    std::coroutine_handle<promise_type> m_Handler; // #3. 코루틴 핸들
public:
    MyCoroutine(std::coroutine_handle<promise_type> handler) : m_Handler(handler) {}
    ~MyCoroutine() 
    {
        if (m_Handler) 
        {
            m_Handler.destroy(); // #3. 코루틴 개체에서 소멸해야 합니다.  
        }
    }
    MyCoroutine(const MyCoroutine&) = delete;
    MyCoroutine(MyCoroutine&&) = delete;
    MyCoroutine& operator =(const MyCoroutine&) = delete;
    MyCoroutine& operator =(MyCoroutine&&) = delete;

    void Resume() const  // #2. 일시 정지된 코루틴을 재개합니다.
    {
        if (!m_Handler.done())  // 코루틴이 종료되지 않았다면
        {
            m_Handler.resume(); // 재개합니다.
        }
    } 

    // Promise 개체
    struct MyPromise  // #4
    {
        MyPromise() = default;
        ~MyPromise() = default;
        MyPromise(const MyPromise&) = delete;
        MyPromise(MyPromise&&) = delete;
        MyPromise& operator =(const MyPromise&) = delete;
        MyPromise& operator =(MyPromise&&) = delete;

        MyCoroutine get_return_object()  // #4. 코루틴 개체를 리턴합니다.
        {
            std::cout << __func__ << std::endl;
            return MyCoroutine{std::coroutine_handle<MyPromise>::from_promise(*this)};
        }
        auto initial_suspend() 
        {
          std::cout << __func__ << std::endl;
          return std::suspend_always{}; // #5. 코루틴 함수 실행 초기의 일시 정지 상태입니다.
        }
        auto final_suspend() noexcept 
        {
          std::cout << __func__ << std::endl;
        //return std::suspend_always{}; //#6. 코루틴 함수 종료시 일시 정지 상태입니다.
          return std::suspend_always{}; 
        }
        void unhandled_exception() {} // 예외 발생시 호출됩니다.
    };
};

// 코루틴 함수
MyCoroutine CoroutineFunc()   // MyCoroutine 개체를 사용하는 코루틴 함수 
{
    std::cout << "CoroutineFunc start" << std::endl;
    co_await std::suspend_always{}; // #1
    std::cout << "CoroutineFunc process" << std::endl;
    co_await std::suspend_always{}; // #1
    std::cout << "CoroutineFunc end" << std::endl;
}

int main()
{

  std::cout << "Call #1" << std::endl;
  MyCoroutine object = CoroutineFunc();
  std::cout << "resume #1" << std::endl;
  object.Resume();
  std::cout << "resume #2" << std::endl;
  object.Resume();
  std::cout << "resume #3" << std::endl;
  object.Resume();
  std::cout << "last " << std::endl;
  object.Resume();
  std::cout << "end" << std::endl;
  object.Resume();
  std::cout << "end2" << std::endl;
  object.Resume();

  return 0;
}
