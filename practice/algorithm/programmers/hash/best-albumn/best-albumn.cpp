// best-albumn.cpp
// 2021.10.20.  hsy
//
// https://programmers.co.kr/learn/courses/30/lessons/42579
// score: 33/100


#include <string>
#include <vector>
#include <map>
#include <set>
#include <iostream>
using namespace std;


/// map<장르,조회수>
using MapGenre =std::map<size_t, std::string,std::greater<size_t>>;

MapGenre music;     /// 조회수가 많은 장르별로 정렬

#define music_count 2  /// 각 앨범당 최대 뮤직수

/// 조회수별 장르 자료구조 저장
void
genres_rank(const std::vector<string> &genres, const std::vector<int> &plays)
{
    int i = 0;
    std::map<std::string, size_t> tmp_map;
    for(const auto& genre : genres)
    {
        tmp_map[genre]+=plays[i];
        i++;
    }
    for(const auto& map : tmp_map)
    {
        music.insert(std::make_pair(map.second,map.first));
    }
}

vector<int>
solution(std::vector<std::string> genres, std::vector<int> plays)
{
    genres_rank(genres, plays);
    vector<int> answer;
    int rank = 1;
    for(auto& genre : music)
    {
        std::map<int,std::set<int>,greater<size_t>> musics; /// 같은 장르 정렬 맵<재생수,<set<배열idx>>
        for (int i=0; i<genres.size(); ++i)
        {
            if (genre.second.compare(genres[i]) == 0)
            {
                musics[plays[i]].insert(i);
            }
        }

        int count = 0;
        for(auto &one : musics)
        {
            for (auto m :one.second)
            {
                if(answer.size() >= music.size() *music_count)
                    return answer;

                answer.push_back(m);
                rank++;
                count++;
                if (count >= music_count)
                    continue;
            }
        }
    }

    return answer;
}

int main()
{
	std::vector<std::string> genres = {"classic", "pop", "classic", "classic", "pop"};
	std::vector<int> plays = {500, 600, 150, 800, 2500};
	///return {4,1,3,0}
	//
	std::vector <int> ret;
	ret = solution(genres, plays);
	
	for (const auto &one : ret)
		std::cout << one << "," << std::endl;

	return 0;
}


