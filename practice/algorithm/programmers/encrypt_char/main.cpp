//https://programmers.co.kr/learn/courses/30/lessons/60057

#include <string>
#include <vector>
#include <iostream>
using namespace std;

class Manager 
{
public:
  Manager(const std::string &str) : str_(str) {
      str_len_ = str_.length();
      std::cout << "str_len_:" <<str_len_ << std::endl;
  }
  int run();
private:
 int  check(const int& num);
 bool compare(const int &start, const int &len);

 int str_len_;
 std::string str_;
};

int 
Manager::run()
{
  int min_len = str_len_;
  int minimum = str_len_;
  for (int i=0; i<str_len_; ++i)
  {
    int tmp_len = check(i);
    if (tmp_len < min_len) {
      min_len = tmp_len;
      minimum = i;
    }
  }
  return minimum;
}

int
Manager::check(const int &num) 
{
  int len = 0;
  int dup = 0;
  std::string p1 = str_.substr(0,num);
  std::cout << "print:" <<p1 << std::endl;
  for (int i=1; i<str_len_/num; ++i)
  { 
    std::string p2 = str_.substr(i*num,num);
    if(p1.compare(p2)==0) 
    {
      dup++;
    }
    else
    {
      if(dup) len +=1; // 중복된경우 +중복횟수길이 (TODO 10넘어갈경우 ..)
        
      len += num;  // 문자길이
      dup=0;
    }
  }
  return len;
}

int 
main(int argc, const char** argv) {
    int answer = 0;
    std::string str1 = argv[1];    
    Manager manager(str1);
    answer = manager.run();
    return answer;
}
