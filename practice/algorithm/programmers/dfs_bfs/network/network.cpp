// network.cpp
// 2021.10.22.  hsy
//
// https://programmers.co.kr/learn/courses/30/lessons/43162
// score: 100/100

#include <string>
#include <vector>
#include <stack>
#include <set>
#include <utility>
#include <iostream>

using namespace std;

using MP = std::pair<int, int>;
int max_depth;  // 1~n

vector<vector<int>> coms;

std::stack<MP> gstack;
int room[201] ={0,};
int connection;
int gtime = 1;
void dfs(int x, int y)  // x = depth
{
    
    // find sub connection
    fprintf(stderr,"dfs: x %d, y %d\n", x,y);
    for ( int i=0; i< max_depth; ++i)   // i = y 
    {
        if (i == x && room[i] == 0)
        {
            room[i] = gtime;
            continue;
        }
        else if ( room[i] > 0 || coms[x][i] ==0) 
        {
            fprintf(stderr,"pass= x %d, y %d\n", x,i);
            continue;  // same with itself, visited and not connected
        }
        else 
        {
            if (coms[x][i] ==1 && coms[i][x] == 1 && room[i] == 0)
            {
                fprintf(stderr,"checked x %d, y %d\n",x,i);
                connection++;
                room[i] = gtime;        // next location visit check
                gstack.push(MP(x,y));  // save current 
                fprintf(stderr,"next= x %d, y %d\n", i,0);
                dfs(i,0);              // next computer check 
            }
        }
    }
    
    //no connection 
    if (gstack.empty() == false )
    {
        
        MP next = gstack.top();
        gstack.pop();
        dfs(next.first, next.second);
    }else{
        return;
    }
}

int solution(int n, std::vector<std::vector<int>> computers) {
    max_depth = n;
    int answer = 0;
    connection = 0;
    coms = computers;
    
    for ( int i=0; i< n; ++i)
    {
        fprintf(stderr,"--------\n");
        dfs(i,0);
        gtime = gtime+1;
    }
    
    std::set<int> ret;
    for ( const auto &i : room)
        if ( i != 0 )
            ret.insert(i);
    
    return ret.size();
}

int main()
{
    std::vector<std::vector<int>> ary = {{1, 1, 0}, {1, 1, 0}, {0, 0, 1}};  // case 1 return 2
    // std::vector<std::vector<int>> ary = {{1, 1, 0}, {1, 1, 1}, {0, 1, 1}};  // case 2 return 1

    int ret = solution(3, ary);

    std::cout << "answer:" << ret << std::endl;
}