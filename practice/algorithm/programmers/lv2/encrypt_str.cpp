//https://programmers.co.kr/learn/courses/30/lessons/60057

#include <string>
#include <vector>
#include <iostream>
using namespace std;

class Manager 
{
public:
  Manager(const std::string &str) : str_(str) {
      str_len_ = str_.length();
      std::cout << "str_len_:" <<str_len_ << std::endl;
  }
  int run();
private:
 int  check(const int& num);
 bool compare(const int &start, const int &len);
 
 int str_len_;
 std::string str_;
};

int 
Manager::run()
{
  int minimum = str_len_;
  for (int i=1; i<=str_len_/2; ++i) 
  {
    int tmp_len = check(i);
    if (tmp_len < minimum) {
      minimum = tmp_len;
    }
  }
  return minimum;
}

int
Manager::check(const int &num) 
{
  bool turnOff=false;
  int len=0;
  int dup=1; // 초기값은 0이아닌 1임.
  int i=0;
  int end = str_len_/num;
  std::string p1 = str_.substr(0,num); // 첫번째 스트링 

  for (i=1; i<=end; ++i)
  { 
    std::string p2 = str_.substr(i*num,num); // 두번째 스트링
    turnOff = false;
    if(p1.compare(p2)==0)  //중복된경우
    {
      dup++;
    }
    else  // 중복되지 않은 경우 
    {
     if(dup>1)
      turnOff = true;
    }

    // std::cout << "p1:"<<p1 << " p2:"<< p2 << " len:"<<len<< " dup:"<<dup<< " turnOff:"<< turnOff << std::endl;
      
    if (turnOff) // 중복이 끝나는 경우
    { 
      std::string str_dup = std::to_string(dup);
      len += str_dup.size() + num; // 중복된경우 +중복횟수길이
      dup = 1;
    }
    else
    {
      if(dup==1)     // 중복이 아닌경우만 길이 덧셈.
        len += num;
    }
     p1.assign(p2); // 첫번째에 두번째 스트링 입력

  }
  
  i -=1;
  if (i*num < str_len_) // 남은 길이 추가
  {
      len += str_len_ - (i*num);
  }

  // std::cout  << std::endl;
  return len;
}

int solution(string s) {
    int answer = 0;
    Manager manager(s);
    answer = manager.run();
    return answer;
}
