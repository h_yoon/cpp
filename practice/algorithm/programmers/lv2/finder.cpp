//https://programmers.co.kr/learn/courses/30/lessons/42888
#include <string>
#include <vector>
#include <map>
#include <iostream>

using namespace std;


struct User {
    User(const std::string &_id, const std::string &_action) : id(_id), action(_action) {}
    std::string id;
    std::string action;
    std::string& get_id(){ return id; }
    std::string& getAction() { return action; }
};

class Manager {
public:
    Manager(const std::vector<std::string> &record) {
       parse(record);
    }
    std::vector<std::string> run();
    
private:
    void parse (const std::vector<std::string> &records);
    void parser(const std::string &str);
    std::vector<std::string> print();
private:
    std::vector<User>                   record_; // {{id,action},}
    std::map<std::string,std::string>   ids_;    // id,name
};

void 
Manager::parse(const std::vector<std::string> &records) {
    for (const auto &record : records) {
        parser(record);
    }
}

void 
Manager::parser(const std::string &str) {
    const char* str_ptr = str.c_str();
   
    std::vector<std::string> ret;
    std::string::size_type start=0, ptr=0;
    for ( int i=0; i<3; ++i) {
        ptr = std::string(str_ptr+start).find(" ");
        //std::cout << "debug " << std::string(str_ptr+start) << ":" << str.substr(start,ptr) << ", ptr:" << ptr<< std::endl;
        ret.push_back(str.substr(start,ptr));
        start += ptr +1;
        if(ptr == std::string::npos) {
           break;
        }
    }
    
    if (ret[0].compare("Leave") !=0) 
        ids_[ret[1]] = ret[2];
    
    if (ret[0].compare("Change") != 0)
        record_.push_back(User(ret[1],ret[0]));
}

std::vector<std::string>
Manager::run() {
    std::string str, action;
    std::vector<std::string> ret;
    
    for (User& user : record_) {
        if (user.getAction().compare("Leave")==0) action = "나갔습니다.";
        else if (user.getAction().compare("Enter")==0) action = "들어왔습니다.";
        
        str = ids_[user.get_id()] +"님이 " + action;
        ret.push_back(str);
    }
    return ret;
}

vector<string> solution(vector<string> record) {
    Manager manager(record);
    return manager.run();
}
