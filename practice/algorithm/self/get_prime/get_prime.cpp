

#include <iostream>
#include <string>
#include <vector>



void get_prime(std::vector<int> &ary, const int &target)
{
  bool flag;
  for (int i=4; i<target; ++i)
  {
    flag = true;
    for (int j=2; j<i/2; ++j)
    {
       if (i%j==0)  //// ex) 100 mod 3 = 1
       {
         flag = false;
         break;
       }
    }
    if (flag == true)
      ary.push_back(i);
  }
}

int main(int argc, char **argv)
{

  if (argc < 2)
  {
     std::cout << "need argument" << std::endl;
     return 0;
  }
  else
  {
    std::cout << "start by " << argv[1] << std::endl;
  }


  int target = atoi(argv[1]);

  std::vector<int> ary;

  get_prime(ary, target);

  for ( const auto &one : ary)
   std::cout << one << std::endl;

  return 0;

}
