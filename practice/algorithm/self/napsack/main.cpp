
// calculate the max value with N weight.

/*
 {{weight}, {value}}
 N = 10
*/

#include <iostream>

int max=10;
int nc = 5; // the number of value  
int ary[2][5] = {{3,4,1,2,3},{2,3,2,3,6}};

class Manager {

public:
  void run() {
    int ret = napsack(0,0,0);
    std::cout << "biggest value:" << ret << std::endl;
  }
  int napsack(int n, int w, int v);

};

int
Manager::napsack(int n, int w, int v) 
{
  // 1. end n size over and weight over max
  if (n >= nc | w >= max) return v;

  // buy, not buy 
  return std::max( napsack(n+1,w,v), napsack(n+1,w+ary[0][n], v+ary[1][n]));
}

int main() {
  Manager mgr;
  mgr.run();
  return 0;
}
