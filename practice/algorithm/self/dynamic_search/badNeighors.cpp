// BadNeighbors. topcoder p235
// 
/*
question

issue
  단순히 번갈아 가면서 합산하면 될꺼같지만, 순서를 바꾸지 않고 합산 했을때, 더큰 수가 나오면 순서를 바꾸어야한다.
  > 순서를 바꾼다, 안바꾼다
*/

#include <vector>
#include <iostream>
#include <string>
                                        
//std::vector<std::vector<int>> aries = { {1,2,3,4,5,1,2,3,4,5}};//16 (3,5,3,5)
                                        // input/answer
std::vector<std::vector<int>> aries = {{10,3,2,5,7,8}, //19
                                       {11,15},        //15
                                       {7,7,7,7,7,7,7},//21
                                       {1,2,3,4,5,1,2,3,4,5},//16 (3,5,3,5)
                                       {94,40,49,65,21,21,106,80,92,81,679,4,61,6,237,12,72,74,29,95,265,35,47,1,61,397,52,72,37,51,1,81,45,435,7,36,57,86,81,72} //2926
                                      };

class Manager {
public:
  Manager() {}
  ~Manager() = default;

  int run(const std::vector<int> &input);

private:
  int start(const int startPoint, int calPoint, int sumValue);
  int checkNeighbor(const int &startPoint, int calPoint, int sumValue);  /// start point in ary, calculate point, sum value
  int len=0;
  std::vector<int> ary;

};

int 
Manager::run(const std::vector<int> &input) {
  ary = input; 
  len = ary.size()-1; /// len starts at 0
  std::cout << "len:" << len << std::endl;
  int ret = std::max(start(0,0,0), start(1,1,0));
  ret = std::max(ret, start(2,2,0));
  std::cout << "result: " << ret << std::endl; 
  return ret;
}

int 
Manager::start(const int startPoint, int calPoint, int sumValue) {
  return checkNeighbor(startPoint, calPoint, sumValue);

}

int 
Manager::checkNeighbor(const int &startPoint, int calPoint, int sumValue) {
  
  if (calPoint > len) /// end if loop over
    return sumValue;
  else if (startPoint+len == calPoint) /// 첫번째 이웃이 계산된 경우라면, 마지막 이웃의 값은 더하지 못한다.
    return sumValue;
  
  sumValue += ary[calPoint];
  //std::cout <<"sum:"<< sumValue << " ary["<< calPoint <<"]:" << ary[calPoint] << " sp:" << startPoint << " len:" << len <<   std::endl;
  return std::max(checkNeighbor(startPoint, calPoint+2, sumValue), checkNeighbor(startPoint, calPoint+3, sumValue));
   
}


int main() {

  Manager mgr;
  for (auto &ary : aries) {
    mgr.run(ary);
  }

  return 0;
}


