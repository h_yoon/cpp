// CorporationSalary. topcoder p226
// question

/* input /returns
1) {N} /1
2) {{"NNYN"}, {"NNYN"}, {"NNNN"}, {"NYYN"}} /5
3) {{"NNNNN"}, {"YNYNNY"}, {"YNNNNY"}, {"NNNNNN"}, {"YNYNNN"}, {"YNNYNN"}} /17
*/

#include <vector>
#include <string>
#include <iostream>



class Manager {
public:
  Manager() { 
    for (int i=0; i<sc; ++i) 
      salaries[i] = -1;
  }
  ~Manager() = default;

  int run();

private:
  int getSalary(int n);

  static const int sc = 10;
  int salaries[sc];  /// ary={-1,} -> {-1,0,0,0,0} 의 의미임.  {-1,-1,-1,-1}로 생각했었따.........

  std::vector<std::string> ary = {{"NNYN"}, {"NNYN"}, {"NNNN"}, {"NYYN"}};
};

int 
Manager::run() {
  
  int m = 0;
  
  for(int i=0; i<ary.size(); ++i) {
    //std::cout << "run: " << i << std::endl;
    m += getSalary(i);
  }

  return m;
}

int 
Manager::getSalary(int n) {
  
  if (salaries[n] != -1 && n < this->sc)  /// 기 계산된 데이터 사용
  {
    return salaries[n]; 
  }

  int money = 0; /// 급여 
  int cnt = 0;   /// 부하의 수 

  for(int i=0; i<ary[n].length(); ++i)
  {
    if (i==n) continue; /// 자신의 data 패스
  
    //std::cout << "check value["<< n << "][" << i <<"]:" << ary[n][i] << std::endl;
    
    if (ary[n][i] == 'Y') { /// 자신의 부하직원인 경우 급여 계산
      money += getSalary(i);
      cnt++;
    }
  }

  if (cnt==0) money=1; ///부하직원이 없는 경우

  //std::cout << "save result["<< n << "]:" << money << std::endl;
  salaries[n] = money;
  return money;
}


int main() 
{
  Manager mgr;
  std::cout << "result: " << mgr.run() << std::endl;
}

