// ChessMetric TopCoder p247
// 2022. 5. 10 practice
/* 
question
  kingNight can Move L or X from K 
  return the count of way to move to specific point from input point with 'numMoves'
 
.......
..L.L..
.LXXXL.
..XKX..
.LXXXL.
..L.L..
......

input / answer
1) size=3
start={0,0}
end={1,0}
numMoves=1
return=1

2) size=3
start={0,0}
end={1,2}
numMoves=1
return=1

3) size=3
start={0,0}
end={2,2}
numMoves=1
return=0

4) size=3
start={0,0}
end={0,0}
numMoves=2
return=5

5) size=100
start={0,0}
end={0,99}
numMoves=50
return=243097310072600
*/

#include <iostream>
#include <vector>


struct Info {
  Info() {}
  Info(int sz, int sx, int sy, int ex, int ey, int num) : sz(sz), sx(sx), sy(sy), ex(ex), ey(ey), num(num) {}

  Info(const Info &r) {
    sz = r.sz;
    sx = r.sx;
    sy = r.sy;
    ex = r.ex;
    ey = r.ey;
    num = r.num;
  }
  int sx, sy, ex, ey, sz, num;
};

class Manager {
public:
  Manager() {};
  ~Manager() {};

  void operator()(Info &_info) { 
    //memo = {};
    info = _info;
  }
  long run();

private:
  long find(int x, int y, int wn, int mn);
  Info info;
  bool memo[100][100][55] = {{{false}}};
};

long Manager::run() {
  long cnt = 0;
  cnt = find(info.sx, info.sy, 0, 0);
  std::cout << "result: " << cnt << std::endl;
  return cnt;
}

long 
Manager::find(int x, int y, int wn, int mn) { // way number(wn), move number(mn)
  
  if(x >= info.sz || x < 0 || y >= info.sz || y < 0 )  /// 게임판을 벗어난 경우 0
  { 
    //std::cout << "out boundary" << std::endl;
    return 0;
  }

  if(memo[x][y][wn] == true) { /// 같은 길 방법으로 방문한 적이 있는 경우 탐색중단.
    std::cout << "stop, already visited" << std::endl;
    return 0; 
  }else if (mn > info.num) {       /// 이동횟수 초과 시 탐색 중단.
    std::cout << "stop, move count over" << std::endl;
    return 0; 
  }else {                      /// 아니면 방문했다고 기록
    std::cout << "info, check visit point. x:" << x << " y:"<< y << " wn:" << wn << std::endl;
    memo[x][y][wn] = true;
  }

  long ret=0;
  if (x==info.ex && y==info.ey && mn == info.num) { /// 주어진 횟수에 맞추어 도착한 경우 성공.
    ret = 1;
    std::cout << "arrived"<< std::endl;
  //}else if (x != info.ex && y != info.ey && mn < info.num ){ /// 경로 탐색
  }else if (mn < info.num ){ /// 경로 탐색
    ++mn;
    ret += find(x-1, y,   ++wn, mn);
    ret += find(x,   y-1, ++wn, mn);
    ret += find(x-1, y-1, ++wn, mn);
    ret += find(x+1, y,   ++wn, mn);
    ret += find(x,   y+1, ++wn, mn);
    ret += find(x+1, y+1, ++wn, mn);
    ret += find(x+1, y+1, ++wn, mn);

    ret += find(x-2, y-1, ++wn, mn);
    ret += find(x-1, y-2, ++wn, mn);
    ret += find(x+1, y-2, ++wn, mn);
    ret += find(x+2, y-1, ++wn, mn);

    ret += find(x+2, y+1, ++wn, mn);
    ret += find(x+1, y+2, ++wn, mn);
    ret += find(x-1, y+2, ++wn, mn);
    ret += find(x-2, y+1, ++wn, mn);
    std::cout << "search next depth. mn:" << mn << std::endl;
      
  }else {
    std::cout << "debug, x:" << x << " y:" << y << " ex:" << info.ex << " ey:" << info.ey << std::endl;
    std::cout << "stop, else case" << std::endl;
    ret = 0;  /// 기타 경우 모두 0;
  }

  return ret;
}

int main() {
  std::vector<Info> data;
  data.emplace_back(Info(3, 0,0, 0,0, 2));

  Manager mgr;
  for (auto &info : data) {
    std::cout << "start " << std::endl;
    mgr(info);
    mgr.run();
  }
  return 0;
}


