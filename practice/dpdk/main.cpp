
#include "DCommon.hpp"

#define LOG_DPDK

INF *inf = NULL;
DPort *dport = NULL;


static void
signal_handler(int signum)
{
	if (signum == SIGINT || signum == SIGTERM) {
		printf("\n\nSignal %d received, preparing to exit...\n",
				signum);
		inf->is_run_ = false;
	}
}
static int launch_lcore(__attribute__((unused)) void *dummy)
{
	inf->main_loop();	
	return 0;
}

/* Check the link status of all ports in up to 9s, and print them finally */
static void
check_all_ports_link_status(uint8_t port_num, uint32_t port_mask)
{
#define CHECK_INTERVAL 100 /* 100ms */
#define MAX_CHECK_TIME 90 /* 9s (90 * 100ms) in total */
	uint8_t portid, count, all_ports_up, print_flag = 0;
	struct rte_eth_link link;

	printf("\nChecking link status\n");
	fflush(stdout);
	for (count = 0; count <= MAX_CHECK_TIME; count++) {
		all_ports_up = 1;
		for (portid = 0; portid < port_num; portid++) {
			if ((port_mask & (1 << portid)) == 0)
				continue;
			memset(&link, 0, sizeof(link));
			rte_eth_link_get_nowait(portid, &link);
			/* print link status if flag set */
			if (print_flag == 1) {
				if (link.link_status)
					printf("Port %d Link Up - speed %u "
							"Mbps - %s\n", (uint8_t)portid,
							(unsigned)link.link_speed,
							(link.link_duplex == ETH_LINK_FULL_DUPLEX) ?
							("full-duplex") : ("half-duplex\n"));
				else
					printf("Port %d Link Down\n",
							(uint8_t)portid);
				continue;
			}
			/* clear all_ports_up flag if any link down */
			if (link.link_status == ETH_LINK_DOWN) {
				all_ports_up = 0;
				break;
			}
		}
		/* after finally printing all link status, get out */
		if (print_flag == 1)
			break;

		if (all_ports_up == 0) {
			printf(".");
			fflush(stdout);
			rte_delay_ms(CHECK_INTERVAL);
		}

		/* set the print_flag if all ports up or timeout */
		if (all_ports_up == 1 || count == (MAX_CHECK_TIME - 1)) {
			print_flag = 1;
			printf("done\n");
		}
	}
}

int main(int argc, char **argv)
{
	int ret;
	unsigned lcore_id;
	struct rte_mempool *mp = NULL;
#ifdef LOG_DPDK
		char fname[64] = {0,};
		sprintf(fname, "/app/app_home/src/DOBE/DPDK_%d",0);
		FILE *dpdklog = fopen(fname,"wt");
		if(dpdklog) rte_openlog_stream(dpdklog);
		rte_log_set_global_level(8U);
#endif

	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);

	inf = new INF();
	dport = &DPort::Instance();

	inf->Init(argc, argv);
	rte_kni_init(1); // 1 is the number of port using kni

	dport->InitPort(0);

	dport->StartPort(0);

	printf("end kniAlloc\n");
	dport->KniAlloc(0);

	unsigned lcore = 1;
	rte_eal_remote_launch(launch_lcore, NULL, lcore);

	check_all_ports_link_status(1,1);

	printf("kni cmd: ifconfig vEth0 192.168.122.124 up\n");
	system("ifconfig vEth0 192.168.122.124 up");
	printf("kni cmd end\n");

	inf->InitTimer();
	inf->Run();

	RTE_LCORE_FOREACH_SLAVE(lcore_id)
	{
		if (rte_eal_wait_lcore(lcore_id) < 0 ) {
			ret = -1;
			break;
		}
	}

	rte_eth_dev_stop(0);
	rte_eth_dev_close(0);

	return 0;
}

