
#include "DCommon.hpp"

extern INF *inf;
/* mask of enabled ports */
static uint32_t dobe_enabled_port_mask = 0;

/* list of enabled ports */
static uint32_t dobe_dst_ports[RTE_MAX_ETHPORTS];

static const char short_options[] =
	"p:"  /* portmask */
	"q:"  /* number of queues */
	"T:"  /* timer period */
	;

#define CMD_LINE_OPT_MAC_UPDATING "mac-updating"
#define CMD_LINE_OPT_NO_MAC_UPDATING "no-mac-updating"

#define MAX_TIMER_PERIOD 86400 /* 1 day max */

enum {
	/* long options mapped to a short option */

	/* first long only option value must be >= 256, so that we won't
	 * conflict with short options */
	CMD_LINE_OPT_MIN_NUM = 256,
};

static int mac_updating = 1;
static unsigned int dobe_rx_queue_per_lcore = 1;

static const struct option lgopts[] = {
	{ CMD_LINE_OPT_MAC_UPDATING, no_argument, &mac_updating, 1},
	{ CMD_LINE_OPT_NO_MAC_UPDATING, no_argument, &mac_updating, 0},
	{NULL, 0, 0, 0}
};


/* display usage */
static void
Dobe_usage(const char *prgname)
{
	printf("%s [EAL options] -- -p PORTMASK [-q NQ]\n"
	       "  -p PORTMASK: hexadecimal bitmask of ports to configure\n"
	       "  -q NQ: number of queue (=ports) per lcore (default is 1)\n"
		   "  -T PERIOD: statistics will be refreshed each PERIOD seconds (0 to disable, 10 default, 86400 maximum)\n"
		   "  --[no-]mac-updating: Enable or disable MAC addresses updating (enabled by default)\n"
		   "      When enabled:\n"
		   "       - The source MAC address is replaced by the TX port MAC address\n"
		   "       - The destination MAC address is replaced by 02:00:00:00:00:TX_PORT_ID\n",
	       prgname);
}

static void check_all_ports_link_status(uint8_t port_num, uint32_t port_mask)
{
#define CHECK_INTERVAL 100 /* 100ms */
#define MAX_CHECK_TIME 90 /* 9s (90 * 100ms) in total */
	uint8_t portid, count, all_ports_up, print_flag = 0;
	struct rte_eth_link link;

	printf("\nChecking link status");
	fflush(stdout);
	for (count = 0; count <= MAX_CHECK_TIME; count++) {
		all_ports_up = 1;
		for (portid = 0; portid < port_num; portid++) {
			if ((port_mask & (1 << portid)) == 0)
				continue;
			memset(&link, 0, sizeof(link));
			rte_eth_link_get_nowait(portid, &link);
			/* print link status if flag set */
			if (print_flag == 1) {
				if (link.link_status)
					printf("Port %d Link Up - speed %u "
						"Mbps - %s\n", (uint8_t)portid,
						(unsigned)link.link_speed,
				(link.link_duplex == ETH_LINK_FULL_DUPLEX) ?
					("full-duplex") : ("half-duplex\n"));
				else
					printf("Port %d Link Down\n",
						(uint8_t)portid);
				continue;
			}
			/* clear all_ports_up flag if any link down */
			if (link.link_status == ETH_LINK_DOWN) {
				all_ports_up = 0;
				break;
			}
		}
		/* after finally printing all link status, get out */
		if (print_flag == 1)
			break;

		if (all_ports_up == 0) {
			printf(".");
			fflush(stdout);
			rte_delay_ms(CHECK_INTERVAL);
		}

		/* set the print_flag if all ports up or timeout */
		if (all_ports_up == 1 || count == (MAX_CHECK_TIME - 1)) {
			print_flag = 1;
			printf("done\n");
		}
	}
}
                                                                                                                    
static int Dobe_parse_portmask(const char *portmask)
{
	char *end = NULL;
	unsigned long pm;

	/* parse hexadecimal string */
	pm = strtoul(portmask, &end, 16);
	if ((portmask[0] == '\0') || (end == NULL) || (*end != '\0'))
		return -1;

	if (pm == 0)
		return -1;

	return pm;
}

static unsigned int Dobe_parse_nqueue(const char *q_arg)
{
	char *end = NULL;
	unsigned long n;

	/* parse hexadecimal string */
	n = strtoul(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return 0;
	if (n == 0)
		return 0;
	if (n >= MAX_RX_QUEUE_PER_LCORE)
		return 0;

	return n;
}

static int Dobe_parse_timer_period(const char *q_arg)
{
	char *end = NULL;
	int n;

	/* parse number string */
	n = strtol(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return -1;
	if (n >= MAX_TIMER_PERIOD)
		return -1;

	return n;
}


static void Dobe_mac_updating(struct rte_mbuf *m, unsigned dest_portid)
{
	struct ether_hdr *eth;
	void *tmp;

	eth = rte_pktmbuf_mtod(m, struct ether_hdr *);

	/* 02:00:00:00:00:xx */
	tmp = &eth->d_addr.addr_bytes[0];
	*((uint64_t *)tmp) = 0x000000000002 + ((uint64_t)dest_portid << 40);

	/* src addr */
//	ether_addr_copy(&dobe_ports_eth_addr[dest_portid], &eth->s_addr);
}

/* Parse the argument given in the command line of the application */
static int Dobe_parse_args(int argc, char **argv)
{
	int opt, ret, timer_secs;
	char **argvopt;
	int option_index;
	char *prgname = argv[0];

	argvopt = argv;

	while ((opt = getopt_long(argc, argvopt, short_options,
				  lgopts, &option_index)) != EOF) 
	{

		switch (opt) 
		{
		/* portmask */
		case 'p':
			dobe_enabled_port_mask = Dobe_parse_portmask(optarg);
			if (dobe_enabled_port_mask == 0) {
				printf("invalid portmask\n");
				Dobe_usage(prgname);
				return -1;
			}
			break;

		/* nqueue */
		case 'q':
			dobe_rx_queue_per_lcore = Dobe_parse_nqueue(optarg);
			if (dobe_rx_queue_per_lcore == 0) {
				printf("invalid queue number\n");
				Dobe_usage(prgname);
				return -1;
			}
			break;

		/* timer period */
		case 'T':
			timer_secs = Dobe_parse_timer_period(optarg);
			if (timer_secs < 0) {
				printf("invalid timer period\n");
				Dobe_usage(prgname);
				return -1;
			}
			//timer_period = timer_secs;
			break;

		/* long options */
		case 0:
			break;

		default:
			Dobe_usage(prgname);
			return -1;
		}
	}

	if (optind >= 0)
		argv[optind-1] = prgname;

	ret = optind-1;
	optind = 1; /* reset getopt lib */
	return ret;
}
INF::INF()
{
	is_run_ = true;
	nb_sent_ = 0;
	nb_recv_ = 0;
}

INF::~INF()
{

}


int INF::Init(int argc, char **args)
{
	this->argc_ = argc;
	this->argv_ = args;

	is_run_ = true;

	EalInit();
	CreateMempool();
	/* init enabled_ports */
	avail_port_ = ParseArgs(argc_, argv_);
	printf("available port :%u\n",avail_port_);

	
	return 0;;

	//check_all_ports_link_status(nb_ports, dobe_enabled_port_mask);
}

int INF::EalInit()
{

	int ret=0;

	ret = rte_eal_init(argc_, argv_);

	if(ret< 0){
		rte_panic("Cannot init the EAL\n");
		return 1;
	}else{
		printf("EAL Init\n");
	}

	argc_ -= ret;
	argv_ += ret;

	return 0;
}

int INF::CreateMempool()
{	
	int ret=0;
	struct rte_mempool *mbuf_pool;
	mbuf_pool = rte_pktmbuf_pool_create("mbuf_pool", NB_MBUF, MEMPOOL_CACHE_SIZE, 0, RTE_MBUF_DEFAULT_BUF_SIZE, rte_socket_id());

	if(mbuf_pool == NULL)
		rte_exit(EXIT_FAILURE, "Cannot init mbuf pool\n");
	else{
		printf("Check ENV: %d, %d, %d, %d\n",NB_MBUF,MEMPOOL_CACHE_SIZE,RTE_MBUF_DEFAULT_BUF_SIZE,rte_socket_id());	
		printf("Created Mempool\n");
		ret = 1;
	}
	mp_info_[0].mp_=mbuf_pool;

	return ret;
}

uint8_t INF::ParseArgs(int argc, char **argv)
{
	int ret;
	uint8_t portid, last_port, nb_port, nb_ports_available, nb_ports;
	unsigned nb_ports_in_mask = 0, rx_lcore_id=0;
	struct lcore_queue_conf *qconf;
	struct rte_eth_dev_info dev_info;

	ret = Dobe_parse_args(argc,argv);

	if(ret < 0)
		rte_exit(EXIT_FAILURE, "Invalid L2FWD arguments\n");

	nb_ports = rte_eth_dev_count();
	printf("nb_ports:%u\n",nb_ports);

	last_port = 0;
	/* Each logical core is assigned a dedicated TX queue on each port. */
	for (portid = 0; portid < nb_ports; portid++) 
	{
		printf("1] portid:%u, nb_ports:%u\n",portid, nb_ports);
		//enabled_ports[portid] = 0;
		/* skip ports that are not enabled */
		if ((dobe_enabled_port_mask & ( 11 << portid)) ==0){
			printf("exit] dobe_enabled_port_mask:%u & ( 11 << %u)=%u\n",dobe_enabled_port_mask,portid, (11 << portid));
			continue;
		}

		if (nb_ports_in_mask % 2) {
			dobe_dst_ports[portid] = last_port;
			dobe_dst_ports[last_port] = portid;
			printf("2] nb_ports_in_mask(%d)%2=0\ndst_port[%d]=%d\ndst_port[%d]=%d\n",nb_ports_in_mask, portid, last_port, last_port, portid);
		}
		else{
			last_port = portid;
			printf("3] else\n last_port(%d) = portid(%d)\n",last_port, portid);
		}

	/* Initialize the port/queue configuration of each logical core */
		/* get the lcore_id for this port */
		while (rte_lcore_is_enabled(rx_lcore_id) == 0 || lcore_queue_conf[rx_lcore_id].n_rx_port == dobe_rx_queue_per_lcore) {
			rx_lcore_id++;
			if (rx_lcore_id >= RTE_MAX_LCORE)
				rte_exit(EXIT_FAILURE, "Not enough cores\n");
		}

		if (qconf != &lcore_queue_conf[rx_lcore_id])
			/* Assigned a new logical core in the loop above. */
			qconf = &lcore_queue_conf[rx_lcore_id];

		qconf->rx_port_list[qconf->n_rx_port] = portid;
		qconf->n_rx_port++;
		printf(">> Lcore %u: RX port %u\n", rx_lcore_id, (unsigned) portid);

		nb_ports_available = nb_ports;
	}

	nb_ports_in_mask++;

	/*
	printf("rte_eth_dev_info_get2:%u\n",portid);
	rte_eth_dev_info_get(portid, &dev_info);

	if (nb_ports_in_mask % 2) {
		printf("Notice: odd number of ports in portmask. nb_ports_in_mask:%u, last_port:%u\n",nb_ports_in_mask,last_port);
		dobe_dst_ports[last_port] = last_port;
	}
	*/
	return nb_ports_available;
}

void INF::CheckKNIStatus(void *_args)
{
	uint8_t port_id = 0;
	_Port_Info *port_info = NULL;
	port_info = inf->port_info_[port_id];

	char cmd[32];
	struct ifreq s;
	int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
	if (fd < 0)
	{
		printf("Cannot Open Socket to get KNI Info");
		return;
	}

	//RTE_ETH_FOREACH_DEV(port_id)
	{
		//port_info = &g_global_info.port_[port_id];

		memset(&s, 0x00, sizeof(struct ifreq));
		snprintf(s.ifr_name, RTE_KNI_NAMESIZE, "vEth%u", port_id);
		if (0 == ioctl(fd, SIOCGIFFLAGS, &s))
		{
			if (!(s.ifr_flags & IFF_UP))
			{
				cmd[0] = '\0';
				snprintf(cmd, 32, "ifconfig %s up", s.ifr_name);

				printf("KNI[%u] is Down\n", port_id);
				printf("Send KNI UP Command (%s)\n", cmd);

				system(cmd);
			}
		}
	}

	if (fd)
		close(fd);
}

int INF::InitTimer()
{
	printf("InitTimer_\n");
	atom::CDateTime start_time;
	sync_timer_.Create(INF::CheckKNIStatus, NULL,start_time, 3, atom::ENUM_DATE_TYPE_SEC);
	return 0;
}

int INF::Run()
{
	
	while(is_run_)
	{
		sync_timer_.CheckTimer();

		usleep(3000);
	}

}
void INF::main_loop(void)
{
	uint8_t portid=0;
	uint8_t queueid=0;
	int sent, recv;
	int oldsent=0, oldrecv=0;
	struct rte_eth_dev_tx_buffer* tx_buffer=NULL;
	struct rte_mbuf *pkts_burst[MAX_PKT_BURST];
	
	while(is_run_)
	{
		/* tx queue drain */
		sent=0;
		tx_buffer = this->tx_buffer[portid];
		sent = rte_eth_tx_buffer_flush(portid, queueid, tx_buffer);
		
		if(sent)
		{
			nb_sent_ += sent;
		}

		/* read packet from rx queue */
		recv=0;
		recv = rte_eth_rx_burst(portid, queueid, pkts_burst, MAX_PKT_BURST); 

		if(recv)
		{
			nb_recv_ += recv;
		}

		if( (nb_sent_ || nb_recv_) &&
			(nb_sent_ != oldsent || nb_recv_ != oldrecv))
		{
			oldsent = nb_sent_;
			oldrecv = nb_recv_;
			//printf("Tx:%u\nRx:%u\n",nb_sent_,nb_recv_);
		}
	}
}


