

#include "DCommon.hpp"

#define NB_MBUF   8192
#define MEMPOOL_CACHE_SIZE 256

#define MAX_PKT_BURST 32
#define BURST_TX_DRAIN_US 100 /* TX drain every ~100us */
#define MEMPOOL_CACHE_SIZE 256

/*
 * Configurable number of RX/TX ring descriptors
 */
#define RTE_TEST_RX_DESC_DEFAULT 128
#define RTE_TEST_TX_DESC_DEFAULT 512

extern INF *inf;

static uint16_t nb_rxd = RTE_TEST_RX_DESC_DEFAULT;
static uint16_t nb_txd = RTE_TEST_TX_DESC_DEFAULT;

/* ethernet addresses of ports */
static struct ether_addr dobe_ports_eth_addr[RTE_MAX_ETHPORTS];


static uint64_t timer_period = 10; /* default period is 10 seconds */

//static struct rte_eth_dev_tx_buffer *tx_buffer[RTE_MAX_ETHPORTS];

//struct rte_mempool * dobe_pktmbuf_pool = NULL;

/* Per-port statistics struct */
struct dobe_port_statistics {
	uint64_t tx;
	uint64_t rx;
	uint64_t dropped;
} __rte_cache_aligned;

struct dobe_port_statistics port_statistics[RTE_MAX_ETHPORTS];

/* Check the link status of all ports in up to 9s, and print them finally */


DPort::DPort()
{
}

DPort::~DPort()
{

}

DPort *DPort::instance_ = NULL;

DPort &DPort::Instance(void)
{
	if(instance_ == NULL)
		instance_ = new DPort();
	return *instance_;
}


int DPort::Init(int argc, char** argv)
{
	return 1;
}


int DPort::InitPort(uint8_t portid)
{

	int ret;
	_Port_Info *port;
	rte_mempool *mp;
	struct rte_eth_dev_tx_buffer **tx_buffer;

	port = new _Port_Info;

	memset(port, 0, sizeof(_Port_Info));

	port->dev_conf_ = new struct rte_eth_conf;
	memset(port->dev_conf_,0x0, sizeof(struct rte_eth_conf));
	port->dev_conf_->rxmode.header_split = 0;
	port->dev_conf_->rxmode.hw_ip_checksum = 0;
	port->dev_conf_->rxmode.hw_vlan_filter = 0;
	port->dev_conf_->rxmode.jumbo_frame = 0;
	port->dev_conf_->rxmode.hw_strip_crc = 1;
	port->dev_conf_->txmode.mq_mode = ETH_MQ_TX_NONE;

	inf->port_info_[portid] = port;

	/*
	 * Ethernet device configuration.
	 */
	struct rte_eth_rxmode rx_mode;
	memset(&rx_mode, 0x00, sizeof(struct rte_eth_rxmode));
	rx_mode.max_rx_pkt_len = ETHER_MAX_LEN;
	rx_mode.header_split = 0;
	rx_mode.split_hdr_size = 0;
	rx_mode.hw_ip_checksum = 0;
	rx_mode.hw_vlan_filter = 0; // 1
	rx_mode.hw_vlan_strip = 0; // 1
	rx_mode.hw_vlan_extend = 0;
	rx_mode.jumbo_frame = 0;
	rx_mode.hw_strip_crc = 1;
	struct rte_fdir_conf fdir_conf;
	memset(&fdir_conf, 0x00, sizeof(struct rte_fdir_conf));
	fdir_conf.mode = RTE_FDIR_MODE_NONE;
	fdir_conf.pballoc = RTE_FDIR_PBALLOC_64K;
	fdir_conf.status = RTE_FDIR_REPORT_STATUS;
	fdir_conf.mask.vlan_tci_mask = 0x0;
	fdir_conf.mask.ipv4_mask.src_ip = 0xFFFFFFFF;
	fdir_conf.mask.ipv4_mask.dst_ip = 0xFFFFFFFF;
	memset(fdir_conf.mask.ipv6_mask.src_ip, 0xff, sizeof(fdir_conf.mask.ipv6_mask.src_ip));
	memset(fdir_conf.mask.ipv6_mask.dst_ip, 0xff, sizeof(fdir_conf.mask.ipv6_mask.dst_ip));
	fdir_conf.mask.src_port_mask = 0xFFFF;
	fdir_conf.mask.dst_port_mask = 0xFFFF;
	fdir_conf.mask.mac_addr_byte_mask = 0xFF;
	fdir_conf.mask.tunnel_type_mask = 1;
	fdir_conf.mask.tunnel_id_mask = 0xFFFFFFFF;
	fdir_conf.drop_queue = 127;
	//_Port_Info *port = inf->port_info_[portid];

	//memcpy(&(port->dev_conf_)->rxmode, &rx_mode, sizeof(struct rte_eth_rxmode));
	//memcpy(&(port->dev_conf_)->fdir_conf, &fdir_conf, sizeof(struct rte_fdir_conf));

	//port->dev_conf_->rx_adv_conf.rss_conf.rss_key = NULL;
	//port->dev_conf_->rx_adv_conf.rss_conf.rss_hf = 0;

	/* TODO : finding mempool as name */
	mp = inf->mp_info_[0].mp_;
	tx_buffer = &inf->tx_buffer[0];

	port->port_id_ = portid;

	/* init port */
	printf("Initializing port %u... ", (unsigned) portid);
	fflush(stdout);
	ret = rte_eth_dev_configure(portid, 1, 1, port->dev_conf_);
	if (ret < 0)
		rte_exit(EXIT_FAILURE, "Cannot configure device: err=%d, port=%u\n", ret, (unsigned) portid);

	rte_eth_macaddr_get(portid,&(port->src_mac_addr_));

	rte_eth_dev_info_get(portid, &(port->dev_info_));
	port->numa_id_ = (uint8_t) rte_eth_dev_socket_id(portid);

	if (port->numa_id_ > RTE_MAX_NUMA_NODES)
	{
		printf("ERROR Invalid Socket ID for Port(%u), Socket(%u)\n", portid, port->numa_id_);
		port->numa_id_ = 0;
	}

	/* init one RX queue */
	fflush(stdout);

	ret = rte_eth_rx_queue_setup(portid, 0, nb_rxd, rte_eth_dev_socket_id(portid), NULL, mp);
	if (ret < 0)
		rte_exit(EXIT_FAILURE, "rte_eth_rx_queue_setup:err=%d, port=%u\n", ret, (unsigned) portid);
	else
		printf("RX_queue setup portid:%u, eth_dev_sock:%u\n",portid, (uint8_t) rte_eth_dev_socket_id(portid));


	/* init one TX queue on each port */
	fflush(stdout);
	ret = rte_eth_tx_queue_setup(portid, 0, nb_txd, rte_eth_dev_socket_id(portid), NULL);

	if (ret < 0)
		rte_exit(EXIT_FAILURE, "rte_eth_tx_queue_setup:err=%d, port=%u\n", ret, (unsigned) portid);
	else
		printf("TX_queue setup portid:%u, eth_dev_sock:%u\n",portid, (uint8_t) rte_eth_dev_socket_id(portid));


	/* Initialize TX buffers */
	*tx_buffer = (rte_eth_dev_tx_buffer*)rte_zmalloc_socket("tx_buffer", 
			RTE_ETH_TX_BUFFER_SIZE(MAX_PKT_BURST), 
			0, 
			rte_eth_dev_socket_id(portid));
	if (*tx_buffer == NULL)
		rte_exit(EXIT_FAILURE, "Cannot allocate buffer for tx on port %u\n", (unsigned) portid);

	rte_eth_tx_buffer_init(*tx_buffer, MAX_PKT_BURST);

	ret = rte_eth_tx_buffer_set_err_callback(*tx_buffer, rte_eth_tx_buffer_count_callback, &port_statistics[portid].dropped);
	if (ret < 0)
		rte_exit(EXIT_FAILURE, "Cannot set error callback for tx buffer on port %u\n", (unsigned) portid);

	return ret;
}

/* Callback for request of configuring network interface up/down */
static int kni_config_network_interface(uint8_t portid, uint8_t if_up)
{
	int ret = 0;

	if (portid >= rte_eth_dev_count() || portid >= RTE_MAX_ETHPORTS) {
		printf("---- Invalid Port id %d\n",portid);
		//RTE_LOG(ERR, APP, "Invalid port id %d\n", portid);
		return -EINVAL;
	}

	//RTE_LOG(INFO, APP, //"Configure network interface of %d %s\n", portid, if_up ? "up" : "down");
	printf("Configure network interface of %d %s\n", portid, if_up ? "up" : "down");

	if (if_up != 0) { /* Configure network interface up */
		rte_eth_dev_stop(portid);
		ret = rte_eth_dev_start(portid);
		printf("%s kni[%u] dev_start\n",__func__,portid);
	} else {/* Configure network interface down */
		rte_eth_dev_stop(portid);
		printf("%s kni[%u] dev_stop\n",__func__,portid);
		return 0;
	}

	if (ret < 0)
		printf("Failed to start port %d", portid);

	_Port_Info *port_info = inf->port_info_[portid];
	struct ifreq s;
	int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);

	snprintf(s.ifr_name, RTE_KNI_NAMESIZE, "vEth%u", portid);

	/*
	if(port_info->is_bond_used_)
	{
		port_info = &g_global_info.port_[port_info->bond_port_info_->port_id_];
	}
	*/

	if(0 == ioctl(fd, SIOCGIFADDR, &s)) {
		struct sockaddr_in *sin;
		sin = (struct sockaddr_in*)&s.ifr_addr;

		memcpy(&(port_info->addr_.IP4_ADDR),
				&(sin->sin_addr), sizeof(port_info->addr_.IP4_ADDR));
	}

	if(0 == ioctl(fd, SIOCGIFHWADDR, &s)) {
		memcpy(port_info->src_mac_addr_.addr_bytes, s.ifr_addr.sa_data, sizeof(port_info->src_mac_addr_));
		printf("KNI Port %u:  %02X:%02X:%02X:%02X:%02X:%02X",
				port_info->port_id_,
				port_info->src_mac_addr_.addr_bytes[0], port_info->src_mac_addr_.addr_bytes[1],
				port_info->src_mac_addr_.addr_bytes[2], port_info->src_mac_addr_.addr_bytes[3],
				port_info->src_mac_addr_.addr_bytes[4], port_info->src_mac_addr_.addr_bytes[5]);
		port_info->kni_info_.is_run_ = true;
	}

	if (fd)
		close(fd);


	return ret;
}

static int kni_change_mtu(uint8_t port_id, unsigned new_mtu)
{
	printf("changing mtu size\n");
	return 0;
}


int DPort::KniAlloc(uint8_t portid)
{
	struct KniInfo *kni_info;
	struct rte_kni *kni;
	struct rte_kni_conf kni_conf;
	struct rte_kni_ops ops;
	struct rte_eth_dev_info dev_info;

	printf("kni_alloc %u\n",portid);

	/* Initialize KNI */
	kni_info = &(inf->port_info_[portid]->kni_info_);
	kni_info->port_id = portid;
	kni_info->lcore_rx = 1;
	kni_info->lcore_tx = 1;

	kni_info->lcore_k[portid] = 1; // lcore id	
	kni_info->nb_lcore_k = 1;	// sum of lcore

	/* KNI alloc */
	/* first KNI device associated to port is the master for multiple kernel thread enviroment. */
	memset(&kni_conf, 0, sizeof(kni_conf));
	memset(&dev_info, 0, sizeof(dev_info));

	rte_eth_dev_info_get(portid, &dev_info);

	snprintf(kni_conf.name, RTE_KNI_NAMESIZE, "vEth%u", portid );
	//kni_conf.core_id = kni_info->lcore_k[portid];
	//kni_conf.force_bind = 1;
	kni_conf.group_id = (uint16_t)portid;
	kni_conf.mbuf_size = 2048;	// MAX_PAKCET_SZ; // define as 2048
	kni_conf.addr = dev_info.pci_dev->addr;
	kni_conf.id = dev_info.pci_dev->id;

	memset(&ops, 0, sizeof(ops));
	ops.port_id = portid;
	ops.change_mtu = kni_change_mtu;
	ops.config_network_if = kni_config_network_interface;

	kni = rte_kni_alloc(inf->mp_info_[portid].mp_, &kni_conf, &ops);
	if(kni){
		kni_info->kni[0] = kni;
		printf("kni_allocated\n");
	}else
		printf("Failed Alloc KNI\n");

	return 0;

}

int DPort::StartPort(uint8_t portid)
{
	int ret=0;
	/* Start device */
	ret = rte_eth_dev_start(portid);
	if (ret < 0){
		rte_exit(EXIT_FAILURE, "rte_eth_dev_start:err=%d, port=%u\n", ret, (unsigned) portid);
		return ret;
	}

	//rte_eth_promiscuous_enable(portid);


	printf("Port(%u) Start. MAC address: %02X:%02X:%02X:%02X:%02X:%02X\n\n",
			(unsigned) portid,
			inf->port_info_[portid]->src_mac_addr_.addr_bytes[0],
			inf->port_info_[portid]->src_mac_addr_.addr_bytes[1],
			inf->port_info_[portid]->src_mac_addr_.addr_bytes[2],
			inf->port_info_[portid]->src_mac_addr_.addr_bytes[3],
			inf->port_info_[portid]->src_mac_addr_.addr_bytes[4],
			inf->port_info_[portid]->src_mac_addr_.addr_bytes[5]);

	/* initialize port stats */
	memset(&port_statistics, 0, sizeof(port_statistics));

	return ret;
}


