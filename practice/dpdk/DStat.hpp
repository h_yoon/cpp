
#ifndef __D_STAT_HPP__
#define __D_STAT_HPP__	1

#define MAX_PORT_STAT	3

struct PortStat {
	uint32_t recv;
	uint32_t send;
	uint32_t err;
}Port_Stat;

class DStat 
{
public:
	DStat();
	virtual ~DStat();

	Port_Stat *port_stat_[MAX_PORT_STAT];
};


#endif
