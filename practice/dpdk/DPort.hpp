
#ifndef __D_PORT_HPP__
#define __D_PORT_HPP__	1


class DPort 
{
public:
	DPort();
	virtual ~DPort();
	static DPort& Instance(void);
	int Init(int argc, char** argv);
	int InitPort(uint8_t portid);
	int StartPort(uint8_t portid);
	int KniAlloc(uint8_t portid);
	uint8_t ParseArgs(int argc, char **argv);

private:
	static DPort *instance_;

protected:
	int InitEAL(int argc, char** argv);
};

#endif
