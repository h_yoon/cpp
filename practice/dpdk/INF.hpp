
#ifndef __D_INF_HPP__
#define __D_INF_HPP__	1

#define MAX_RX_QUEUE_PER_LCORE 16
#define MAX_TX_QUEUE_PER_PORT 16


typedef struct PortQueueInfo
{
	char queue_name_[MAXNAME];
	uint8_t queue_id_;
}_port_queue_info;

typedef struct MemPoolInfo
{
	char pool_name_[MAXNAME];
	uint8_t pool_id_;
	struct rte_mempool *mp_;

}_mem_pool_info;


struct IP46_ADDR
{
	union
	{
		uint8_t     addr8[16];          /**< 1byte 로 표현  */
		uint16_t    addr16[8];          /**< 2byte 로 표현  */
		uint32_t    addr32[4];          /**< 4byte 로 표현  */
		uint64_t    addr64[2];          /**< 8byte 로 표현  */
	} ip_u;
};

#define IP4_ADDR    ip_u.addr32[0]      /**< IPv4 일 때 IP 에 접근 */

/**< IPv4 Print Argments  */
#define IPV4(addr)               \
	    addr.ip_u.addr8[0],  \
    addr.ip_u.addr8[1],  \
    addr.ip_u.addr8[2],  \
    addr.ip_u.addr8[3]

#define KNI_MAX_KTHREAD

struct KniInfo {
	bool	is_run_;
	uint8_t port_id;/* Port ID */
	unsigned lcore_rx; /* lcore ID for RX */
	unsigned lcore_tx; /* lcore ID for TX */
	uint32_t nb_lcore_k; /* Number of lcores for KNI multi kernel threads */
	uint32_t nb_kni; /* Number of KNI devices to be created */
	unsigned lcore_k[KNI_MAX_KTHREAD]; /* lcore ID list for kthreads */
	struct rte_kni *kni[KNI_MAX_KTHREAD]; /* KNI context pointers */
} __rte_cache_aligned;

// 포트의 정보
// 연결된 큐와 rt, tx 버퍼
typedef struct PortInfo
{
	uint8_t	port_id_;	//	port ID
	bool	is_used_;	//	is_used
	bool	is_kni_;	//	kni is used
	uint8_t	numa_id_;	//	numa socket ID, to get mempool
	struct rte_eth_conf *dev_conf_;
	struct rte_eth_dev_info dev_info_;
	struct KniInfo kni_info_;

	struct IP46_ADDR	addr_;          /**< 패킷 전송 시 사용하는 IP Address */
	struct ether_addr   src_mac_addr_;  /**< 패킷 전송 시 사용하는 Mac Address */

}_Port_Info;


// core에 할당된 port번호
typedef struct CoreInfo
{
	uint8_t core_id_;
	_Port_Info *port_info_;

}_core_info;

struct lcore_queue_conf {
	unsigned n_rx_port;
	unsigned rx_port_list[MAX_RX_QUEUE_PER_LCORE];
} __rte_cache_aligned;

class INF
{
public:
	INF();
	virtual ~INF();
	int Init(int argc, char **argv);
	uint8_t ParseArgs(int argc, char **argv);
	void main_loop(void);
	static void CheckKNIStatus(void *_args);
	int InitTimer(void);
	int Run(void);

	_Port_Info *port_info_[MAX_ETH];
	_mem_pool_info mp_info_[MAX_POOL];
	_port_queue_info queue_info_[MAX_QUEUE];

	atom::CTimer    sync_timer_;
	int nb_recv_;
	int nb_sent_;

	bool is_run_;
	int argc_;
	char **argv_;
	uint8_t avail_port_;
	struct rte_eth_dev_tx_buffer *tx_buffer[RTE_MAX_ETHPORTS];
	struct lcore_queue_conf lcore_queue_conf[RTE_MAX_ETHPORTS];

	/* list of enabled ports */
	uint32_t enabled_ports[RTE_MAX_ETHPORTS];
	uint8_t last_port;

protected:
	int EalInit(void);
	int CreateMempool(void);
};

#endif
