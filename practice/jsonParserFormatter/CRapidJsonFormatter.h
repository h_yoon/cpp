
#ifndef __H_RAPIDJSON_FORMAATER__
#define __H_RAPIDJSON_FORMAATER__

#include "Framework/Common/Common.h"

#include "CJsonObject.h"

#include "JsonFormatterInterface.h"

#include "Framework/SDK/Util/rapidjson/document.h"
#include "Framework/SDK/Util//rapidjson/prettywriter.h"

#include <iostream>
#include <sstream>
#include <list>


class CRapidJsonFormatter : public JsonFormatterInterface
{
public:
	SGBOOL Parse(IN CJsonObject* a_pJson);
	sgCString Print();
public:
	CRapidJsonFormatter(IN const SGBOOL& a_bPrintPretty = SGTRUE);
	virtual ~CRapidJsonFormatter();

private:
	SGBOOL SetArrayObject(IN CJsonObject* a_pJson, OUT rapidjson::Value& a_RapidAry);
	SGBOOL SetDocumentObject(IN CJsonObject* a_pJson, OUT rapidjson::Value& a_RapidRootValue);

	SGVOID EnablePrint() { m_bPrintPossible = SGTRUE; }
	SGVOID DisablePrint() { m_bPrintPossible = SGFALSE; }
	SGBOOL& IsEnablePrint() { return m_bPrintPossible; }

	SGVOID EnablePretty() { m_bPrettyPrint = SGTRUE; }
	SGVOID DisablePretty() { m_bPrettyPrint = SGFALSE; }
	SGBOOL& IsEnablePretty() { return m_bPrettyPrint; }

private:
	SGBOOL m_bPrintPossible = SGFALSE;
	SGBOOL m_bPrettyPrint = SGFALSE;

	rapidjson::Value m_RapidRootValue;
	rapidjson::Document m_RapidRootDoc;
	rapidjson::Document::AllocatorType &m_RapidAllocator = m_RapidRootDoc.GetAllocator();
};



#endif
