

#include "CJsonObject.h"

#include <map>
#include <utility>

CJsonObject* CJsonArray::At(IN const SGINT& a_nIndex)
{
	if (m_list.size() == 0)
		return SGNULL;

	if (a_nIndex > m_list.size()-1)
		return SGNULL;

	return m_list[a_nIndex];
}

SGBOOL CJsonArray::CheckType(IN const EOBJECT_TYPE& a_eType)
{
	if (m_eChildType != EOBJECT_TYPE::eNOTHING && m_eChildType != a_eType)
	{
		std::cout << "ERROR, Failed Array Handling, different type inserted. array_type:" << (SGINT)m_eChildType << " input_type:" << (SGINT)a_eType << std::endl;
		return SGFALSE;
	}

	if (m_eChildType == EOBJECT_TYPE::eNOTHING)
		m_eChildType = a_eType;

	return SGTRUE;
}

CJsonObject* CJsonArray::operator[](IN const SGINT& a_nIndex)
{
	return At(a_nIndex);
}

SGBOOL CJsonArray::Append(IN CJsonObject* a_pJsonObj, IN const EOBJECT_TYPE& a_eType)
{
	if (CheckType(a_eType) == SGFALSE) {
		SGDELETE a_pJsonObj;
		return SGFALSE;
	}

	m_list.push_back(a_pJsonObj);
	return SGTRUE;
}

SGBOOL CJsonArray::AddValue()
{
	CJsonObject* pJsonChild = SGNEW CJsonObject();
	return Append(pJsonChild, EOBJECT_TYPE::eNULLTYPE);
}

SGBOOL CJsonArray::AddValue(IN SGPCHAR a_pcValue)
{
	CJsonObject* pJsonChild = SGNEW CJsonString("", a_pcValue);
	return Append(pJsonChild, EOBJECT_TYPE::eSTRING);
}

SGBOOL CJsonArray::AddValue(IN const SGINT& a_nValue)
{
	CJsonObject* pJsonChild = SGNEW CJsonInt("", a_nValue);
	return Append(pJsonChild, EOBJECT_TYPE::eNUMERIC);
}

SGBOOL CJsonArray::AddValue(IN const SGBOOL& a_bValue)
{
	CJsonObject* pJsonChild = SGNEW CJsonBool("", a_bValue);
	EOBJECT_TYPE eType = EOBJECT_TYPE::eBOOLEAN;
	return Append(pJsonChild, eType);
}

SGBOOL CJsonArray::AddValue(IN CJsonArray* a_pJsonArray)
{
	return Append((CJsonObject*)a_pJsonArray, EOBJECT_TYPE::eARRAY);
}

SGBOOL CJsonArray::AddValue(IN CJsonDocument* a_pJsonDocument)
{
	return Append((CJsonObject*)a_pJsonDocument, EOBJECT_TYPE::eDOCUMENT);
}

CJsonArray* CJsonArray::NewArray()
{
	return SGNEW CJsonArray();
}

CJsonDocument* CJsonArray::NewDocument()
{
	return SGNEW CJsonDocument();
}

CJsonArray* CJsonArray::NewArrayChild()
{
	CJsonArray* pJsonChildAry = NewArray();
	if (AddValue(pJsonChildAry) == SGFALSE)
		return SGNULL;

	return pJsonChildAry;
}

CJsonDocument* CJsonArray::NewDocumentChild()
{
	CJsonDocument* pJsonChildDoc = NewDocument();
	if (AddValue(pJsonChildDoc) == SGFALSE)
		return SGNULL;

	return pJsonChildDoc;
}


CJsonObject* CJsonDocument::Find(IN SGPCCHAR a_pcKey)
{
	if (map_document.size() == 0)
		return SGNULL;

	const auto &itr = map_document.find(a_pcKey);
	if (itr == map_document.end())
		return SGNULL;

	return itr->second;
}

SGBOOL CJsonDocument::Append(IN SGPCCHAR a_pcKey, IN CJsonObject* pJsonChild)
{
	const std::unordered_map<std::string, CJsonObject*>::iterator &itr = map_document.find(a_pcKey);
	if (itr != map_document.end()) // Check duplicated key
	{
		std::cout << "Warning, Duplicated Key in CJsonDocument. key:" << a_pcKey << std::endl;
		SGDELETE pJsonChild;
		return SGFALSE;
	}

	map_document.emplace(a_pcKey, pJsonChild);
	return SGTRUE;
}

SGBOOL CJsonDocument::AddMember(IN SGPCCHAR a_pcKey)	// add null object
{
	CJsonObject* pJsonChild = SGNEW CJsonObject(a_pcKey);
	return Append(a_pcKey, pJsonChild);
}

SGBOOL CJsonDocument::AddMember(IN SGPCHAR a_pcKey, IN SGPCHAR a_pcValue)
{
	CJsonObject* pJsonChild = SGNEW CJsonString(a_pcKey, a_pcValue);
	return Append(a_pcKey, pJsonChild);
}

SGBOOL CJsonDocument::AddMember(IN SGPCCHAR a_pcKey, IN const SGINT& a_nValue)
{
	CJsonObject* pJsonChild = SGNEW CJsonInt(a_pcKey, a_nValue);
	return Append(a_pcKey, pJsonChild);
}

SGBOOL CJsonDocument::AddMember(IN SGPCCHAR a_pcKey, IN const SGBOOL& a_bValue)
{
	CJsonObject* pJsonChild = SGNEW CJsonBool(a_pcKey, a_bValue);
	return Append(a_pcKey, pJsonChild);
}

SGBOOL CJsonDocument::AddMember(IN SGPCCHAR a_pcKey, IN CJsonArray*& a_pJsonArray)
{
	return Append(a_pcKey, a_pJsonArray);
}

SGBOOL CJsonDocument::AddMember(IN SGPCCHAR a_pcKey, IN CJsonDocument*& a_pJsonDocument)
{
	return Append(a_pcKey, a_pJsonDocument);
}

CJsonArray* CJsonDocument::NewArray(IN SGPCCHAR a_pcKey)
{
	CJsonArray* pJsonChildAry = SGNEW CJsonArray(a_pcKey);
	return pJsonChildAry;
}
CJsonDocument* CJsonDocument::NewDocument(IN SGPCCHAR a_pcKey)
{
	CJsonDocument* pJsonChildDoc = SGNEW CJsonDocument(a_pcKey);
	return pJsonChildDoc;
}

CJsonArray* CJsonDocument::NewArrayMember(IN SGPCCHAR a_pcKey)
{

	CJsonArray* pJsonChildAry = NewArray(a_pcKey);
	if (AddMember(a_pcKey, pJsonChildAry) == SGFALSE)
		return SGNULL;
	else
		return pJsonChildAry;
}

CJsonDocument* CJsonDocument::NewDocumentMember(IN SGPCCHAR a_pcKey)
{
	CJsonDocument* pJsonChildDoc = NewDocument(a_pcKey);
	if (AddMember(a_pcKey, pJsonChildDoc) == SGFALSE)
		return SGNULL;
	else
		return pJsonChildDoc;
}
