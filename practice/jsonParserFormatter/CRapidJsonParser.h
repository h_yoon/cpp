 
#ifndef __H_RAPIDJSON_PARSER__
#define __H_RAPIDJSON_PARSER__


#include "Framework/Common/Common.h"

#include "CJsonObject.h"
#include "JsonParserInterface.h"

#include "Framework/SDK/Util/rapidjson/document.h"
#include "Framework/SDK/Util/rapidjson/writer.h"
#include "Framework/SDK/Util/rapidjson/stringbuffer.h"

#include <memory>
#include <list>
#include <iostream>
 


// �Ľ�
class CRapidJsonParser : public JsonParserInterface
{
public:
	SGBOOL Parse(IN SGPCHAR a_strJson, OUT CJsonObject *a_pJsonObj);

public:
	CRapidJsonParser() = default;
	virtual ~CRapidJsonParser() = default;

private:
	SGBOOL SetArrayObject(IN const rapidjson::Value &a_RapidAry, OUT CJsonArray *a_pJsonAry);
	SGBOOL SetDocumentObject(IN SGPCHAR a_strMsg, OUT CJsonObject* a_pJsonObj);
	sgCString GetDocStr(const rapidjson::Value& RapidDoc);
};


#endif
