
#include "Framework/Common/Common.h"
#include "Framework/SDK/Util/JsonParser2/CJsonObject.h"
#include "Framework/SDK/Util/JsonParser2/CJsonParser2.h"
#include "Framework/SDK/Util/JsonParser2/CJsonFormatter2.h"

#include <chrono>
#include <thread>
#include <iostream>

class SGJsonFormatter;

void test() 
{

	/// 객체 생성
	CJsonDocument rootObj;
	
	CJsonArray *ary = rootObj.NewArrayMember("data");	///document에  Array1  추가 
	CJsonArray *ary1 = ary->NewArrayChild();			///Array1에  Array2  추가 
	CJsonArray *ary2 = ary1->NewArrayChild();			///Array2에  Array3-1  추가 
	CJsonDocument *sd = ary2->NewDocumentChild();		///Array3-1에  Document1  추가 

	sd->AddMember("1.1.1.1", "data1");					///Document1에  데이터  추가 
	sd->AddMember("msg1", "test 1111 message1");
	sd->AddMember("msg2", "test 1111 message2");

	sd = ary2->NewDocumentChild();						///Array3에  Document2  추가 
	sd->AddMember("1.1.1.2", "data1");					///Document2에  데이터  추가 
	sd->AddMember("msg1", "test 1112 message1");
	sd->AddMember("msg2", "test 1112 message2");
		
	sd = ary2->NewDocumentChild();						///Array3에  Document3  추가 
	sd->AddMember("1.1.1.3", "data1");					///Document3에  데이터  추가 
	sd->AddMember("msg1", "test 1113 message1");
	sd->AddMember("msg2", "test 1113 message2");

	ary2 = ary1->NewArrayChild();						///Array2에  Array3-2  추가 
	sd = ary2->NewDocumentChild();						///Array3-2에  Document4  추가 
	sd->AddMember("1.1.2.1", "data1");					///Document4에  데이터  추가 
	sd->AddMember("msg1", "test 1121 message1");
	sd->AddMember("msg2", "test 1121 message2");

	sd = ary2->NewDocumentChild();
	sd->AddMember("1.1.2.2", "data1");
	sd->AddMember("msg1", "test 1122 message1");
	sd->AddMember("msg2", "test 1122 message2");

	sd = ary2->NewDocumentChild();
	sd->AddMember("1.1.2.3", "data1");
	sd->AddMember("msg1", "test 1123 message1");
	sd->AddMember("msg2", "test 1123 message2");

	ary1 = ary->NewArrayChild();
	ary2 = ary1->NewArrayChild();
	sd = ary2->NewDocumentChild();
	sd->AddMember("1.2.1.1", "data2");
	sd->AddMember("msg1", "test 1211 message1");
	sd->AddMember("msg2", "test 1211 message2");

	sd = ary2->NewDocumentChild();
	sd->AddMember("1.2.1.2", "data1");
	sd->AddMember("msg1", "test 1212 message1");
	sd->AddMember("msg2", "test 1212 message2");

	sd = ary2->NewDocumentChild();
	sd->AddMember("1.2.1.3", "data1");
	sd->AddMember("msg1", "test 1213 message1");
	sd->AddMember("msg1", "test 1213 message2");

	ary2 = ary1->NewArrayChild();
	sd = ary2->NewDocumentChild();
	sd->AddMember("1.2.2.1", "data1");
	sd->AddMember("msg1", "test 1221 message1");
	sd->AddMember("msg2", "test 1221 message2");

	sd = ary2->NewDocumentChild();
	sd->AddMember("1.2.2.2", "data1");
	sd->AddMember("msg1", "test 1222 message1");
	sd->AddMember("msg2", "test 1222 message2");

	sd = ary2->NewDocumentChild();
	sd->AddMember("1.2.2.3", "data1");
	sd->AddMember("msg1", "test 1223 message1");
	sd->AddMember("msg2", "test 1223 message2");	/// 객체 데이터 구성 End


	CJsonFormatter2 Formatter2;								
	JsonFormatterInterface* formatter = Formatter2.GetFormatter();	 /// default Formatter는 RapidJson사용
	if (formatter->Parse(&rootObj) == false)				/// Formatter를 통해 CJsonObject -> Json Str로 파싱
	{
		std::cout << "ERROR " << __func__ << " failed Parse 2" << std::endl;
	}
	sgCString result = formatter->Print();

	std::cout << "INFO result 1" << std::endl;
	std::cout << result.GetBuffer() << std::endl;	/// Json Str 출력

	CJsonDocument parsedObj;
	CJsonParser2 Parser2;
	JsonParserInterface* parser = Parser2.GetParser();		/// 파서객체 생성. (Json Str -> CJsonObject)

	if (parser->Parse(result.GetBuffer(), &parsedObj) == false) {	/// 기 생성된 Json Str문자열을 사용하여, CJsonObject가 동일하게 생성되는지 확인.
		std::cout << "Error, Failed Parsing " << std::endl;
		return;
	}

	CJsonObject *childAry = parsedObj.Find("data");	/// 데이터 엑세스
	CJsonObject *childAry1 = childAry->At(0);
	CJsonObject *childAry2 = childAry1->At(0);
	CJsonObject *childDoc1 = childAry2->At(0);
	sgCString key1 = "1.1.1.1";
	sgCString key2 = "msg1";
	sgCString key3 = "msg2";

	sgCString data1, data2;
	CJsonObject* obj = childDoc1->Find("1.1.1.1");

	obj->GetValue();

	std::cout << key1.GetBuffer() << ":" << obj->GetValue() << std::endl;

	childAry1 = childAry->At(1);
	childAry2 = childAry1->At(1);
	childDoc1 = childAry2->At(2);

	data1 = childDoc1->Find("1.2.2.3")->GetValue();
	data2 = childDoc1->Find("msg1")->GetValue();

	std::cout << key1.GetBuffer() << ":" << data1 << std::endl;
	std::cout << key2.GetBuffer() << ":" << data2 << std::endl;

}

SGBOOL DefaultConfig_GetInteger(IN /*ECONF_KEY*/SGINT a_eKeyId, OUT SGINT* a_pnValue, OUT SGBOOL* a_pbForce) { return true; }
SGBOOL DefaultConfig_GetString(IN /*ECONF_KEY*/SGINT a_eKeyId, OUT SGPCHAR* a_pszValue, OUT SGBOOL* a_pbForce) { return true; }
SGPCHAR DefaultConfig_GetConfigFileName() { return "filename"; }

void main()
{
	int cnt = 0;
	while (true) 
	{
		std::cout << "count " << cnt << " " << std::endl;
		test();
		//std::this_thread::sleep_for(std::chrono::microseconds(100));
		++cnt;
	}
	return;
}