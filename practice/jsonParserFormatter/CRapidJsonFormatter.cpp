#include "CRapidJsonFormatter.h"


CRapidJsonFormatter::CRapidJsonFormatter(IN const SGBOOL& a_bPrintPretty)
{
	if (a_bPrintPretty) 
		EnablePretty();
	else 
		DisablePretty();

	m_RapidRootValue = rapidjson::Value(rapidjson::kObjectType);
}

CRapidJsonFormatter::~CRapidJsonFormatter()
{
	
}

SGBOOL CRapidJsonFormatter::Parse(IN CJsonObject* a_pJson)
{
	DisablePrint();
	m_RapidRootValue.RemoveAllMembers();	// Clear Members
	if (SetDocumentObject(a_pJson, m_RapidRootValue) == SGFALSE)
	{
		std::cout << "ERROR " << __func__ << " failed add" << std::endl;
		return SGFALSE;
	}
	EnablePrint();
	return SGTRUE;
}

sgCString CRapidJsonFormatter::Print()
{
	if (IsEnablePrint() == SGFALSE)
		return "";

	m_RapidRootDoc.CopyFrom(m_RapidRootValue, m_RapidAllocator);
	rapidjson::StringBuffer RapidStrBuffer;

	if (IsEnablePretty() == SGTRUE)
	{
		rapidjson::PrettyWriter<rapidjson::StringBuffer> RapidStrPrettyWriter(RapidStrBuffer);
		m_RapidRootDoc.Accept(RapidStrPrettyWriter);
	}
	else
	{
		rapidjson::Writer<rapidjson::StringBuffer> RapidStrWriter(RapidStrBuffer);
		m_RapidRootDoc.Accept(RapidStrWriter);
	}

	sgCString result = (SGPCHAR)RapidStrBuffer.GetString();
	return	result;
}

SGBOOL CRapidJsonFormatter::SetArrayObject(IN CJsonObject* a_pJson, OUT rapidjson::Value& a_RapidAry)
{
	CJsonArray* pJsonChildAry = dynamic_cast<CJsonArray*>(a_pJson);
	if (pJsonChildAry == SGNULL)
	{
		std::cout << "ERROR " << __func__ << " failed <CJsonArray*>" << std::endl;
		return SGFALSE;
	}

	EOBJECT_TYPE eObjectType = pJsonChildAry->GetChildType();

	for (CJsonArray::arrayIterator itr = pJsonChildAry->MemberBegin(); itr != pJsonChildAry->MemberEnd(); ++itr)
	{
		rapidjson::Value rapidChildObj;
		CJsonObject* pJsonChild = *itr;

		switch (eObjectType)
		{
		case EOBJECT_TYPE::eNULLTYPE:
			a_RapidAry.PushBack(rapidChildObj.Move(), m_RapidAllocator);
			break;
		case EOBJECT_TYPE::eBOOLEAN:
			{
				SGBOOL bValue = pJsonChild->GetBool();
				a_RapidAry.PushBack(rapidChildObj.SetBool(bValue), m_RapidAllocator);
			}
			break;
		case EOBJECT_TYPE::eSTRING:
			{
				sgCString strValue = pJsonChild->GetValue();
				rapidChildObj.SetString(strValue.GetBuffer(), strValue.GetLength(), m_RapidAllocator);
				a_RapidAry.PushBack(rapidChildObj, m_RapidAllocator);
			}
			break;
		case EOBJECT_TYPE::eNUMERIC:
			{
				int nValue = pJsonChild->GetInt();
				a_RapidAry.PushBack(rapidChildObj.SetInt(nValue), m_RapidAllocator);
			}
			break;
 		case EOBJECT_TYPE::eARRAY:
			{
				rapidjson::Value RapidChildAry(rapidjson::kArrayType);
				if (SetArrayObject(pJsonChild, RapidChildAry) == SGFALSE)
				{
					std::cout << "ERROR " << __func__ << " failed add" << std::endl;
					assert(SGFALSE);
				}
				a_RapidAry.PushBack(RapidChildAry, m_RapidAllocator);
			}
			break;
		case EOBJECT_TYPE::eDOCUMENT:
			{
				CJsonDocument *pJsonChildDoc = dynamic_cast<CJsonDocument*>(pJsonChild);
				if (pJsonChildDoc == SGNULL) {
					std::cout << "ERROR " << __func__ << " fail dynamic_cast<CJsonDocument*>(itr->second);" << std::endl;
					assert(SGFALSE);
				}

				rapidjson::Value RapidChildDoc(rapidjson::kObjectType);
				if (SetDocumentObject(pJsonChildDoc, RapidChildDoc) == SGFALSE)
				{
					std::cout << "ERROR " << __func__ << " fail SetDocumentObject()" << std::endl;
					assert(SGFALSE);
				}
				a_RapidAry.PushBack(RapidChildDoc, m_RapidAllocator);
			}
			break;
		default:
			std::cout << "ERROR " << __func__ << "default type" << std::endl;
			break;
		}
	}
	return SGTRUE;
}

SGBOOL CRapidJsonFormatter::SetDocumentObject(IN CJsonObject* a_pJsonObj, OUT rapidjson::Value& a_RapidRootValue)
{
	CJsonDocument* pJsonDoc = dynamic_cast<CJsonDocument*>(a_pJsonObj);
	if (pJsonDoc == SGNULL)
	{
		std::cout << "ERROR " << __func__ << " failed <CJsonDocument*>" << std::endl;
		assert(SGFALSE); // assert is only for debug in developing
		return SGFALSE;
	}

	for (CJsonDocument::mapIterator itr = pJsonDoc->MemberBegin(); itr != pJsonDoc->MemberEnd(); ++itr)
	{
		EOBJECT_TYPE eObjType = (EOBJECT_TYPE)itr->second->GetType();

		sgCString strKey = (SGPCHAR)(itr->first.c_str());
		CJsonObject*& pJsonChild = itr->second;

		rapidjson::Value RapidKey;
		rapidjson::Value RapidValue;

		RapidKey.SetString(strKey.GetBuffer(), strKey.GetLength(), m_RapidAllocator);

		switch (eObjType)
		{
		case EOBJECT_TYPE::eNULLTYPE:
			RapidValue.SetNull();
			a_RapidRootValue.AddMember(RapidKey, RapidValue, m_RapidAllocator);
			break;
		case EOBJECT_TYPE::eBOOLEAN:
			{
				SGBOOL bValue = pJsonChild->GetBool();
				RapidValue.SetBool(bValue);
				a_RapidRootValue.AddMember(RapidKey, RapidValue, m_RapidAllocator);
			}
			break;
		case EOBJECT_TYPE::eSTRING:
			{
				sgCString strValue = pJsonChild->GetValue();
				RapidValue.SetString(strValue.GetBuffer(), strValue.GetLength(), m_RapidAllocator);
				a_RapidRootValue.AddMember(RapidKey, RapidValue, m_RapidAllocator);
			}
			break;
		case EOBJECT_TYPE::eNUMERIC: // int, double	//TODO: int64, unsigned int
			{
				int nValue = itr->second->GetInt();
				RapidValue.SetInt(nValue);
				a_RapidRootValue.AddMember(RapidKey, RapidValue, m_RapidAllocator);
			}
			break;
		case EOBJECT_TYPE::eARRAY:
			{
				rapidjson::Value RapidAry(rapidjson::kArrayType);
				if (SetArrayObject(pJsonChild, RapidAry) == false)
				{
					std::cout << "ERROR " << __func__ << " fail parsing " << std::endl;
					assert(SGFALSE); // assert is only for debug in developing
					return SGFALSE;
				}
				a_RapidRootValue.AddMember(RapidKey, RapidAry, m_RapidAllocator);
			}
			break;
		case EOBJECT_TYPE::eDOCUMENT:
			{
				if (pJsonChild == SGNULL)
				{
					std::cout << "ERROR " << __func__ << " fail dynamic_cast<CJsonDocument*>(itr->second);" << std::endl;
					assert(SGFALSE); // assert is only for debug in developing
					return SGFALSE;
				}

				rapidjson::Value RapidDoc(rapidjson::kObjectType);
				if (SetDocumentObject(pJsonChild, RapidDoc) == SGFALSE)
				{
					std::cout << "ERROR " << __func__ << " fail SetDocumentObject()" << std::endl;
					assert(SGFALSE); // assert is only for debug in developing
					return SGFALSE;
				}
				a_RapidRootValue.AddMember(RapidKey, RapidDoc, m_RapidAllocator);
			}
			break;
		default:
			std::cout << "ERROR " << __func__ << "default type" << std::endl;
			break;
		}
	}
	return SGTRUE;
}
