

#ifndef __H_SGCJsonObject__
#define __H_SGCJsonObject__


#include "Framework/Common/Common.h"
#include "Framework/Config/DefaultConfig.h"

#include <unordered_map>
#include <deque>
#include <iostream>
#include <string>

/*
type define
null				0
bool-false			1
bool-true			2
document			3
list				4
string				5
int, double, float	6
*/



/* 
TODO list
	int형 -> int64로 교체
	to_string
	return false -> 크리티컬 부분에 Assert 적용

Note
1 child의 순서를 완벽하게 보장하지는 않음.
2 pretty한 출력을 설정 할수 있으나, 간격에 대한 포맷팅은 미지원
3 중복키는 처리하지 않음. (warning 만 표시)
*/

/* 코드리뷰 후 수정사항
1 string return nullptr -> return "" 수정
2 리턴명 함수명 한줄로
3 함수들 cpp파일로
4 enumType 체크
5 함수 기능분리
	- SetDocument함수에서 SetObject함수 분리
	- NewDocumentMember, NewArrayMember 가 두가지 기능(생성/추가)
6 헤더파일에 rapidjson Library 노출 하지 않기.
*/



typedef enum class EJsonLibName {
	eJLN_RapidJson = 0,
} EJSONLIB_NAME;


typedef enum class EJsonLibraryObjectType{
	eNULLTYPE = 0,
	eFALSE,
	eTRUE,
	eDOCUMENT,
	eARRAY,
	eSTRING,
	eNUMERIC
} EJSONLIB_TYPE;

typedef enum class EJsonObjectType{
	eNOTHING = 0, // initial value of EOBJECT_TYPE in array object.
	eNULLTYPE,
	eBOOLEAN,
	eDOCUMENT,
	eARRAY,
	eSTRING,
	eNUMERIC,
} EOBJECT_TYPE;


class CJsonObject {
public:
	sgCString GetKey() { return m_strKey; }
	SGVOID SetKey() = delete;

	virtual EOBJECT_TYPE GetType() { return EOBJECT_TYPE::eNULLTYPE; }
	virtual SGPCHAR GetValue() { return ""; }
	virtual	SGINT GetInt() { return 0; }
	virtual SGBOOL GetBool() { return SGFALSE; }

	virtual CJsonObject* At(IN const SGINT& a_nIndex) { return SGNULL; }
	virtual CJsonObject* Find(IN const char* a_pcKey) { return SGNULL; }

	virtual CJsonObject* operator[](IN const SGINT& a_nIndex) { return SGNULL; }

public:
	CJsonObject(IN SGPCCHAR a_strKey = "") { m_strKey.Assign((SGPCHAR)a_strKey); }
	virtual ~CJsonObject() {}

protected:

private:
	sgCString m_strKey;

};

class CJsonString : public CJsonObject {
public:
	SGPCHAR GetValue() { return GetString(); }
	EOBJECT_TYPE GetType() { return EOBJECT_TYPE::eSTRING; }
	SGPCHAR GetString() { return m_strValue.GetBuffer(); }

public:
	CJsonString(SGPCHAR a_pcKey, SGPCHAR a_pcValue)
		: CJsonObject(a_pcKey) {
		m_strValue.Assign(a_pcValue);
	}

	virtual ~CJsonString() = default;

private:
	sgCString m_strValue;
};


class CJsonInt : public CJsonObject {
public:
	SGPCHAR GetValue() {
		return "1111111111111111";	// TODO !!!
		//return std::to_string(GetInt());
	}

	EOBJECT_TYPE GetType() { return EOBJECT_TYPE::eNUMERIC; }
	SGINT GetInt() { return m_nValue; }

public:
	CJsonInt() {}
	CJsonInt(SGPCCHAR a_pcKey, const int &a_nValue)
		: CJsonObject(a_pcKey) , m_nValue(a_nValue) {}
	virtual ~CJsonInt() = default;

private:
	SGINT m_nValue;
};


class CJsonBool : public CJsonObject {
public:
	SGPCHAR GetValue() { return (m_bValue ? "true" : "false"); } // TODO fix it -> to return buffer.
	EOBJECT_TYPE GetType() { return EOBJECT_TYPE::eBOOLEAN; }
	SGBOOL GetBool() { return m_bValue; }

public:
	CJsonBool(IN SGPCCHAR a_pcKey, const bool& a_bValue)	
		: CJsonObject(a_pcKey) , m_bValue(a_bValue) {}

	virtual ~CJsonBool() = default;

private:
	SGBOOL m_bValue;
};

class CJsonDocument;

class CJsonArray : public CJsonObject {
public:
	SGUINT64 GetSize() { return m_list.size(); }
	EOBJECT_TYPE GetType() { return EOBJECT_TYPE::eARRAY; }
	SGVOID SetChildType(const EOBJECT_TYPE& a_eChildType) { m_eChildType = a_eChildType; }
	EOBJECT_TYPE GetChildType() { return m_eChildType; }

	CJsonObject* At(IN const SGINT& a_nIndex);
	CJsonObject* operator[](IN const SGINT& a_nIndex);

	SGBOOL AddValue();	// null type
	SGBOOL AddValue(IN SGPCHAR a_pcValue);
	SGBOOL AddValue(IN const SGINT& a_nValue);
	SGBOOL AddValue(IN const SGBOOL& a_bValue);
	SGBOOL AddValue(IN CJsonArray* a_pJsonArray);
	SGBOOL AddValue(IN CJsonDocument* a_pJsonDocument);

	CJsonArray* NewArray();
	CJsonArray* NewArrayChild();
	CJsonDocument* NewDocument();
	CJsonDocument* NewDocumentChild();

	typedef std::deque<CJsonObject*>::iterator arrayIterator;
	arrayIterator MemberBegin() {
		return m_list.begin();
	}
	arrayIterator MemberEnd() {
		return m_list.end();
	}

public:
	CJsonArray(IN SGPCCHAR a_pcKey = "") : CJsonObject(a_pcKey) {} 
	virtual ~CJsonArray() {
		for (auto &member : m_list)
			delete member;
	}

private:
	SGBOOL CheckType(IN const EOBJECT_TYPE& a_eType);
	SGBOOL Append(IN CJsonObject* pJsonChild, IN const EOBJECT_TYPE& a_eType);

private:
	std::deque<CJsonObject*> m_list;
	EOBJECT_TYPE m_eChildType = EOBJECT_TYPE::eNOTHING;
};


class CJsonDocument : public CJsonObject
{
public:
	std::unordered_map<std::string, CJsonObject*>& getDocument() { return map_document; }
	
	SGUINT64 GetSize() { return map_document.size(); }
	EOBJECT_TYPE GetType() { return EOBJECT_TYPE::eDOCUMENT; }
	CJsonObject* Find(IN SGPCCHAR a_pcKey);

	SGBOOL AddMember(IN SGPCCHAR a_pcKey); // null object
	SGBOOL AddMember(IN SGPCHAR a_pcKey, IN SGPCHAR a_pcValue);
	SGBOOL AddMember(IN SGPCCHAR a_pcKey, IN const SGINT& a_nValue);
	SGBOOL AddMember(IN SGPCCHAR a_pcKey, IN const SGBOOL& a_bValue);

	SGBOOL AddMember(IN SGPCCHAR a_pcKey, IN CJsonArray*& a_pJsonArray);
	SGBOOL AddMember(IN SGPCCHAR a_pcKey, IN CJsonDocument*& a_PJsonDocument);

	CJsonArray* NewArray(IN SGPCCHAR a_pcKey);
	CJsonArray*	NewArrayMember(IN SGPCCHAR a_pcKey);
	CJsonDocument* NewDocument(IN SGPCCHAR a_pcKey);
	CJsonDocument* NewDocumentMember(IN SGPCCHAR a_pcKey);

	typedef std::unordered_map<sgString, CJsonObject*>::iterator  mapIterator;
	mapIterator MemberBegin() {
		return map_document.begin();
	}
	mapIterator MemberEnd() {
		return map_document.end();
	}

public:
	CJsonDocument(IN SGPCCHAR a_pcKey = "") : CJsonObject(a_pcKey) {}
	virtual ~CJsonDocument() {
		for (auto &member : map_document)
			delete member.second;
	}

private:
	SGBOOL Append(IN SGPCCHAR a_pcKey, IN CJsonObject* pJsonChild);

private:
	std::unordered_map<sgString, CJsonObject*> map_document;
};

#endif
