
#ifndef __H_JSON_FORMATTER_INTERFACE__
#define __H_JSON_FORMATTER_INTERFACE__


class CJsonObject;

class JsonFormatterInterface {
public:
	JsonFormatterInterface() = default;
	virtual ~JsonFormatterInterface() = default;

	virtual SGBOOL Parse(IN CJsonObject* a_pJson) = 0;
	virtual sgCString Print() = 0;
};

#endif
