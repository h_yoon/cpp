

#ifndef __H_CJSON_FORMATTER2__
#define __H_CJSON_FORMATTER2__

#include "Framework/Common/Common.h"

#include "Framework/SDK/Util/JsonParser2/JsonFormatterInterface.h"
#include "Framework/SDK/Util/JsonParser2/CRapidJsonFormatter.h"


class CJsonFormatter2 
{
public:
	JsonFormatterInterface* GetFormatter() { return m_Formatter; }

public:
	CJsonFormatter2(const EJSONLIB_NAME& a_LibType = EJSONLIB_NAME::eJLN_RapidJson)
	{
		switch (a_LibType) 
		{
			case EJSONLIB_NAME::eJLN_RapidJson:
				m_Formatter = SGNEW CRapidJsonFormatter();
				break;
			default:
				std::cout << "ERROR, Invalid Json Library Type:"<< (int)a_LibType << std::endl;
				assert(false);
				break;
		}
	}
	virtual ~CJsonFormatter2() {
		SGDELETE m_Formatter;
	}

private:
	JsonFormatterInterface* m_Formatter;

};


#endif
