
#ifndef __H_JSON_PARSER_INTERFACE__
#define __H_JSON_PARSER_INTERFACE__

#include "Framework/Common/Common.h"

class CJsonObject;

class JsonParserInterface {
public:
	JsonParserInterface() = default;
	virtual ~JsonParserInterface() = default;

	virtual SGBOOL Parse(IN SGPCHAR a_pcJson, OUT CJsonObject* a_pJsonObj) = 0;
};

#endif
