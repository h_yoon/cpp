

#ifndef __H_CJSON_PARSER2__
#define __H_CJSON_PARSER2__

#include "Framework/SDK/Util/JsonParser2/JsonParserInterface.h"
#include "Framework/SDK/Util/JsonParser2/CRapidJsonParser.h"


class CJsonParser2 
{
public:
	CJsonParser2(const EJSONLIB_NAME& a_LibType = EJSONLIB_NAME::eJLN_RapidJson) 
	{
		switch (a_LibType)
		{
		case EJSONLIB_NAME::eJLN_RapidJson:
			m_Parser = SGNEW CRapidJsonParser();
			break;
		default:
			std::cout << "ERROR, Invalid Json Library Type:" << (int)a_LibType << std::endl;
			assert(false);
			break;
		}
	}
	virtual ~CJsonParser2() {
		SGDELETE m_Parser;
	}

	JsonParserInterface* GetParser() { return m_Parser; }

private:
	JsonParserInterface* m_Parser;

};

#endif
