
#include "CRapidJsonParser.h"

SGBOOL CRapidJsonParser::Parse(IN SGPCHAR a_strJson, OUT CJsonObject* a_pJsonObj)
{
	return SetDocumentObject(a_strJson, a_pJsonObj);
}


SGBOOL CRapidJsonParser::SetArrayObject(IN const rapidjson::Value& a_RapidAry, OUT CJsonArray* a_pJsonAry)
{
	assert(a_RapidAry.IsArray());

	SGBOOL result = SGFALSE;

	if (a_RapidAry.Size() > 0)	// array size 0
	{
		EJSONLIB_TYPE eType = (EJSONLIB_TYPE)a_RapidAry[0].GetType();

		for (rapidjson::SizeType i = 0; i < a_RapidAry.Size(); i++)  // rapidjson uses SizeType instead of size_t.
		{
			switch (eType) {
			case EJSONLIB_TYPE::eNULLTYPE:
				a_pJsonAry->AddValue();
				break;
			case EJSONLIB_TYPE::eFALSE:
			case EJSONLIB_TYPE::eTRUE:
				a_pJsonAry->AddValue(a_RapidAry[i].GetBool());
				break;
			case EJSONLIB_TYPE::eSTRING:
				a_pJsonAry->AddValue((SGPCHAR)a_RapidAry[i].GetString());
				break;
			case EJSONLIB_TYPE::eNUMERIC: 	// TODO int, double
				a_pJsonAry->AddValue(a_RapidAry[i].GetInt());
				break;
			case EJSONLIB_TYPE::eARRAY:
			{
				CJsonArray* pJsonChildAry = a_pJsonAry->NewArrayChild();
				result = SetArrayObject(a_RapidAry[i], pJsonChildAry);
			}
			break;
			case EJSONLIB_TYPE::eDOCUMENT:
			{
				sgCString pcDocStr = GetDocStr(a_RapidAry[i]);
				CJsonObject* pJsonChildDoc = (CJsonObject*)a_pJsonAry->NewDocumentChild();
				result = SetDocumentObject(pcDocStr.GetBuffer(), pJsonChildDoc);
			}
			break;

		default:
			break;
			}
		}
	}

	return SGTRUE;
}

SGBOOL CRapidJsonParser::SetDocumentObject(IN SGPCHAR a_pcJsonMsg, OUT CJsonObject* a_pJsonAryObj)
{
	// parse
	rapidjson::Document RapidDoc;
	RapidDoc.Parse(a_pcJsonMsg);

	if (RapidDoc.HasParseError() == true) {
		std::cout << "ERROR " << __func__ << " Failed Parsing" << std::endl;
		std::cout << "parsing text (" << a_pcJsonMsg << ")" << std::endl;
		return SGFALSE;
	}

	CJsonDocument* pJsonDoc = dynamic_cast<CJsonDocument*>(a_pJsonAryObj);
	if (pJsonDoc == nullptr) {
		std::cout << "ERROR, Failed dynamic_cast" << std::endl;
		return SGFALSE;
	}

	for (rapidjson::Value::ConstMemberIterator itr = RapidDoc.MemberBegin(); itr != RapidDoc.MemberEnd(); ++itr)
	{
		EJSONLIB_TYPE e_RapidType = (EJSONLIB_TYPE)itr->value.GetType();

		sgCString strKey = (SGPCHAR)itr->name.GetString();
		SGPCHAR buffStr = strKey.GetBuffer();
		switch (e_RapidType)
		{
		case EJSONLIB_TYPE::eNULLTYPE:
			pJsonDoc->AddMember(buffStr);
			break;
		case EJSONLIB_TYPE::eFALSE:
		case EJSONLIB_TYPE::eTRUE:
			pJsonDoc->AddMember(buffStr, itr->value.GetBool());
			break;
		case EJSONLIB_TYPE::eSTRING:
			pJsonDoc->AddMember(buffStr, (SGPCHAR)itr->value.GetString());
			break;
		case EJSONLIB_TYPE::eNUMERIC: 	// TODO adding int64, uint64
			pJsonDoc->AddMember(buffStr, itr->value.GetInt());
			break;
		case EJSONLIB_TYPE::eARRAY:
			{
				const rapidjson::Value& RapidChildAry = RapidDoc[buffStr];
				CJsonArray* pJsonAry= pJsonDoc->NewArrayMember(buffStr);
				SetArrayObject(RapidChildAry, pJsonAry);
			}
			break;
		case EJSONLIB_TYPE::eDOCUMENT:
			{
				sgCString pcDocStr = GetDocStr(RapidDoc[buffStr]);
				CJsonObject* pJsonChildDoc = (CJsonObject*)pJsonDoc->NewDocumentMember(buffStr);
				SetDocumentObject(pcDocStr.GetBuffer(), pJsonChildDoc);
			}
			break;
		default:
			return SGFALSE;
			break;
		}
	}
	return SGTRUE;
}

sgCString CRapidJsonParser::GetDocStr(const rapidjson::Value& a_RapidDoc)
{
	assert(a_RapidDoc.IsObject());
	rapidjson::StringBuffer rapidStrBuffer;
	rapidStrBuffer.Clear();

	rapidjson::Writer<rapidjson::StringBuffer> writer(rapidStrBuffer);
	a_RapidDoc.Accept(writer);
	sgCString result = (SGPCHAR)rapidStrBuffer.GetString();
	return result;
}
