/*
 * Singletone.h
 *
 *  Created on: 2021. 6. 1.
 *      Author: hsyoon
 */

#ifndef H_Singletone_
#define H_Singletone_

#include <mutex>

template <typename T>
class Singletone
{
public:
	virtual ~Singletone() {};

	static T* get()
	{
		return instance_;
	}

	static T* ins()
	{
		std::call_once(flag_, create_once);
		return instance_;
	}

	static void create_once() 
	{
		instance_ = new T;
	}

private:
	static std::once_flag flag_;
	static T* instance_;
	static std::mutex mtx_;

};

template <typename T>
T* Singletone<T>::instance_ = nullptr;

template <typename T>
std::mutex Singletone<T>::mtx_;

template <typename T>
std::once_flag Singletone<T>::flag_;

#endif