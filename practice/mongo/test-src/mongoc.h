
#ifndef MONGO_C_H_
#define MONGO_C_H_

#include <mongoc/mongoc.h>
#include <iostream>

class MongoClient
{
public:
  MongoClient() = default;
  virtual ~MongoClient() = default;

  bool init(const std::string &uri_str,
            const std::string &app_name,
            const std::string &db_name,
            const std::string &collection_name) 
  {
    bson_error_t error;
    uri_ = mongoc_uri_new_with_error (uri_str, &error);
    if (!uri_) 
    {
      std::cout << "failed to parse URI: %s\nerror message:       %s\n" <<uri_str << error.message << std::endl;
      return false;
    }

    client_ = mongoc_client_new_from_uri (uri_);
    if (!client_) 
    {
      std::cout << "failed to create client" << std::endl;
      return false;
    }

    database_   = mongoc_client_get_database (client, db_name);
    collection_ = mongoc_client_get_collection (client, db_name, collection_name);
  }

  bool connect();
  bool ping();
  
  static void initialize() { mongoc_init (); }
private:
  mongoc_client_t     *client_;
  mongoc_database_t   *database_;
  mongoc_collection_t *collection_;

  mongoc_uri_t *uri_;
};

#endif /* MONGO_C_H_ */