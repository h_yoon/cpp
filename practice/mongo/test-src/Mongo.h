

#ifndef MONGO_H_
#define MONGO_H_

#include "Singletone.h"

#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/pool.hpp>
#include <mongocxx/uri.hpp>
#include <mongocxx/stdx.hpp>

#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/json.hpp>

#include <memory>
#include <iostream>

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::make_document;
using connection = mongocxx::pool::entry;


#define mongos Mongo::ins()

class Mongo : public Singletone<Mongo>
{
public:
  Mongo() = default;
  virtual ~Mongo();

  bool init(const std::string &url); // 단 한번만 호출되어야한다.
  bool ping();

  connection get_connection() { return (pool_->acquire()); }
private:

private:
  std::shared_ptr<mongocxx::instance> instance_ = nullptr;
  std::shared_ptr<mongocxx::pool>     pool_   = nullptr;
};

Mongo::~Mongo()
{
  if(pool_)     
    pool_.reset();
  
  if(instance_) 
    instance_.reset();
}


bool
Mongo::init(const std::string &url)
{
  try
  {
    instance_ = std::make_shared<mongocxx::instance>();
    pool_ = std::make_shared<mongocxx::pool>(mongocxx::uri{url});
    
    return ping();
  }
  catch(...)
  {
    std::cout << "Error Mongo::init()" << std::endl;
  }

  return false;
}

bool
Mongo::ping()
{
  try
  {
    auto client = get_connection();
    auto test_db = (*client)["dummy_name"];
    auto result = test_db.run_command(make_document(kvp("ping", 1)));
    std::cout << bsoncxx::to_json(result) << "\n";

    return true;
  }
  catch(const std::exception& xcp)
  {
    std::cout << "connection failed: " << xcp.what() << "\n";
    std::cout << "Fail mongo connect test" << std::endl;
  }
  
  return false;
}

#endif /* MONGO_H_ */
