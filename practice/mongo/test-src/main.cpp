
#include "Mongo.h"
#include "ChatRooms.h"

#include <stdlib.h>
#include <stdio.h>

int main()
{

  if (mongos->init("mongodb://localhost:27017") == false)
  {
    std::cout << "fail init" << std::endl;
    return 0;
  }

  if (ChatRooms::write_room("admin","testdb", "myroom_test1") == false)
  {
    std::cout << "fail write_room" << std::endl;
    return 0;
  }

  std::cout << "success" << std::endl;
  return 0;
}

