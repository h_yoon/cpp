/*
  Author: hyoon
  date  : 2024.10.10
*/

#ifndef CHAT_ROOMS_H_
#define CHAT_ROOMS_H_

#include "Mongo.h"
#include <mongocxx/client.hpp>
#include <bsoncxx/builder/basic/document.hpp>

/*
chat_rooms
{
    _id:ObjectId('R_YYYYMMDDHHSS01001'),  # document  
    name:"최초방이름",
    users:    
    [ 
        ObjectId('user_id), 
    ],
    "createdAt": ISODate("2024-09-26T12:42:41.382Z")
    # 기타 방 설정
}
*/
using mongo_document = bsoncxx::document::value;
using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;

class ChatRooms
{
public:
  ChatRooms() = default;
  virtual ~ChatRooms() = default;

  static bool find_room (const std::string &room_name);
  static bool write_room(const std::string &db_name,
                         const std::string &coll_name,
                         const std::string &room_name);
};


inline bool
ChatRooms::find_room(const std::string &room_name)
{

  
  return true;
}

inline bool
ChatRooms::write_room(const std::string &db_name,
                      const std::string &coll_name,
                      const std::string &room_name)
{
  try 
  {
    auto now = std::chrono::system_clock::now();
    std::time_t now_time = std::chrono::system_clock::to_time_t(now);
    auto bson_date = bsoncxx::types::b_date{std::chrono::system_clock::from_time_t(now_time)};

    auto client = mongos->get_connection();
    auto coll   = (*client)[db_name][coll_name];

     auto doc = make_document(
         kvp("name",  room_name),
         kvp("users", make_array("hsyoon", "qpek12314", "username44", "user1name", "powersolname", "user_nae3")),
         kvp("createdAt", bson_date)
       );

    auto insert_one_result = coll.insert_one(std::move(doc));
      
    if (!insert_one_result)
    {
      std::cout << "Error write ChatRooms" << std::endl;
      return false;
    }
  }
  catch (...)
  {
    std::cout << "Error exception write ChatRooms" << std::endl;
    return false;
  }
    
  return true;
}

#endif /* CHAT_ROOMS_H_ */
