
// alloc test
// 2022.08.05 hsy

// alloc spped: local alloc is fast (local alloc 650ms vs new alloc 1400ms)
// copy speed: new alloc is fast

// delete 까지 하니까 속도가 비슷해짐....

#include <iostream>
#include <vector>
#include <map>
#include "../common/TimeCounter.h"

class Object
{
public:
	Object() {}
	Object(const Object &r) {
		mInt = r.mInt;
		mStr = r.mStr;
		if(r.mMap.size())
			mMap = r.mMap;
	}
	~Object() = default;

	int mInt;
	std::string mStr;
	std::map<int,std::string> mMap;

	

};

int main()
{


TimeCounter timer;
size_t loop = 30000000;



	std::cout << "Pointer Object alloc" << std::endl;
	timer.start();

	for(size_t i=0; i<loop; ++i)
	{
		std::vector<Object*> pObjVec;
		Object* obj = new Object();
		pObjVec.push_back(obj);
	}
	timer.stop();
	timer.print();

	std::cout << "---------------------------------" << std::endl;

	std::cout << "Object alloc" << std::endl;
	timer.start();
	for(size_t i=0; i<loop; ++i)
	{
		std::vector<Object> objVec;
		objVec.push_back(Object());
	}
	timer.stop();
	timer.print();

	return 0;

}
