// string streamer by using functor v2
// 2021.06.30. hsy

#include <iostream>
#include <string>

#define LOG Streamer<myLogger>(__FILE__,__LINE__,__func__)
  
struct myLogger
{
  void operator()(const std::string &header, const std::string &msg)  {
    std::cout << header << msg << std::endl;
  }
};

template <typename LOGGER>
class Streamer
{
public:
  Streamer(const std::string &file,
           const size_t      &line,
           const std::string &func
            )
          { 
            header_ = file+":" + std::to_string(line) + " " + func +"] ";
          };
  ~Streamer() { 
    logger_(header_, msg_);
  }

  Streamer& operator<<(const std::string &str) { msg_ = msg_ + " " + str; return *this; }

private:
  std::string header_;
  std::string msg_;
  LOGGER logger_;
};

int main()
{
  LOG << "HI" << "my" << "Logger"; 
  return 0;
}

