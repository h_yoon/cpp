
// string streamer by using funtor v1
// 2021.06.30. hsy 

#include <iostream>

#define LOG Streamer()

class Streamer
{
public:
  Streamer() = default;
  ~Streamer() { std::cout << msg_ << std::endl; }

  Streamer& operator<<(const std::string &str) {
    msg_ = msg_ + " " + str;
    return *this; 
  }

private:
  std::string msg_ = "";
};

int main()
{
  LOG << "HI" << "In studing" << "hiho";
  return 0;
}
