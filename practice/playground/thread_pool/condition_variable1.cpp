// conditaion_variable with thread poll
// 2022.05.13 hsy
// practice 1

#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <unistd.h>

std::mutex m;
std::condition_variable cv;
std::string data;
bool ready = false;
bool processed = false;


void
worker_thread() 
{
  std::unique_lock<std::mutex> lk(m);
  std::cout << "waiting tid:"<< std::this_thread::get_id() << std::endl;
  cv.wait(lk, []{return ready;});

  std::cout << "worker thread processing. tid: " << std::this_thread::get_id() << std::endl;
  data += " after processing";

  processed = true;
  std::cout << "worker processing completed. tid " << std::this_thread::get_id() << std::endl;
 
  lk.unlock();
  //cv.notify_one();
}


int main() 
{
  std::thread worker(worker_thread);
  std::thread worker2(worker_thread);
  std::thread worker3(worker_thread);
  std::thread worker4(worker_thread);

  data = "example data";
  sleep(5);
  /// send data to ther worker thread
  {
    std::lock_guard<std::mutex> lk(m);
    ready = true;
    std::cout << "main() signals data ready for processing\n";
  }
  cv.notify_one();
  cv.notify_one();

  /// wait for the worker
  {
    std::unique_lock<std::mutex> lk(m);
    cv.wait(lk, []{return processed; });
  }
  std::cout << "waiting notify_one result" << std::endl;
  sleep(5); 
  
  worker.join();
  worker2.join();
  worker3.join();
  worker4.join();
}
    

