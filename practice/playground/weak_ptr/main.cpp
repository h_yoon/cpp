//2022 03 16  hsy
// weak_ptr practice
// weak_ptr로 여러 컨테이너에 저장하고, 
// 원본 파괴 확인 방법

#include <iostream>
#include <memory>
#include <string>
#include <map>
#include <list>

class OBJ {
public:
	OBJ(int i) : id(i) {}
	int id =111;
};


int main() 
{

	std::list<std::shared_ptr<OBJ>> list1;	
	std::list<std::weak_ptr<OBJ>> list2;

	for ( int i = 0; i<10; ++i)
	{
		auto sp = std::make_shared<OBJ>(i);
		auto wp = std::weak_ptr<OBJ>(sp);
		list1.push_back(sp);
		list2.push_back(wp);
	}

	std::cout << "before remove" << std::endl;
	for(const auto &it : list2) {
		std::shared_ptr<OBJ> val = it.lock();
		std::cout << "print:" << val->id << std::endl;
	}
	
		
	int i=0;

	for (auto it : list1) {
		if (i==3) {
			list1.remove(it);
			std::cout << "erased " << i << std::endl;
			break;
		}
		i++;
	}

	std::cout << "after remove" << std::endl;
	for(auto it : list2) {
		std::shared_ptr<OBJ> val = it.lock();
		if(it.expired()){
			std::cout << "expired" << std::endl;
		}
		if(val)
			std::cout << "print:" << val->id << std::endl;
	}

return 0;
}
