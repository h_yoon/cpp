// async class test
// 2022 03 23 hsyoon


#include "TimeCounter.h"

#include <unistd.h>
#include <iostream>
#include <future>
#include <chrono>
#include <vector>
#include <list>


size_t is_prime( size_t x) {
	for (size_t i=2; i<x; ++i)  {
		if (x % i == 0) {
//     std::cout << i << std::endl;
     return 0;
    }
	}

	return x;
}

class ProcessManager {
public:
  ProcessManager(const int& count) : thread_count(count) {};
  virtual ~ProcessManager() = default;

  bool init();
  void run();
  void print() {
    std::cout << "found prime count " << primes.size() << std::endl;
    for ( const auto &it : primes) {
      std::cout << it << ",";
    }
    std::cout << std::endl;
  }

private:
  int checker();
  void dispense(const int& cnt);

  bool increas_gap() { point += distance;}

private:
  std::list<std::future<size_t>> results;
  size_t point;
  std::vector<size_t> primes;
  int thread_count = 0;

private:
  static size_t distance;  // 하나의 async로 처리할 양
};

size_t ProcessManager::distance = 1;

int
ProcessManager::checker() {
  int com = 0;
  std::chrono::microseconds wms(1);

 std::list<std::future<size_t>>::iterator iter;
  for (iter = results.begin(); iter !=results.end(); ++iter) 
  {
    if (iter->wait_for(wms) == std::future_status::ready) {
      size_t ret = iter->get();

      if (ret > 0) {
       // std::cout << "found prime" << ret << std::endl;
        primes.push_back(ret);
      }
      results.erase(iter++);
      ++com;
    }
  }
  return com;
}

void
ProcessManager::dispense(const int &cnt) {

  for (int i=0; i<cnt; ++i) {
    increas_gap();
    //std::cout << "point:" << point << std::endl;
    results.push_back(std::async(std::launch::async, is_prime, point));
  }
}

bool 
ProcessManager::init() {
  point=2;

  dispense(thread_count);

  return true;
}

void
ProcessManager::run() {

  
  while(primes.size()<10000){
    int cnt = checker();
    if(cnt) {
      dispense(cnt);
    }else{
      usleep(100);
    }
  }
}

int main(int argc, char **argv)
{

  int pc = 0;
  if ( argc < 2) {
    std::cout << "need async count" << std::endl;
    return 0;
  } else {
   pc = atoi(argv[1]);
   std::cout << "count is " << pc<< std::endl;
  }
   
  ProcessManager *manager = new ProcessManager(pc);

  TimeCounter tc;

  tc.start();
  manager->init();
  std::cout << "init period:" << tc.lab() << std::endl;
  manager->run();
  std::cout << "init period:" << tc.lab() << std::endl;
  tc.stop();
  //manager->print();
  tc.print();
	return 0;

}
