// async를 사용한 병렬 처리 연습 1

// wait_for()에서 deferred 가 출력 되는데 ..
// async 호출시 std::launch::async 호출추가

#include "TimeCounter.h"
#include <unistd.h>
#include <iostream>
#include <future>
#include <chrono>

size_t number = 444444443 ;

bool is_prime( size_t x) {
	for (int i=2; i<x; ++i)  {
		if (x % i == 0) {
    std::cout << i << std::endl;
     return false;
    }
	}

	return true;
}


int main()
{
	TimeCounter tcnt;

	tcnt.start();
	std::future<bool> fut = std::async(std::launch::async, is_prime, number);
	
	std::cout << "checking, please wait"<< std::endl;
	std::chrono::microseconds span(1);
	std::chrono::microseconds span2(10000);

	if (fut.wait_for(span) == std::future_status::deferred) {
		std::cout << "deferred" << std::endl;

	}else {
		while(fut.wait_for(span2) != std::future_status::ready ) {
			std::cout << "not ready" << std::endl;
		}
	}
	tcnt.stop();
	std::cout << "end" << std::endl;

	bool x = fut.get();

	std::cout << number << " " << (x ? "is" : "is not") << " prime time_ms:" << tcnt.get()<< std::endl;

	return 0;

}
