
#include <future>
#include <iostream>
#include <string>


void worker(std::promise<std::string> *p) 
{
	// 해당 결과는 future에 입력된다 .
	p->set_value("some test data in thread");
}

int main()
{
	std::promise<std::string> p;

	// 미래에 string data를 돌려 주겠다는 선언 
	std::future<std::string> data = p.get_future();

	std::thread thr(worker, &p);

	data.wait();

	std::cout << "recv data:" << data.get() << std::endl;

	thr.join();
}
