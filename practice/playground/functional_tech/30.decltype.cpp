// decltype test

#include <iostream>

template <typename A, typename B>
auto add(A a, B b) -> decltype(a+b)
{
	return a+b;
}

int main() 
{
	auto r = add(1,2.3);

	std::cout << "result: " << r << std::endl;
	return 0;
}

