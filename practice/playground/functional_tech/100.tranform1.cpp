
// transform v1
// vector에 있는 수를 다른 배열에 제곱하여 저장


/*
	std::transform(begin, end, std::back_inserter(dest-vector), perform function-name)
							   시작,  종료, 다른 벡터뒤에 저장, 실행함수

  std::copy_if(begin, end, std::back_inserter(dst-vector), []()->bool)
	 조건이 만족하는 값만 복사  
  std::remove_copy_if(begin, end, std::back_inserter(dst-vector), []()->bool)
	 조건이 만족하지 않는 값만 복사 
*/
#include <vector>
#include <algorithm>
#include <iostream>


auto main() -> int 
{

	std::vector<int> src = {1,2,3,4,5,};
  std::vector<int> dst;


	std::transform(std::begin(src), std::end(src), std::back_inserter(dst), [](int i) { return i * i; });

	std::cout << "transform test" << std::endl;
	for (const auto &i : dst) {
	  std::cout << i << std::endl;
	}


	dst = {};
  std::copy_if(std::begin(src), std::end(src), std::back_inserter(dst), 
	[](int i)->bool{ 
		if ((i%2)==0) 
			return true;
		else
			return false;
	});

	std::cout << "2의 배수만 복사 " << std::endl;
	for (const auto &i : dst) {
	  std::cout << i << std::endl;
	}


	dst = {};
  std::remove_copy_if(std::begin(src), std::end(src), std::back_inserter(dst), 
	[](int i)->bool {
		if ((i%2)==0) 
			return true;
		else
			return false;
	});

	std::cout << "2의 배수가 아닌 경우 복사 " << std::endl;
	for (const auto &i : dst) {
	  std::cout << i << std::endl;
	}

	return 0;
}
