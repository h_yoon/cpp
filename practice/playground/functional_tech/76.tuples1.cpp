// tuple simple test

#include <iostream>
#include <tuple>


auto main() -> int
{
  // create tuple 
	std::tuple<int, std::string, bool> t1(1, "test", true);
	auto t2 = std::make_tuple(2, "test2", false);

  // print tuple value
	std::cout << std::get<0>(t1) << std::endl;
	std::cout << std::get<1>(t1) << std::endl;
	std::cout << std::get<2>(t1) << std::endl;


	int a;
  std::string b;
  bool c;

  // get by std::tie()
	std::tie(a,b,c) = t1;
	std::cout << a << b << c << std::endl;


 	// get by tie with std::ignore
	std::tie(std::ignore,b,std::ignore) = t1;
	std::cout <<  b << std::endl;
  
  return 0;
}
