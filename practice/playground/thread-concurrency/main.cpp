// 2023. 01. hsy
// 멀티스레딩 정복하기 챕터4. p.102

#include "Dispatcher.h"
#include "Request.h"

#include <iostream>
#include <string>
#include <csignal>
#include <thread>
#include <chrono>

sig_atomic_t signal_caught = 0;
std::mutex logMutex;

void sigint_handler(int signal) {
    signal_caught = 1;
}

void logFunc(std::string text) {
    logMutex.lock();
    std::cout << text << std::endl;
    logMutex.unlock();
}

int main() {
    signal(SIGINT, &sigint_handler);
    Dispatcher::init(10);

    std::cout << "Initialised" << std::endl;
    int cycles = 0;

    Request* req = 0;

    while(!signal_caught && cycles < 50)
    {
        req = new Request();
        req->setValue(cycles);
        req->setOutput(&logFunc);
        Dispatcher::addRequest(req);
        cycles++;
    }

    std::this_thread::sleep_for(std::chrono::seconds(5));
    Dispatcher::stop();

    std::cout << "Clean-up done." << std::endl;
    return 0;
}
