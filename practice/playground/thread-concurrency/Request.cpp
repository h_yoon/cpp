
#include "Request.h"

void Request::process() 
{
    outFunc("Staring process request " + std::to_string(value) + "...");
    ///
}

void Request::finish() 
{
    outFunc("Finished request " + std::to_string(value));
}