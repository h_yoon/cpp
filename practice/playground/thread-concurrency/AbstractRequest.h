#pragma once
#ifndef ABSTRCT_REQEUST_H
#define ABSTRCT_REQEUST_H

class AbstractRequest {
public:
    virtual void setValue(int value) = 0;
    virtual void process() = 0;
    virtual void finish() = 0;
};

#endif 