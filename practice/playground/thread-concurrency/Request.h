#pragma once
#ifndef REQUEST_H
#define REQUEST_H

#include <string>

#include "AbstractRequest.h"

typedef void(*logFunction)(std::string text);

class Request : public AbstractRequest 
{
public:
    void setValue(int value) { 
        this->value = value;
    }
    void setOutput(logFunction func) {
        outFunc = func;
    }
    void process();
    void finish();

private:
    int value;
    logFunction outFunc;

};

#endif