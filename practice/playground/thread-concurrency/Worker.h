#pragma once
#ifndef WORKER_H
#define WORKER_H

#include <mutex>
#include <condition_variable>
#include "AbstractRequest.h"

class Worker
{
public:
    Worker() {
        running = true;
        ready = false;
        unlock = std::unique_lock<std::mutex>(mtx);
    }

    void run();
    void stop() { running = false; }
    void setRequest(AbstractRequest* request) { 
        this->request = request;
        ready =true;
    }
    void getCondition(std::condition_variable* &cv);
private:
    std::condition_variable cv;
    std::mutex mtx;
    std::unique_lock<std::mutex> unlock;
    AbstractRequest* request;
    bool running;
    bool ready;

};

#endif