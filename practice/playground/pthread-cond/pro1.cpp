

#include <thread>
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <mutex>

pthread_mutex_t mtx;
pthread_cond_t	cond;
int data;

static 
void increase()
{
	while(true)
	{
		pthread_mutex_lock(&mtx);
		data++;
		std::cout << pthread_self() << "data plus: " << data <<std::endl;
		pthread_cond_signal(&cond);
		pthread_mutex_unlock(&mtx);
		sleep(1);
	}
}

static
void print()
{
	while(true)
	{
		pthread_mutex_lock(&mtx);
		pthread_cond_wait(&cond, &mtx);
		std::cout << pthread_self() << "data:" << data << std::endl;
		pthread_mutex_unlock(&mtx);
	}
}

int main()
{
	
	pthread_mutex_init(&mtx,NULL);
	pthread_cond_init(&cond,NULL);

	std::thread thr1(increase);
	//std::thread thr2(increase);
	std::thread thr3(print);

	thr1.join();
	//thr2.join();
	thr3.join();

	return 0;
}
