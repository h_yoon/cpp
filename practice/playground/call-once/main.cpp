//simple call once 
//hsy

#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

std::once_flag flag;

void callonce()
{
	std::cout << "called," << std::endl;
}

void call()
{	
	std::call_once(flag, callonce);
}

int main()
{

	std::thread th1(call);
	std::thread th2	(call);
	std::thread th3 (call);

	th1.join();
	th2.join();
	th3.join();


return 0;
}
