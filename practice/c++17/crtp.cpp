#include <iostream>
using namespace std;

template<typename T>
class Parent
{
public:
    void msgLoop()
    {
        // 부모 호출 
        OnClick();
        // this 포인터를 파생 클래스인 T 타입으로 캐스팅 후 호출    
        static_cast<T*>(this)->OnClick();
    }
    //가상함수로 만들면 가상함수 테이블이 관리됨
    void OnClick() { cout << "Parent OnClick" << endl; }
};

class Child : public Parent<Child>
{
public:
    void OnClick() { cout << "Child OnClick" << endl; }
};

int main()
{
    Child fw;
    fw.msgLoop(); // OUTPUT : FrameWindow OnClick
}

