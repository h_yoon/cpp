
#2021 10 18 
#SAN certificate(멀티도메인)

1. 개인키 생성
openssl genrsa -des3 -out server.key 2048
--> 비번 입력 (1111)

2. 인증요청서 생성 (IP 입력)
openssl req -new -key server.key -out server.csr
--> 무조건 enter

3. 개인키에서 Pwd 제거
cp server.key server.key.origin
openssl rsa -in server.key.origin -out server.key



4. 인증서 생성 (SAN 방식)
openssl.cnf
-------------------------------
[ v3_ca ]
subjectAltName=IP:192.168.14.4
-------------------------------
openssl x509 -req -in server.csr -extfile ./openssl.cnf -extensions v3_ca -days 3650 -signkey server.key -out server.crt



5. 인증서 확인
openssl x509 -in server.crt -text -noout
-------------------------------
X509v3 extensions:
X509v3 Subject Key Identifier:
12:17:EA:C9:FF:48:2E:C3:AD:24:92:D2:0E:4D:FA:09:67:6C:FC:4A
X509v3 Authority Key Identifier:
keyid:12:17:EA:C9:FF:48:2E:C3:AD:24:92:D2:0E:4D:FA:09:67:6C:FC:4A



X509v3 Basic Constraints:
CA:TRUE
X509v3 Subject Alternative Name:
IP Address:192.168.14.4
-------------------------------
5. curl 테스트
curl -i -X GET -H "Content-type: application/json" -H "X-Auth-Token: 5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5" https://192.168.14.4:12553/easm/v1/eas/acid/com.android.conference2 --cacert server.crt


6. crt 파일 client에 전달.
