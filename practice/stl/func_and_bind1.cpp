// std::function practice 
// g++ func_and_bind1.cpp -o b1 --std=c++11

#include <iostream>
#include <functional>

class Test
{
public:
  void addAndPrint(int a, int b)
  {
    std::cout << "sum:" << a + b<< std::endl;
  }
};

int main()
{
  Test test;
  std::function<void(Test&, int, int)> func1 = &Test::addAndPrint;
  func1(test,10,20);
  return 0;
}


