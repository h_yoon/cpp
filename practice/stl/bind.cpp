/* member function binding
 */

#include <functional>
#include <iostream>

class Test 
{
public:
  void AddPrint(int a, int b)
  {
    std::cout<< "sum:" << a+b << std::endl;
  }

  void FuncBind(std::function<void(int,int)> &dest)
  {
    dest = std::bind(&Test::AddPrint, this,std::placeholders::_1, std::placeholders::_2);
  }

};

int main()
{

 Test test;
 std::function<void(int, int)> func1;
 test.FuncBind(func1);
 func1(100,300);
 return 0;
}
