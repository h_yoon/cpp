/* async practice 3 
 * 
 note
  std::async(std::launch::async   ,{fn} ,{args...})
             std::lanuch::deferred
 launch::async - run immediately in other thread
 launch::deferred - lazy evaluation. run in same thread when result is needed
 */

#include <iostream>
#include <future>
#include <thread>
#include <memory>

//void sum(const int &beg, const int& end, std::shared_ptr<std::promise<int>> prom) 
void sum(const int &beg, const int& end, std::promise<size_t> &&prom) 
{
  size_t ret=0;
  for (size_t i=beg; i<end; ++i) {
     ret += i;
  }

  //prom->set_value(ret); 
  prom.set_value(ret); 
}


int run(const size_t&end, const int &tc)
{
  //std::shared_ptr<std::promise<int>> ptrprom = std::make_shared<std::promise<int>>();
  std::promise<size_t> prom;
  std::future<size_t> future = prom.get_future();

  auto t = std::async(std::launch::async, sum, 0, end, std::move(prom));
  std::cout << "print" << future.get() << std::endl;
  return 0;
}

int main(int argc, char **argv)
{
  std::cout << "start" << std::endl;
  return run(10000000000,1);
  std::cout << "end" << std::endl;
}
