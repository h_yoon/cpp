/* lambda and function practice
 * [capture](parameter) -> return type { body } 
 * note: - 캡처
            [&] = 모든내용 캡처
            [&a, b] = a변수만 참조, b변수 값복사
         - 리턴가능 '->' 사용 
         - std::function으로 객체화.
         - 클래스처럼 람다함수 종료시 ';' 추가.
 */

#include <iostream>
#include <functional>

int main()
{

  int a,b;

  a=10;
  b=20;
  std::function<int(int&,int&)> fn = [&](int &x,int &y) -> int {
                                      return x+y;
                                    };

  std::cout << "result:" << fn(a,b) << std::endl;

}

