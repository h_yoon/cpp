#include "common/StaticConfig.h"
#include "common/MainManager.h"

#include "Client.h"
#include "ClientManager.h"
#include "ReactorManager.h"

#include <signal.h>

using namespace std;

int main(int argc, char** args)
{
  (void)argc;

  MainManager manager;
  signal(SIGPIPE, SIG_IGN);

  if (manager.init() == 0) 
     std::cout <<__FILE__<<__LINE__<< "fail init MainManager" <<std::endl;

  ReactorManager *reactors = new ReactorManager();
  
  if (reactors->init(&(manager.getConfig())) == 0) 
  {
    std::cout <<__FILE__<<__LINE__<< " fail client init" <<std::endl;
    return 0;
  }
  
  reactors->start();
  
  ClientManager clients(&(manager.getConfig()));

  clients.init(reactors); // first event trigger
  clients.start();

  CMtxLockGuard waiter_;
  while(true)
  {
    waiter_.wait_ms(1000);
  }
  //client->join();
  return 0;
}

