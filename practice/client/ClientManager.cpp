
#include "ClientManager.h"

#include "reactor/Reactor.h"

void
ClientManager::server_info_add(const std::string &server_name, const std::string &ip, const int &port)
{
  // TODO verification of Server Info

  ILOG << "load server, name:" << server_name << "ip:" << ip << "port:"<< port;
  server_list_.emplace(server_name, ServerInfo(ip, port));
}

bool
ClientManager::server_info_find(const std::string &server_name, ServerInfo &info)
{
  return server_list_.find(server_name, info);
}

bool 
ClientManager::init(ReactorManager *reactors)
{

  server_info_add("ECS", conf->get("ECS_IP"), std::stoi(conf->get("ECS_PORT")));
  server_info_add("EES", conf->get("EES_IP"), std::stoi(conf->get("EES_PORT")));

  int count = std::stoi(conf->get("CLIENT_COUNT"));

  if(count <= 0)
  {
    ILOG << "error client count";
    return false;
  }

  ILOG << "a number of client is " << count;

  Client *client = nullptr;
  size_t eec_id = 10000000;

  for (size_t start = 0; start < count; ++start)
  {
    client = new Client(reactors, server_list_);

    client->init(eec_id);
    ++eec_id;

    client_list_.push_back(new Client(reactors, server_list_));
  }
  return true;
}

void
ClientManager::start()
{
  for (auto &client : client_list_)
  {
        ILOG;
    client->process();
  }
}