// ClientManager.h
// 2021.11.01 hsy

#ifndef H_ClientManager__
#define H_ClientManager__

#include "Client.h"
#include "ServerInfoList.h"
#include "reactor/ReactorManager.h"
#include "common/StaticConfig.h"
#include "common/CStreamLogger.h"

#include <deque>


class ClientManager
{
public:
  ClientManager(StaticConfig *_conf) : conf(_conf){}
  virtual ~ClientManager() = default;
  bool init(ReactorManager *reactors);

  void start();
  
  bool server_info_find(const std::string &server_name, ServerInfo &info);
  void server_info_add(const std::string &server_name, const std::string &ip, const int &port);

  StaticConfig *conf;

private:
  ServerInfoList server_list_;
  std::deque<Client*> client_list_;
};




#endif