
#include "Client.h"
#include "Connector.h"

bool
Client::init(const size_t &eec_id)
{
  custom_attr.set_eecid(eec_id);
  request_info.type = RequestType::REQUEST;
  
  return true;
}

/// reactor에 의해 request callback()
void 
Client::request()
{
  request_info.type = RequestType::RESPONSE;

  /// Check Server Connection 
  if (is_connect() == false)  
  {
    // send_event();
    req_connect("ECS");
  }
  else
  {
    req_connect("ECS");
  }
}

/// reactor에 의해 response callback()
bool
Client::response()
{
  request_info.type = RequestType::REQUEST;
  return true;
}

/// request connection to connector
void
Client::req_connect(const std::string &server_name)
{
  if (target_servers.find(server_name, this->connect_server)==true)
  { 
    reactors_->add_connect(this);
  }
}

/// request connection to reactor
bool
Client::send_event() 
{
  return reactors_->add_event(this);
}

void 
Client::close_connection()
{
  if (is_connect() == false)
    return;

  ::close(epoll_eve_.data.fd);
  set_connect(false);
}

bool
Client::process()
{
  request();
  return true;
}


