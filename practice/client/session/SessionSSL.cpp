
#include "SessionSSL.h"
#include "SslCtx.h"

#include "CStreamLogger.h"

bool
SessionSSL::init()
{
  
  if ( inited_ == false) 
  {
    SSL *ssl = ssl_ctx_maker_->create();
    inited_  = cssl_.Init(fd_sock_, ssl);
  }
  return inited_;
}

bool
SessionSSL::connect()
{
  if (init() == false)
    return false;

  cssl_.Connect();
  return true;
}

int
SessionSSL::disconnect()
{
  return cssl_.Clear();
}

int 
SessionSSL::read(std::string &message)
{
  return cssl_.Read(message);
}

int 
SessionSSL::write(const std::string &message)
{
  std::string msg;
  // TODO 여기서 수동으로 하지말고, http 파서가 필요하다!
  msg += "POST /test HTTP/1.1\n";
  msg += "User-Agent: hyoon\n";
  msg += "Host: 127.0.0.1:10020\n";
  msg += "Accept: */*\n";
  msg += "Content-Length: 6\n";
  msg += "Content-Type: application/json\r\n\r\n";
  msg += "HIHIHI";
  msg += "\r\n\r\n";
  
   ILOG << "send message\n" << msg;
  return cssl_.Write(msg);
}

