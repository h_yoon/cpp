// SessionSSL.h
// 2021.07.14 hsy


#ifndef H_SessionSSL_
#define H_SessionSSL_

#include "Session.h"
#include "CSsl.h"

class SslCtx;

class SessionSSL : public Session
{
public:
  SessionSSL(const int &fd_sock) : Session(fd_sock) {}
  virtual ~SessionSSL() = default;

  void  ctxSetter(SslCtx *ctx) { ssl_ctx_maker_ = ctx; }
  bool  init();
  bool  connect();
  int   disconnect();
  int   read(std::string &message);
  int   write(const std::string &message);

private:
  CSsl    cssl_;
  SslCtx *ssl_ctx_maker_;

  bool  inited_   = false;
};

#endif

