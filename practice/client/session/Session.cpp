

#include "Session.h"
#include "HttpProc.h"
#include "common/CUtil.h"
#include <deque>
#include <iostream>
#include "CStreamLogger.h"


bool
Session::request_process(const std::string &message)
{
  ILOG << "session message:" << message;
  std::deque<std::string> lines = CUtil::tokenizer(message,'\n');

  for ( auto &line : lines)
  {
    ILOG << "line:" << line;
  }

  return true;
}
