
#include "SessionNoSSL.h"
#include "CStreamLogger.h"
#include <unistd.h>

size_t SessionNoSSL::read_message_length_ = 512;

bool
SessionNoSSL::init()
{
  inited_ = true;

  return inited_;
}

bool
SessionNoSSL::connect()
{
  if (init() == false)
    return false;

  return true;
}

int
SessionNoSSL::disconnect()
{
  return 1;
}

int 
SessionNoSSL::read(std::string &message)
{
  char buf[SessionNoSSL::read_message_length_] = {0,};

  size_t read_length = ::read(fd_sock_,buf, read_message_length_);
  
  if(read_length)
    message.assign(buf);

  return read_length;
}

int 
SessionNoSSL::write(const std::string &message)
{
  std::string msg;
  // TODO 여기서 수동으로 하지말고, http 파서가 필요하다!
  msg += "POST /test HTTP/1.1\n";
  msg += "User-Agent: hyoon\n";
  msg += "Host: 127.0.0.1:10021\n";
  msg += "Accept: */*\n";
  msg += "Content-Length: 6\n";
  msg += "Content-Type: application/json\r\n\r\n";
  msg += "HIHIHI";
  msg += "\r\n\r\n";
  
  ILOG << "send message\n" << msg;
  return ::write(fd_sock_, msg.c_str(), msg.length());
}

