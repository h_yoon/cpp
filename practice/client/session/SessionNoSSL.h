// SessionNoSSL.h
// 2022.10.1 hsy


#ifndef H_SessionNoSSL_
#define H_SessionNoSSL_

#include "Session.h"

class SessionNoSSL : public Session
{
public:
  SessionNoSSL(const int &fd_sock) : Session(fd_sock) {}
  virtual ~SessionNoSSL() = default;

  bool  init();
  bool  connect();
  int   disconnect();
  int   read(std::string &message);
  int   write(const std::string &message);

private:
  static size_t read_message_length_;

  bool  inited_   = false;
};

#endif

