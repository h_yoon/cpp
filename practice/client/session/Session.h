// Session.h
// 2021.07.14 hsy

#ifndef H_Session_
#define H_Session_

#include "Processor.h"

#include <string>

class Session
{
public:
  Session(const int& fd) : fd_sock_(fd) {}
  virtual ~Session() = default;

  virtual bool init() = 0;
  virtual bool  connect() = 0;
  virtual int  disconnect() = 0;
  virtual int  read(std::string &message) = 0;
  virtual int  write(const std::string &message) = 0;

  bool process(std::string &message) { return processor_.process(message); }
  bool request_process(const std::string &message);
protected:
  bool  isInit()     { return inited_;   }
  
  int   fd_sock_ = 0;
  bool  inited_ = false;
  
  Processor processor_;
  
};

#endif

