// EpollEvent.h
// 2021.09.28 hsy

#ifndef EpollEvent_Client_H_
#define EpollEvent_Client_H_

#include <sys/epoll.h>
#include <set>
#include <map>
#include <memory>
#include <string>

#include <arpa/inet.h>  // inet_addr

class EventHandler;
class Client;

// struct eventCompare    /// comparer, not used in current.
// {
//   bool operator()(const epoll_event &lhs, const epoll_event &rhs) const
//   {
//     return lhs.data.fd < rhs.data.fd;
//   }
// };

struct EpollEvent
{
  EpollEvent(Client *_client, struct epoll_event &_event) : client(_client), epoll_eve(_event) {}
  EpollEvent(Client *_client, struct sockaddr_in &_addr) : client(_client), addr(_addr) {}
  
  void operator=(const EpollEvent &rhs) {
    client    = rhs.client;
    addr      = rhs.addr;
    epoll_eve = rhs.epoll_eve;
  }

  void event_update(struct epoll_event &eve) { epoll_eve = eve; }
  int  fd_get() { return epoll_eve.data.fd; }
  
  Client             *client;
  struct sockaddr_in  addr;
  struct epoll_event  epoll_eve; /// epoll_eve
};

using EventQueue = std::map<int, std::shared_ptr<EpollEvent>>;
using EventMap   = std::map<int, std::shared_ptr<EventHandler>>;

#endif

