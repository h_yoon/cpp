
#include "Demuxer.h"
#include "EpollAction.h"
#include "CStreamLogger.h"
#include <unistd.h>
bool
Demuxer::init()
{
  if (epoll_.init()==false)
    return false;
  
  return true;
}

bool
Demuxer::update(const int &fd, struct epoll_event &event, const ActionType &action)
{
  // const uint32_t &types = event.events;
  switch (action)
  {
  //  case ActionType::ADD_ALL  : return event_add (fd, event, (EPOLLIN | EPOLLOUT | EPOLLERR));
  //  case ActionType::ADD_IN   : return event_add (fd, event, EPOLLIN );
  //  case ActionType::ADD_OUT  : return event_add (fd, event, EPOLLOUT);
  //  case ActionType::ADD_INOUT: return event_add (fd, event, (EPOLLIN | EPOLLOUT));
  //  case ActionType::DEL_IN   : return event_del (fd, event, types - EPOLLIN);
  //  case ActionType::CTL_ERR  : return event_ctl (fd, event, EPOLLERR); 

    case ActionType::DEL_ALL  : { ::close(fd);  return true; }

    case ActionType::CTL_IN   : return event_ctl (fd, event, EPOLLIN | EPOLLPRI); 
    case ActionType::CTL_OUT  : return event_ctl (fd, event, EPOLLOUT); 
    case ActionType::CTL_INOUT: return event_ctl (fd, event, EPOLLIN | EPOLLPRI | EPOLLOUT); 
    case ActionType::CTL_RDHUP: return event_ctl (fd, event, EPOLLRDHUP | EPOLLHUP); 
    case ActionType::CTL_WANT : return true; // inter process to retry
    default:
       ILOG << "ERROR need to handle step:" <<action;
    break;
  }
  return false;
}

int
Demuxer::event_receive(EventQueue &events, EventMap &handlers)
{
  int recv_cnt  = 0;
  EventHandler *ptr = nullptr;

  recv_cnt = epoll_.wait(0);

  for ( int i=0; i<recv_cnt; ++i)
  {
    epoll_event &eve = epoll_.getfd(i);
    int client_fd   = eve.data.fd;
    
    ptr = handlers[client_fd].get();
    ptr->event_update(eve);

    events.emplace(client_fd, std::make_shared<EpollEvent>(ptr->getEvent().get()->client, eve));
  }
  return recv_cnt;
}

// used in acceptor by async server
bool
Demuxer::event_register(const int &fd, struct epoll_event &event)
{
  return epoll_.add(fd, event, (EPOLLIN));
}

// used in connector by async client
bool
Demuxer::event_register_client(const int &fd, struct epoll_event &event)
{
  return epoll_.add(fd, event, (EPOLLIN | EPOLLOUT));
}

// used by client(user equpment)
bool
Demuxer::event_register_client(const int &fd, struct epoll_event &event, const EPOLL_EVENTS &epoll_enum)
{
  return epoll_.add(fd, event, epoll_enum);
}

bool
Demuxer::event_add(const int &fd, struct epoll_event &event, const uint32_t &type)
{
  return epoll_.add(fd, event, type);
}

bool
Demuxer::event_del(const int &fd, struct epoll_event &event, const uint32_t &type)
{
  bool ret = epoll_.ctl(fd,event,type);
  return ret;
}

bool
Demuxer::event_ctl(const int &fd, struct epoll_event &event, const uint32_t &type)
{
  return epoll_.ctl(fd,event,type);
}
