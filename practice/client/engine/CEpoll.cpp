
#include "CEpoll.h"
#include "CStreamLogger.h"
#include "fcntl.h"

#include <cstring>
CEpoll::CEpoll(const size_t &max_conn)
{
  max_connection_ = max_conn;
}

bool
CEpoll::init()
{
  if (create() == false)
  {
    ILOG << "ERROR fail, epoll create";
    return false;
  }
  return true;
}
bool
CEpoll::create()
{
  int epoll_fd;
  epoll_fd = epoll_create(max_connection_);

  if (epoll_fd <= 0)
  {
    ILOG << "ERROR epoll create"<< epoll_fd_ << max_connection_;
    return false;
  }
  epoll_fd_ = epoll_fd;
  return true;
}

void
CEpoll::nonblock_setter(const int &fd)
{
  int flag = fcntl( fd, F_GETFL, 0 );   /// set to non-blocking
  if (fcntl( fd, F_SETFL, flag | O_NONBLOCK )== -1)
     ILOG << "ERROR non-block setting"<< fd;
}


// epoll에 상태 모니터링할 소켓 등록 
bool
CEpoll::add(const int &fd, const uint32_t &event_type)
{
  // nonblock_setter(fd);
  struct epoll_event event;
  event.events = event_type;
  event.data.fd = fd;
  
  if(epoll_ctl(epoll_fd_, EPOLL_CTL_ADD , fd, &event) == -1)
  {
    ILOG << "ERROR EPOLL_CTL_ADD fd:"<< fd << "event_type:"<<event.events << "errno:"<<std::strerror(errno);
    return false;
  }
  else
  {
    nonblock_setter(fd);
    ILOG << "Success EPOLL_CTL_ADD fd:"<< fd << " event_type:"<<event.events;
    return true;
  }
}

bool
CEpoll::add(const int &fd, struct epoll_event &event, const uint32_t &event_type)
{
  // nonblock_setter(fd);
  event.events = event_type;
  event.data.fd = fd;

  if (epoll_ctl(epoll_fd_, EPOLL_CTL_ADD , fd, &event) == -1)
  {
    ILOG << "ERROR EPOLL_CTL_ADD fd:"<< fd << "event_type:"<<event.events << "errno:"<<std::strerror(errno);;
    return false;
  }
  else
  {
    nonblock_setter(fd);
    // ILOG << "Success EPOLL_CTL_ADD fd:"<< fd << " event_type:"<<event.events;
    return true;
  }
}


bool 
CEpoll::ctl(const int &fd, struct epoll_event &event, const uint32_t &event_type)
{
  event.events = event_type;
  if(epoll_ctl(epoll_fd_, EPOLL_CTL_MOD, fd, &event) == -1)
  {
    ILOG << "Error EPOLL_CTL_MOD" << event.events << "fd" << fd << "errno:"<<std::strerror(errno);;
    return false;
  }
  else
  {
    // ILOG << " Success EPOLL_CTL_MOD fd:"<< fd << " event_type:"<<event.events;
    return true;
  }
}

int
CEpoll::wait(const int &timeout)
{
  int cnt = 0;

  cnt = epoll_wait(epoll_fd_, events_, max_connection_, timeout); 
  return cnt;
}

void
CEpoll::copy(const int &count, struct epoll_event *events)
{
  memcpy(events, events_, sizeof(struct epoll_event)*count);
  // events = events_;
  return;
}

bool 
CEpoll::remove(const int &fd, struct epoll_event &event)
{
  if (epoll_ctl(epoll_fd_, EPOLL_CTL_DEL, fd, &event) == -1)
  {
    ILOG << "ERROR EPOLL_CTL_DEL fd:"<< fd << "errno:"<<std::strerror(errno);;
    return false;
  }
  else
  {
    return true;
  }
}

struct epoll_event&
CEpoll::getfd(const int &num)
{
  return events_[num];
}
