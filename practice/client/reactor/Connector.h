/// Connector.h
/// 2021.10.04 hsy

#ifndef __H_CONNECTOR__
#define __H_CONNECTOR__


#include "engine/CEpoll.h"
#include "engine/EpollEvent.h"

#include "common/CThread.h"
#include "common/CSpinlock.h"
#include "common/CMtxLockGuard.h"
#include "common/CSingletone.h"

#include <set>
#include <deque>
#include <netinet/in.h>
#include <arpa/inet.h>  // inet_addr
#include <sys/socket.h>

class Connector
{
public:
  Connector();
  virtual ~Connector();

  void request_connect(Client *ue);
  int  client_connect(struct sockaddr_in &addr);
  int  event_receive(EventQueue &event);

protected:

private:
  std::deque<EpollEvent> addr_list_;

  int connection_limit_ = 1;
  int connection_count_ = 0;
  int message_count_    = 0;

  struct sockaddr_in serv_addr_;
  EventQueue events_;
  CSpinlock event_spin_;
  CSpinlock addr_list_spin_;
};


#endif
