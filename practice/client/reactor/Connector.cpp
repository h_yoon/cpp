
#include "Connector.h"
#include "Client.h"

#include "engine/EpollEvent.h"
#include "common/CStreamLogger.h"
#include <unistd.h>

Connector::Connector()
{

}

Connector::~Connector()
{

}

void
Connector::request_connect(Client *client)
{

  ServerInfo &info = client->server_info();

  std::string ip_addr = info.ip;
  int         port    = info.port;
  struct sockaddr_in serv_addr;

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port); 
  serv_addr.sin_addr.s_addr = inet_addr(ip_addr.c_str());
  memset(&(serv_addr_.sin_zero), 0x00, sizeof(serv_addr.sin_zero)); /* zero the rest of the struct */

  addr_list_spin_.Lock();
  addr_list_.emplace_back(EpollEvent(client, serv_addr)); // epoll_event를 할당하지 못한 상태
  addr_list_spin_.unLock();
}

int 
Connector::event_receive(EventQueue &reactor_event)
{
  int sock_fd;
  size_t connection = 0;
  std::deque<EpollEvent> addr_list = {};

  addr_list_spin_.Lock();
  addr_list_.swap(addr_list);
  addr_list_spin_.unLock();
  
  for(EpollEvent &client_event : addr_list)
  {
    sock_fd = client_connect(client_event.addr);
    
    if(sock_fd > 0)
    {
      epoll_event new_event;
      new_event.data.fd = sock_fd;
      new_event.events  = EPOLLOUT;   /// Client. Epoll Out
      // event_spin_.Lock();
      
      reactor_event.emplace(sock_fd, std::make_shared<EpollEvent>(client_event.client, new_event));
      // event_spin_.unLock();
      connection++;
    }
    else
    {
      ILOG << "fail connect" << sock_fd;
    }
  }
  return connection;
}

int
Connector::client_connect(struct sockaddr_in &addr)
{
  int sock_fd = 0;

  if((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    ILOG << "Error : Could not create socket";
    return 0;
  }

  int ret = ::connect(sock_fd, (struct sockaddr *)&addr, sizeof(addr));

  if(ret<0) {
    ILOG << "Connect Failed errno:" << errno;
    switch (errno)
    {
    case EACCES:
        std::cout << "EACCESS" << std::endl;
        break;
    case EPERM:
        std::cout << "EPERM" << std::endl;
        break;
    case EADDRINUSE:
        std::cout << "EADDRINUSE" << std::endl;
        break;
    case EAFNOSUPPORT:
        std::cout << "EAFNOSUPPORT" << std::endl;
        break;
    case EAGAIN:
        std::cout << "EAGAIN" << std::endl;
        break;
    case EALREADY:
        std::cout << "EALREADY" << std::endl;
        break;
    case EBADF:
        std::cout << "EBADF" << std::endl;
        break;
    case ECONNREFUSED:
        std::cout << "ECONNREFUSED" << std::endl;
        break;
    case EFAULT:
        std::cout << "EFAULT" << std::endl;
        break;
    case EINPROGRESS:
        std::cout << "EINPROGRESS" << std::endl;
        break;
    case EINTR:
        std::cout << "EINTR" << std::endl;
        break;
    case EISCONN:
        std::cout << "EISCONN" << std::endl;
        break;
    case ENETUNREACH:
        std::cout << "ENETUNREACH" << std::endl;
        break;
    case ENOTSOCK:
        std::cout << "ENOTSOCK" << std::endl;
        break;
    case ETIMEDOUT:
        std::cout << "ETIMEDOUT" << std::endl;
        break;
    default:
        std::cout << "unknown error" << errno << std::endl;
        break;
    }
    return 0;
  }
  return sock_fd;
}

