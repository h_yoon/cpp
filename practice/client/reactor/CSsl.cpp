// CSsl.cpp
// 2021.10.09 hsy


#include "CSsl.h"
#include "SslCtx.h"
#include "CStreamLogger.h"

CSsl::CSsl()
{
 
}

int 
CSsl::Clear()
{ 
  SSL_shutdown(ssl_);
   
  SSL_free(ssl_); // it includes ::free(ssl)
  return 1;
}

bool
CSsl::Init(const int& io_handle, SSL *ssl)
{
  client_cert_ = NULL;
  fd_sock_ = io_handle;
  ssl_ = ssl;
  return true;
}

void 
CSsl::Connect()
{
  if (SSL_set_fd(ssl_, fd_sock_)==0){
     ILOG << "ERROR Fail CSsl::Connect SSL_set_fd()";
     return;
  }
  
  SSL_set_connect_state(ssl_);
}

int
CSsl::Read(std::string &message)
{
  char buffer[MAX_RECV_SIZE] = {0,};

  client_cert_ = SSL_get_peer_certificate(ssl_);

  int err = SSL_read(ssl_, buffer, MAX_RECV_SIZE-1);

  X509_free(client_cert_);  // if not free X509, it's memory leak point 2022.04.18

  if(err <= 0)
  {
    switch (SSL_get_error(ssl_, err) )
    {
      case SSL_ERROR_WANT_READ :
      case SSL_ERROR_WANT_WRITE:
      case SSL_ERROR_ZERO_RETURN :    // epoll_in case
        err = 0;
        break;
      case SSL_ERROR_SYSCALL :
        std::cout << "error SSL_ERROR_SYSCALL " << err << std::endl;
        err = -1;
        break;
      case SSL_ERROR_SSL :
        std::cout << "error SSL_ERROR_SSL " << err << std::endl;
        err = -1;
        break;
      default:
        std::cout << "error default " << err << std::endl;
        err = -1;
        break;
    }
    return err; 
  }

  message.assign(buffer);
  return message.length();
}

/*
 * result > 0 : Success
 *                              = 0 : retry
 *                              < 0     : error
 */
int
CSsl::Write(const std::string& message)
{
  int write_result = SSL_write(ssl_, message.c_str(), message.length() );

  if(write_result <= 0)
  {
    switch (SSL_get_error(ssl_, write_result) )
    {
    case SSL_ERROR_WANT_READ :
    case SSL_ERROR_WANT_WRITE:
      write_result = 0;
      break;
    case SSL_ERROR_ZERO_RETURN:    //ERR need to delete
      std::cout << __func__ << " SSL_ERROR_ZERO_RETURN Failed Send(error_code:"<< write_result<< ")" <<std::endl;
      break;
    case SSL_ERROR_SYSCALL:
      std::cout << __func__ << " SSL_ERROR_SYSCALL Failed Send(error_code:"<< write_result<< ")" <<std::endl;
      break;
    case SSL_ERROR_SSL:
      std::cout << __func__ << " SSL_ERROR_SSL Failed Send(error_code:"<< write_result<< ")" <<std::endl;
      ERR_print_errors_fp(stderr);
      break;
    default:
      std::cout << __func__ << " default case (error_code:"<< write_result<< ")" <<std::endl;
      ERR_print_errors_fp(stderr);
      break;
    }
  }

  return write_result;
}

