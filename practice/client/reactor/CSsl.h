// CSsl.h
// 2021.07.17 hsy


#ifndef H_CSsl_
#define H_CSsl_


#include <iostream>
#include <string>

#include <openssl/rsa.h>
#include <openssl/crypto.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#define CERTF "cert/server.crt"
#define KEYF  "cert/server.key"

#define MAX_RECV_SIZE  512

class CSsl
{
public:
  CSsl();
  virtual ~CSsl() = default;

  bool  Init(const int& io_handle, SSL *ssl);
  void  Connect();
  int   Read(std::string &message);
  int   Write(const std::string &message);
  int   Clear();

private:
  int               fd_sock_;
  SSL_CTX          *ctx_;
  SSL              *ssl_;
  X509             *client_cert_;
  const SSL_METHOD *meth_;
  std::string       msg_req_;
  std::string       msg_res_;
};

#endif
