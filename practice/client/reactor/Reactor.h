// Reactor.h
// 2021.07.18 hsy

#ifndef H_REACTOR__
#define H_REACTOR__

#include "handler/EventHandler.h"

#include "reactor/SslCtx.h"
#include "reactor/Connector.h"

#include "engine/EpollEvent.h"
#include "engine/EpollAction.h"
#include "engine/Demuxer.h"

#include "common/CThread.h"
#include "common/CSpinlock.h"
#include "common/CMtxLockGuard.h"

#include <unistd.h>
#include <map>
#include <deque>
#include <iostream>


class Reactor: public CThread
{
public:
  Reactor(const bool &ssl_enable = true) { ssl_enable_ = ssl_enable; }
  virtual ~Reactor()	= default;
  bool  init();

  void request_connect(Client *ue);
  bool request_event  (Client *ue);

protected:
  virtual void run();

private:
  bool  react(EventHandler *handler, ActionType &result);
  int   event_receive(EventQueue &event_local);
  void  process();
  
private:
  bool  ssl_enable_ = true;

  Connector connector_;
  Demuxer   demuxer_;
  SslCtx    ssl_ctx_;

  EventMap  event_map_;

  CMtxLockGuard waiter_;
};

#endif

