
#include "Reactor.h"

#include "Client.h"
//#include "reactor/Connector.h"

#include "common/CStreamLogger.h"

#include <memory>
#include <iostream>

bool
Reactor::init()
{
  ILOG;

  if (demuxer_.init() == false)
  {
    ILOG << "Error io_handler init";
    return false;      
  }

  if(ssl_enable_ == false)
  {
    ILOG << "Not Used SSL"; 
    return true;
  }

  if (ssl_ctx_.init() == false)
  {
    ILOG << "Error ssl_ctx_ init";
    return false;   
  }

  return true;
}

/// make new client connection
void 
Reactor::request_connect(Client *ue)
{
  connector_.request_connect(ue);
}

/// add local event into demuxer
bool
Reactor::request_event(Client *ue_event)
{
  struct epoll_event &eve = ue_event->epoll_event();

  return demuxer_.event_register_client(eve.data.fd, eve);
}

int
Reactor::event_receive(EventQueue &reactor_event)
{
  if (connector_.event_receive(reactor_event))
  {
    int connection_count = 0;
    for (auto &iter : reactor_event)
    {
      std::shared_ptr<EpollEvent> event_ptr = iter.second;
      const int &fd = event_ptr.get()->epoll_eve.data.fd;
      
      std::shared_ptr<EventHandler> handler = std::make_shared<EventHandler>(event_ptr, ssl_enable_);
      
      EventProcessor::HandlerInit(*handler, &ssl_ctx_);

      event_map_[fd] = handler;
      
      // std::cout << "Received connect event. fd:"<< fd << std::endl;
      if (demuxer_.event_register_client(fd, handler->get_event()) == false)
      {
        ILOG << "ERROR event_register fd:" <<fd;
      }
      ++connection_count;
    }
    return connection_count;
  }
  demuxer_.event_receive(reactor_event, event_map_); // event_map_에서 events만 업데이트

  return 0;
}

void 
Reactor::run()
{
  while (is_run())
  {
    process();
  }
}

void
Reactor::process()
{
  EventQueue local_event = {};
  ActionType epoll_action;
  
  event_receive(local_event); 

  int client_fd;
  std::set<int> remove_fd;

  while(local_event.empty() == false) // 새로운 커넥션이 없는경우
  {
    remove_fd.clear();
    
    for (const auto &iter: local_event)
    {
      client_fd =  iter.second->epoll_eve.data.fd;
      EventHandler *EventHandler = event_map_[client_fd].get();

      if (react(EventHandler, epoll_action)==false)   // reacter 이벤트 실행
      {
        remove_fd.emplace(client_fd);                 // 이벤트처리 완료시 제거목록에 추가
      }
    
      struct epoll_event &event_ref = EventHandler->get_event();
      if (demuxer_.update(client_fd, event_ref, epoll_action) == false) // 이벤트 처리후 상태 저장,
      {
        ILOG << "ERROR demuxer_.update() fd:" << client_fd 
                                  << "action:"<< epoll_action;
        close(client_fd);  /// 반영 실패시 소켓 종료, TODO: Timer 연결하기
      }
    }

    for (const int &fd : remove_fd)
    {
      local_event.erase(fd);
    }
  }
}

bool
Reactor::react(EventHandler* handler, ActionType &result)
{
  bool remain = handler->process(result);
  
  if (result == ActionType::CTL_DONE && remain == false)  
  {
    event_map_.erase(handler->get_fd());      
  }

  return remain;
}

