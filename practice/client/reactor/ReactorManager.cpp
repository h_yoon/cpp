#include "ReactorManager.h"

#include "Reactor.h"
#include "Client.h"

ReactorManager::ReactorManager()
{

}

ReactorManager::~ReactorManager()
{

}

int
ReactorManager::init(StaticConfig *conf)
{
  std::string log_path;
  std::string log_filename;
  bool ssl_enable;

  static_conf = conf;

  if (static_conf->get("REACTOR_COUNT", reactor_count_) == false) 
    return 0;
  
  if (static_conf->get("MAX_CONNECTION", sock_max_connection_) == false) 
     return 0;
  
  if (static_conf->get("SSL_ENABLE", ssl_enable) == false) 
    return 0;

  if (reactors_init(ssl_enable) == false)
  {
    ILOG << "fail socketInit()" ;
    return 0;
  }

  return 1;
}

void 
ReactorManager::start()
{
  reactors_start();
  return;
}

bool
ReactorManager::reactors_init(const bool& ssl_enable)
{
  for (int i=0; i<reactor_count_; ++i)
  {
    std::shared_ptr<Reactor> reactor = std::make_shared<Reactor>(ssl_enable);
    
    if (reactor->init() == false)
      return false; 
    
    reactors_.push_back(reactor);
  }
  
  return true;
}

void
ReactorManager::reactors_start()
{
  for (const auto& reactor : reactors_)
  {
    reactor->start();
  }
}

void
ReactorManager::add_connect(Client *client)
{
  //ILOG;
  Reactor *reactor = select();
  
  reactor->request_connect(client);
}

bool
ReactorManager::add_event(Client *client)
{
  Reactor *reactor = select();

  return reactor->request_event(client);
}

Reactor*
ReactorManager::select()
{
  int idx = countor % reactor_count_;

  ++countor;

  return reactors_[idx].get();
}
