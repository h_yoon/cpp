// ReactorManager.cpp
// 2021.10.09 hsy

#ifndef H_ReactorManager_
#define H_ReactorManager_



// #include "common/CThread.h"
// #include "common/CLogger.h"
#include "common/CStreamLogger.h"
#include "common/CMtxLockGuard.h"
#include "common/StaticConfig.h"


#include <iostream>
#include <string.h> // for bzero
#include <stdio.h>
#include <arpa/inet.h> 
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <deque>
#include <atomic>
#include <memory>

class Reactor;
class Client;

class ReactorManager
{
public:
  ReactorManager(); 
  ~ReactorManager();
  
  int      init(StaticConfig *conf);   // create Socket
  void     start();
  
  void     add_connect(Client *client);
  bool     add_event  (Client *client); 

  StaticConfig *static_conf;
  
protected: 

private:
  Reactor* select();
  int      socketInit();
  bool     reactors_init(const bool& ssl_enable = true);
  void     reactors_start();

  int     sock_max_connection_ = 100;
  int     reactor_count_       = 1;
  
  CMtxLockGuard waiter_;
  std::deque<std::shared_ptr<Reactor>> reactors_;
  std::atomic<uint8_t>  countor;
  // Reactor  reactor_;

  struct sockaddr_in my_addr_;
};


#endif