// Client.h
// 2021.11.01 hsy

#ifndef H_Client__
#define H_Client__

#include "ClientManager.h"
#include "ServerInfoList.h"
#include "reactor/ReactorManager.h"

#include "engine/EpollEvent.h"
#include <string>

class ReactorManager;

class Client
{
public:
  Client(ReactorManager *reactors, ServerInfoList &targets) : target_servers(targets), reactors_(reactors) {}

  virtual ~Client() = default;

  bool init    (const size_t &eec_id);
  void request ();
  bool response();

  struct epoll_event& epoll_event() { return epoll_eve_;     }
  ServerInfo&         server_info() { return connect_server; }

  bool is_connect() { return connected_; }
  void set_connect(const bool &connect) { connected_ = connect; }

  bool process();
  
  ServerInfo      connect_server;
  ServerInfoList  target_servers;

private:
  void req_connect(const std::string &server_name); // request to connector
  bool send_event();                // request to reactor
  void close_connection();

  bool                connected_ = false;
  struct epoll_event  epoll_eve_;

  ReactorManager     *reactors_;

public:
  struct CustomAttr 
  {
    std::string access_token = "";
    std::string       eec_id = "";

    void set_eecid(const size_t &eec_id_part)
    {
      eec_id = "8210" + std::to_string(eec_id_part);
    }
    void set_access_token(const std::string &token)
    {
      access_token.assign(token);
    }
    std::string& get_eecid()
    {
      return eec_id;
    }
    std::string& get_access_token()
    {
      return access_token;
    }
  }custom_attr;
  
  enum class ServerName {
    ECS = 0,
    EES
  };
  enum class RequestType {
    WAIT = 0,
    REQUEST,
    RESPONSE
  };
  enum class RequestPhase {
    PROVISIONING = 0,
    REGISTRATION,
    DISCOVERY,
    SUBSCRIBE
  };
  struct RequestInfo {
    ServerName   target;
    RequestType  type;
    RequestPhase phase;
  }request_info;

  std::map<RequestPhase, ServerName> request_map = 
                  {{RequestPhase::PROVISIONING, ServerName::ECS},
                  {RequestPhase::REGISTRATION, ServerName::EES},
                  {RequestPhase::DISCOVERY,    ServerName::EES},
                  {RequestPhase::SUBSCRIBE,    ServerName::EES}};
};

#endif