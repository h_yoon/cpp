
#include "EventHandler.h"
#include "Client.h"
#include "CStreamLogger.h"
void
EventHandler::init()
{
}

/* return 
 *  true : queue에 유지,
 *  false: queue에서 제거후, epoll 재 등록
 */
bool
EventHandler::process(ActionType &epoll_action)
{
  bool remain = false;
  ActionType action;
  
  switch(handler_phase_)
  {
    case HandlerPhase::CONNECT:
      remain = handle_connect();
      break;
    case HandlerPhase::PROCESS:
      remain = handle_process();
      break;
    case HandlerPhase::WRITE:
      remain = handle_write();
      break;
    case HandlerPhase::READ:
      remain = handle_read();
      break;
    case HandlerPhase::CALLBACK:
      remain = handle_callback();
      break;
    case HandlerPhase::CLOSE:
      remain = handle_close();
      break;
    case HandlerPhase::SHUTDOWN:
      remain = handle_shutdown();
      break;
    default:
      break;
  }
  
  epoll_action = action_;
  // ILOG  << "phase-"            << handler_phase_ 
  //       << "keep_in_queue-"    << remain==true?"yes":"no"
  //       << "next_epoll_action-"<< epoll_action
  //       << "current_types-"    << types;
  return remain;
}

