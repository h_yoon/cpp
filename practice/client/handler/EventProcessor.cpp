// EventProcessor.cpp
// 2021.09.08 hsy

#include "EventProcessor.h"
// #include "EpollEvent.h"
#include "Client.h"

#include "SslCtx.h"
#include "CStreamLogger.h"
#include <memory>
#include <sys/socket.h> /// shutdown()

EventProcessor::EventProcessor(std::shared_ptr<EpollEvent> client_eve, const bool& ssl_enable)
{
  client_event_ = client_eve;

  if (ssl_enable == true)    
    eventHandlerInit = &EventProcessor::HttpsHandlerInit;
  else              
    eventHandlerInit = &EventProcessor::HttpHandlerInit;
}

void
EventProcessor::HttpHandlerInit(SslCtx *sslCtx)
{
    sslCtx = nullptr; // not used
    std::shared_ptr<SessionNoSSL> session_no_ssl;

    session_no_ssl = std::make_shared<SessionNoSSL>(get_fd());
    
    session_ = std::static_pointer_cast<Session>(session_no_ssl);
}

void
EventProcessor::HttpsHandlerInit(SslCtx *sslCtx)
{
    std::shared_ptr<SessionSSL> session_ssl;

    session_ssl =std::make_shared<SessionSSL>(get_fd());
    session_ssl->ctxSetter(sslCtx);

    session_ = std::static_pointer_cast<Session>(session_ssl);
}



/* return
 * true : 성공           action:
 * false: 실패 or 재시도
 */
bool
EventProcessor::handle_connect()
{
  if (session_->connect() == false)
    return false;

  action_ = ActionType::CTL_OUT;             //상태 변화없음
  handler_phase_ = HandlerPhase::PROCESS;   //연결 성공
  client_event_->client->set_connect(true);      
  return true;                              //true 큐 유지, 메지시 준비후 write 콜백 요청
}


bool
EventProcessor::handle_process() //입력에 대한 process와 callback에 대한 process가 별도로 필요할듯.
{
  bool result = session_->request_process(message_);

  if (result == true)
  {
    action_        = ActionType::CTL_OUT;    //출력상태로 변경
    handler_phase_ = HandlerPhase::WRITE;    //처리 성공
    return false;                            //queue제거
  }
  else
  {
    ILOG << "Error, Handle Processing";
    action_ = ActionType::CTL_WANT;   //처리 실패, 출력스트림 제거
    handler_phase_ = HandlerPhase::CLOSE; 
    return true;                   //queue 유지해서 handle_close로 전달
  }
}


bool
EventProcessor::handle_write()
{
  ILOG << "write msg:" << message_;
  int result = session_->write(message_);
  
  if (result > 0)
  {
    action_        = ActionType::CTL_IN;   //EPOLL 입력상태 변경
    handler_phase_ = HandlerPhase::READ;
    return false;                             //false: queue제거 epoll_ctl 수행
  }
  else if (result == 0)
  {
    action_ = ActionType::CTL_WANT; //재시도 필요, EPOLL 상태 변화없음
    return true;                   //true: queue 유지 
  }
  else
  {
    ILOG << "Write Error, Out delete";
    action_ = ActionType::CTL_WANT;   //쓰기 실패, 출력스트림 제거
    handler_phase_ = HandlerPhase::CLOSE; 
    return false;                   //false: queue제거 epoll_ctl 수행
  }
}


bool
EventProcessor::handle_read()
{
  //ILOG;
  int result = session_->read(message_);
  if (result > 0)
  {
    action_        = ActionType::CTL_WANT;    // EPOLL Shutdown
    handler_phase_ = HandlerPhase::CALLBACK;  // callback mode
    // ILOG << "response received!" << message; 
    return true;                               //false :queue 제거
  }
  else if (result == 0)
  {
    // ILOG << "read retry"; 
    action_ = ActionType::CTL_WANT; //EPOLL 상태 유지, 재시도
    return true;                   //queue 유지
  }
  else
  {
    // ILOG<< "read fail? result:" << result; 
    action_ = ActionType::DEL_ALL;   //리드 실패, 출력스트림 제거
    handler_phase_ = HandlerPhase::CLOSE; 
    return false;                  //queue 제거
  }
}

bool 
EventProcessor::handle_callback() 
{
  // ILOG;
  Client *client = client_event_.get()->client;
  client->process(); 

/// TODO connection reuse case
  // action_ = ActionType::CTL_OUT;   //쓰기 실패, 출력스트림 제거
  // handler_phase_ = HandlerPhase::WRITE; 

/// connection close case
  action_ = ActionType::CTL_WANT;   //쓰기 실패, 출력스트림 제거
  handler_phase_ = HandlerPhase::CLOSE; 

///
  return true;
}

bool
EventProcessor::handle_shutdown()
{
  ILOG;
  action_ = ActionType::CTL_RDHUP;
  handler_phase_ = HandlerPhase::CLOSE;
  ::shutdown(get_fd(),SHUT_RD);
  return false;                      //false: queue 제거
}

bool
EventProcessor::handle_close()
{
  // ILOG;
  session_->disconnect();         /// ssl 미종료시, close이후에 time_wait으로 socket이 유지됬었다.
  action_ = ActionType::DEL_ALL;  /// just close, epoll will automatically deregitser itself from epoll.

  return false;                   /// false: queue제거 epoll_ctl 제거
}

