// EventHandler.h
// 2021.09.08 hsy

#ifndef H_EventHandler_
#define H_EventHandler_

#include "EventProcessor.h"
#include "EpollEvent.h"
#include "CEpoll.h"

#include "CStreamLogger.h"

#include <utility>


class EventHandler : public EventProcessor
{
public:
  // using epollEvent = struct epoll_event;
  
  EventHandler(std::shared_ptr<EpollEvent> client_event, const bool& ssl_enable): EventProcessor(client_event, ssl_enable) {}
  // EventHandler(EpollEvent *client_event): EventProcessor(client_event->epoll_eve.data.fd, client_event)
  // {
  //   // fd          = client_event.epoll_eve.data.fd;
  //   // types       = client_event.epoll_eve.events;
  //   epoll_event = client_event->epoll_eve;
  // }
  // EventHandler(const struct epoll_event &event): EventProcessor(event.data.fd) // 상속 먼저 초기화
  //                               {fffff
  //                                 fd    = event.data.fd;
  //                                 types = event.events;
  //                                 client_event.epoll_eve = event;
  //                               }
  virtual ~EventHandler() = default;

  void init();
  // void update(const struct epoll_event &event) {
  //   // fd    = event.data.fd;
  //   // types = event.events;
  //   epoll_event = event;
  // }
  // struct epoll_event& get_event() { return epoll_event; }
  // int&                getFd()     { return fd;          }

  bool process(ActionType &result);
  
protected:
  int                fd;
  uint32_t           types;
  
  // struct epoll_event epoll_event;
};

#endif

