// EventProcessor.h
// 2021.09.08 hsy

#ifndef H_EventProcessor_
#define H_EventProcessor_

#include "EpollAction.h"
#include "EpollEvent.h"
#include "SessionSSL.h"
#include "SessionNoSSL.h"
#include <map>
#include <unistd.h>
#include <memory>

class SslCtx;
// class EpollEvent;

class EventProcessor
{

public:
  EventProcessor(std::shared_ptr<EpollEvent> client_eve, const bool& ssl_enable);
  virtual ~EventProcessor() = default;

  void  event_handler_init(SslCtx *ssl_ctx); 

  bool  handle_connect();
  bool  handle_read();
  bool  handle_write();
  bool  handle_process();
  bool  handle_timeout();
  bool  handle_shutdown();
  bool  handle_close();
  bool  handle_callback();

  void (EventProcessor::*eventHandlerInit)(SslCtx *sslCtx); // function pointer.
  static void HandlerInit(EventProcessor &proc, SslCtx *sslCtx) { (proc.*proc.eventHandlerInit)(sslCtx); }  // used case of function pointer

  std::shared_ptr<EpollEvent> getEvent()  { return client_event_; }

  void event_update(struct epoll_event &event) {
    client_event_.get()->event_update(event);
  }

  struct epoll_event& get_event() {
    return client_event_.get()->epoll_eve;
  }

  int get_fd() {
    return client_event_->fd_get();
    // return client_event_.get()->epoll_eve.data.fd;
  }

private:
  void  HttpHandlerInit (SslCtx *sslCtx = nullptr);
  void  HttpsHandlerInit(SslCtx *sslCtx);

protected:

  HandlerPhase  handler_phase_ = HandlerPhase::CONNECT;
  ActionType    action_;

  std::shared_ptr<EpollEvent>   client_event_;
  std::string   message_;

private:
  std::shared_ptr<Session> session_;
  // int   fd_sock_;
  bool  connected_;
};

#endif
