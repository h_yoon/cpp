
#ifndef H_ServerInfoList__
#define H_ServerInfoList__

#include <map>
#include <string>

struct ServerInfo
{
  ServerInfo() = default;
  ServerInfo(const std::string &_ip, const int &_port) : ip(_ip), port(_port) {}
  
  std::string   ip;
  int           port;
};

// struct ClientEvent
// {
//   ClientEvent(Client *_client, struct sockaddr_in &_addr) {
//     client = _client;
//     addr   = _addr;
//   }
//   Client             *client;
//   struct sockaddr_in  addr;
// };
class ServerInfoList
{
public:
  ServerInfoList() = default;
  virtual ~ServerInfoList() = default;
  
  bool  find(const std::string &name, ServerInfo &info) 
  {
    const auto &iter = list_.find(name);
    if (iter != list_.end())
    {
      info.ip   = iter->second.ip;
      info.port = iter->second.port;
      return true;
    }
    return false;
  }

  void emplace(const std::string &server_name, ServerInfo &&info) 
  {
    list_.emplace(server_name, info);  
  }

private:
  std::map<std::string, ServerInfo> list_;
};

#endif