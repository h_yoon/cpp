
// HttpProc.h
// 2021.08.22 hsy

#ifndef H_HttpProc_
#define H_HttpProc_

#include "common/CUtil.h"
#include <memory>
#include <string>
#include <iostream>
#include <map>

class HttpProc
{
public:
  HttpProc() = default;
  virtual ~HttpProc() = default;

  bool  streamEndCheck(const std::string &contents);

  // string end check
  // header check
  // get body

private:
  std::string contents_;

};

class HttpHeader
{
public:
  HttpHeader() = default;
  virtual ~HttpHeader() = default;

  std::string method;
  std::string http_version;

  std::multimap<std::string, std::string> header_contents;
};

class HttpRequest
{
public:
  HttpRequest() = default;
  virtual ~HttpRequest() = default;

  HttpHeader header;
  
};

class HttpResponse
{
public:
  HttpResponse() = default;
  virtual ~HttpResponse() = default;

  HttpHeader header;
};

///class Http2Parser {}
class Http1Parser
{
public:
  Http1Parser() = default;
  virtual ~Http1Parser() = default;

  static std::shared_ptr<HttpRequest> parse(const std::string &contents);
};

/**
-input header from ab tool
  POST / HTTP/1.0
  Content-length: 7
  Content-type: text/plain
  Content-Type: application/json
  Host: 192.168.14.6:10020
  User-Agent: ApacheBench/2.3
  Accept: *"\*
 * 
 */
std::shared_ptr<HttpRequest>
Http1Parser::parse(const std::string &contents)
{
  std::deque<std::string> header_lines = {};
  header_lines = CUtil::tokenizer(contents, '\n');
  
}

#endif