// Processor.h
// 2021.09.09 hsy

#ifndef H_Processor_
#define H_Processor_

#include <string>

class Processor
{
public:
  Processor() = default;
  virtual ~Processor() = default;

  bool process(std::string &message)
  {
    message = "good~~";
    return true;
  }
};

#endif