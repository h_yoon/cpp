
#include "CTokenizer.h"


#include <iostream>
#include <string>
#include <vector>


int main()
{
  CTokenizer tokenizer;

  std::vector<std::string> list;

  std::string input = "hi hello, my name is hsy, what is your hobby?";
  std::string delim = ",";
  list = tokenizer.get(input, delim);

  for (const auto &text : list)
    std::cout << text << std::endl;

return 0;
}
