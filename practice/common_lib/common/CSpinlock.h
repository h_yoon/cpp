// CSpinlock.h 
// 2021.08.24 hsy

#ifndef CSpinlock_H_
#define CSpinlock_H_

#include <atomic>
#include <mutex>

class CSpinlock {
	public:
		void Lock()
		{
			while (lock.test_and_set(std::memory_order_acquire)) {

			}
		}
		bool tryLock()
		{
			return !lock.test_and_set(std::memory_order_acquire);
		}
		void unLock()
		{
			lock.clear(std::memory_order_release);
		}

	private:
		std::atomic_flag lock = ATOMIC_FLAG_INIT;
};

#endif