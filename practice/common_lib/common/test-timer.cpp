
#include "ObjectTimer.h"

#include <unistd.h>
#include <iostream>
#include <functional>

class Session : public TimerObject
{
public:
	Session() = default;
	virtual ~Session() = default;
	void timeout() { std::cout << "bye" << std::endl; }
};

int main(void)
{
	Session *session; 
	session = new Session();

	// timer->add(500, session->timeout);
	gObjectTimer->add(5,(TimerObject*)session);
	while(true)
	{
		gObjectTimer->run();
		sleep(1);
		std::cout << "sleep" << std::endl;
	}
	return 0;
}


