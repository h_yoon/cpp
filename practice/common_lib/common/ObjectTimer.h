// ObjectTimer.h
// 2021.10.01 hsy

#ifndef _H_ObjectTimer
#define _H_ObjectTimer

#include "CMtxLockGuard.h"
#include "CSingletone.h"
#include <map>
#include <memory>
#include <ctime>


class TimerObject
{
public:
  TimerObject() = default;
  virtual ~TimerObject() = default;

  virtual void    timeout() = 0;
  time_t  expire_time;
};


/* 
 * timer 함수를 상속받으며, 지정된 시간이후에 timer함수 실행.
 */
class ObjectTimer : public CSingletone<ObjectTimer>
{
public:
  ObjectTimer() = default;
  virtual ~ObjectTimer() = default;

  int   add     (const time_t &timeout_sec, TimerObject *object);  // return key
  void  remove  (const int &key);

  void  run();

private:
  time_t calc_expire_time(const time_t &timeout_sec);  // 만료시간 계산
  bool   calc_time(const time_t &expire_time, time_t current_time = time(NULL));   // 주어진 시간이 지났는지 확인
  int    increase_key();

  int    key_;
  CMtxLockGuard mtx_;
  CMtxLockGuard mtx_key_;

  std::map<int, TimerObject*> time_objects_;
};

#define gObjectTimer ObjectTimer::ins()

#endif