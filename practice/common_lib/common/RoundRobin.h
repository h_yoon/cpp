// RoundRobin
// hsy 2021.03.06

// 특정 객체 T에 대해 RoundRobin을 수행
// T는 변수 'weight'(가중치)를 가지고 있어야한다.

// TODO: 
// lock 추가.

#include <iostream>
#include <vector>
#include <memory>

template <class T>
class RoundRobin 
{
public:
	RoundRobin() = default;
	~RoundRobin() = default;

	void insert(T data);
	T select_one();

private:
	int	 get_weight(T ptr) { return ptr->weight; }
	void next();
	int& idx() { return idx_; }
	void idx(const int &idx) { idx_ = idx; }

		
	std::vector<T> list_;
	int count_ = 0;
	int idx_    = 0;
	//TODO mutex add
};

template <class T>
void RoundRobin<T>::insert(T data) {
	list_.push_back(data);
}

template <class T>
T RoundRobin<T>::select_one() {
	
	T one = list_[idx()];
	++count_;
	// 현재 카운트와 T의 weight보다 크다면 다음 객체로 변경
	std::cout << "get_weight("<<idx()<<")" << get_weight(one) << std::endl; 
	if(count_ > get_weight(one)) {
		count_ = 0;
		next();	
	}

	return list_[idx()];
}

template <class T>
void RoundRobin<T>::next() {

	int index = idx();

	if ((index + 1) >= list_.size())
		index=0;
	else
		index++;

	idx(index);
}

//example
/*
class EAS
{
public:
	EAS(int idx, int weight) :index(idx), weight(weight) {}
	int index;
	int	weight;	
};


void sample(RoundRobin<std::shared_ptr<EAS>> *rr) {

	std::shared_ptr<EAS> ptr;
	for(int i=0; i<1000; ++i) {	
		ptr = std::make_shared<EAS>(i,10);
		rr->insert(ptr);		
	}
}

void test(RoundRobin<std::shared_ptr<EAS>> *rr) {
	
	std::shared_ptr<EAS> ptr;
	for (int i=0; i<100; ++i) {
		ptr = rr->select_one();	
		std::cout << i << " select " <<  ptr->index << std::endl;
	}
}

auto main() -> int 
{
	
	RoundRobin<std::shared_ptr<EAS>> rr;
	sample(&rr);
	test(&rr);
	return 0;
}
*/
