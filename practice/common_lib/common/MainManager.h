// MainManager.h
// 2021.10.15 hsy

#ifndef H_MainManager_
#define H_MainManager_

#include "CTpsManager.h"
#include "ObjectTimer.h"
#include "StaticConfig.h"
#include "CStreamLogger.h"

#include <time.h>

/// managing of tps, timer
class MainManager
{
public:
  MainManager() = default;
  virtual ~MainManager() = default;
  bool init();
  bool stop();
  void run();

  StaticConfig& getConfig() { return static_conf_; }
  
  bool is_run() { return running; }
  
private:
  bool logInit();

private:
  bool  running = true;
  CMtxLockGuard mtx_;
  StaticConfig static_conf_;
};

#endif