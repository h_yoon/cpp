

#include "CTpsManager.h"


CTpsManager::CTpsManager()
{
}

CTpsManager::~CTpsManager()
{
}

bool CTpsManager::register_calculater(CTpsCalculater &calculator)
{
  spin_.Lock();
  tls_tps_map_.emplace(std::this_thread::get_id(), calculator);
  spin_.unLock();
  
  return true;
}

void CTpsManager::run()
{
  size_t total_tps = 0;
  spin_.Lock();

  for ( auto &iter : tls_tps_map_)
    total_tps += iter.second.get();

  spin_.unLock();

  if (total_tps)
  	ILOG << "TPS:" << total_tps;
  
}
