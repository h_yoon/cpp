#include "CMtxLockGuard.h"

#include <iostream>

void
CMtxLockGuard::lock()
{
  mtx_.lock();
  //const std::lock_guard<std::mutex> lock(mtx_);
  return;
}

void
CMtxLockGuard::release()
{
  mtx_.unlock();
  return;
}

void
CMtxLockGuard::wait_ms(const time_t &wait_time)
{
  mtx_.try_lock_for(std::chrono::milliseconds(wait_time));

  return;
}