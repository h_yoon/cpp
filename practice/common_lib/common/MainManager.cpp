
#include "MainManager.h"

bool 
MainManager::init()
{
  if(static_conf_.init() == false)
    return false;

  return logInit();
}

bool
MainManager::logInit()
{
  std::string log_path;
  std::string log_filename;

  if (static_conf_.get("LOG_PATH", log_path) == false)
  {
    std::cout << "Init Fail, Cannot found LOG_PATH info" << std::endl;
    return false;
  }
  
  if (static_conf_.get("LOG_FILE", log_filename) == false)
  {
    std::cout << "Init Fail, Cannot found LOG_FILE info" << std::endl;
    return false;
  }  

  if (gCLogger->init(log_path, log_filename) == false)
  {
    std::cout << "Init Fail logger" << std::endl;
    return false;
  }

  gCLogger->start();

  return true;
}

bool
MainManager::stop()
{
  running = false; 
  return false;
}

void
MainManager::run()
{
  time_t current, last=0;

  while(running == true)
  {
    current = time(NULL);

    if (current != last)
    {
      last = current;
      
      gObjectTimer->run();
      gCTpsManager->run();
      
      mtx_.wait_ms(1000);
    }
  }
}
