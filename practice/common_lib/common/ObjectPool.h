 // ObjectPool.h
 // 2021.10.28  hsy

#ifndef H_ObjectPool__
#define H_ObjectPool__

#include "common/CSpinLock.h"

template <typename T, int pool_size>
class ObjectPool
{
public:
    ObjectPool();
    virtual ~ObjectPool();
    void  create_pool();

    T*    acquire();   // get empty object 
    //void  release();

private:
    T*    object_pool_[pool_size];
    int   max_size_ = pool_size;
    int   index_ = 0;

    CSpinLock spin_;
};

template <typename T, int pool_size>
ObjectPool<T, pool_size>::ObjectPool() 
{
  create_pool();
}

template <typename T, int pool_size>
ObjectPool<T, pool_size>::~ObjectPool() 
{
  for (int i=0; i<max_size_; ++i)
  {
    delete object_pool_[i];
  } 
}

template <typename T, int pool_size>
void ObjectPool<T, pool_size>::create_pool()
{
  for (int i=0; i<max_size_; ++i)
  {
    object_pool_[i] = new T();
  }
}

template <typename T, int pool_size>
T* ObjectPool<T, pool_size>::acquire()
{
  int idx;

  spin_.lock();
  if (index_ >= max_size_)
  {
    index_ = 0;
    idx = index_;
  }
  else
  {
    idx = index_;
    ++index_;
  }

  T* obj = object_pool_[idx];
  spin_.unlock();

  return obj;
}

#endif
