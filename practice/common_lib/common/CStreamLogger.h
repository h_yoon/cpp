// CStreamLogger.h
// 2021.09.18 hsy

#ifndef CStreamLogger_H_
#define CStreamLogger_H_

#include "CLogger.h"

#include <sstream>
#include <iomanip>
#include <string>
#include <iostream>

template <typename Logger>
class CStreamLogger
{
public:
  CStreamLogger(const std::string &file,
                const size_t      &line,
                const std::string &func)
  {
    std::stringstream buffer;
    buffer << " "
           << std::left
           << std::setw(FILE_LEN) 
           << file
           << std::right
           << std::setw(LINE_LEN) 
           << std::to_string(line)
           << " "
           << std::left 
           << std::setw(FUNC_LEN) 
           << func;
           
    header_ = buffer.str();
  }

  ~CStreamLogger() 
  {
    gCLogger->log(header_, msg_);
  }

  CStreamLogger& operator<<(const std::string &str) 
  { 
    msg_ = msg_ + " " + str;
    return *this;
  }

  CStreamLogger& operator<<(const int &number)
  {
    msg_ = msg_ + " " + std::to_string(number);
    return *this;
  }

private:
  std::string header_;
  std::string msg_;

  static size_t FILE_LEN; /// include null 
  static size_t LINE_LEN;
  static size_t FUNC_LEN;

};

template <typename Logger>
size_t CStreamLogger<Logger>::FILE_LEN = 16;
template <typename Logger>
size_t CStreamLogger<Logger>::LINE_LEN = 5;
template <typename Logger>
size_t CStreamLogger<Logger>::FUNC_LEN = 15;


#define ILOG CStreamLogger<CLogger>(__FILE__, __LINE__, __func__)

#endif