// TimeCounter.h
// 2022.03.29 hsy 

#include <chrono>
#include <iostream>

class TimeCounter {
public:
  TimeCounter() = default;
  virtual ~TimeCounter() = default;

  void start();
  void stop();
  int lab();
  int get();
  void print();

private:
  std::chrono::time_point<std::chrono::system_clock> start_;
  std::chrono::time_point<std::chrono::system_clock> end_;
  int period_;
};

inline void 
TimeCounter::start() {
  start_ = std::chrono::system_clock::now();
}

inline void
TimeCounter::stop() {
  end_ = std::chrono::system_clock::now();
}

inline int 
TimeCounter::lab() {
  std::chrono::time_point<std::chrono::system_clock> current = std::chrono::system_clock::now();
  return std::chrono::duration_cast<std::chrono::milliseconds>(current-start_).count();
}

inline int
TimeCounter::get() {
  period_ = std::chrono::duration_cast<std::chrono::milliseconds>(end_-start_).count();
  return period_;
}

inline void
TimeCounter::print() {
  std::cout << "time priod: " << get() << " ms" << std::endl;
}
