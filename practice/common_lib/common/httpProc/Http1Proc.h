// http1Proc.h
// 2022.08.19 hsy

// http1 parsing class

#ifndef _H_HTTP1PROC_
#define _H_HTTP1PROC_

#include "HttpInfo.h"

class Http1Proc
{
public:
  Http1Proc() = default;
  virtual ~Http1Proc() = default;

  bool HttpParse(const std::string &message, HttpInfo &httpInfo);
  
private:
  bool findHeader(const std::string &message);
  bool parseHeader(HttpInfo &httpInfo);
  
  static constexpr int MAX_HEADER_LEN = 1024;
  static constexpr int MAX_BODY_LEN = 4096;

  char header_[Http1Proc::MAX_HEADER_LEN] = {0};
  char body_[Http1Proc::MAX_BODY_LEN] = {0};
  
};
#endif // _H_HTTP1PROC_
