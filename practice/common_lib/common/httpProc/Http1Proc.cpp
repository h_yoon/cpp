
#include "Http1Proc.h"
#include "CUtil.h"
#include "CStreamLogger.h"

#include <string.h>

bool
Http1Proc::HttpParse(const std::string &message, HttpInfo &httpInfo)
{
  
  if(findHeader(message) == false)
    return false;

  if(parseHeader(httpInfo) == false)
    return false;

  return true;
}

bool
Http1Proc::findHeader(const std::string &message)
{
  size_t nIdx = 0;
  size_t nLen = message.length();

  if(nLen <= 4)
  {
    ILOG << "Error Parsing. too short message("<< nLen <<")\n"<< message;
    return false;
  }

  char *pMsg = (char*)(message.c_str());

  for(nIdx=0; nIdx<nLen-4; ++nIdx)
  {
    pMsg += 1;

    if(*pMsg=='\r' && *(pMsg+1)=='\n' && *(pMsg+2)=='\r' && *(pMsg+3)=='\n')  // find \r\n\r\n
    {
      if(nIdx > Http1Proc::MAX_HEADER_LEN)
      {
        ILOG << "Failed Parsing. header-length over limit";
        return false;
      }

      strncpy(header_, message.c_str(), nIdx); // copy header
      
      if(nLen-nIdx > Http1Proc::MAX_BODY_LEN)
      {
        ILOG << "Failed Parsing. body-length over limit";
        return false;
      }
      
      if(nIdx+5 >= nLen)
      {
        ILOG << "Failed Parsing No body("<<message<<")";
        return false;
      }

      strncpy(body_, message.c_str()+(nIdx+5), nLen-nIdx); // copy body
      return true;
    }
  }

  ILOG << "Cannot Found Header Last: '\\r\\n\\r\\n'";
  return false;
}

/* header example
  POST /test HTTP/1.1
  Host: 192.168.0.12:10020
  User-Agent: curl/7.68.0
  Accept: * / *
  Content-Length: 6
  Content-Type: application/x-www-form-urlencoded
*/
bool 
Http1Proc::parseHeader(HttpInfo &httpInfo)
{
  std::map<std::string, std::string> headers;
  std::deque<std::string> lines, headData;

  lines = CUtil::tokenizer(header_, "\n");
  headData = CUtil::tokenizer(lines[0], " ");

  httpInfo.method(headData[0]);
  httpInfo.url(headData[1]);
  httpInfo.data(body_);
  httpInfo.dataLength(strlen(body_));
  // TODO : Get http version, ip, port and length.
  
  std::deque<std::string>::iterator strIter = lines.begin()+1;
  std::string::size_type pLocate;

  for(strIter; strIter!=lines.end(); strIter++)
  {
    pLocate = strIter->find(":");
    if(pLocate == std::string::npos)
    {
      ILOG << "Parsing Fail, Incorrect Header(" << strIter->c_str() << ")";
      continue;
    }
    headers[strIter->substr(0, pLocate)] = strIter->substr(pLocate+1);
  }

  return true;
}
