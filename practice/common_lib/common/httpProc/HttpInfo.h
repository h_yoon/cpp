// http1.h
// 2022.08.19 hsy

// http1 info class 

#ifndef _H_HTTPINFO_
#define _H_HTTPINFO_

#include <string>

class HttpInfo
{
public:
  HttpInfo() = default;
  ~HttpInfo() = default;

  // getters
  std::string& method(void) { return method_; } 
  std::string& url() { return url_; }
  size_t&  dataLength() { return content_length_; }
  std::string& data() { return data_; }

  // setters
  void method(const std::string &method) { method_.assign(method); }
  void url(const std::string &url) { url_.assign(url); }
  void dataLength(const size_t &len) { content_length_ = len; }
  void data(const std::string &data) { data_.assign(data); }

private:
  std::string http_version_;
  std::string ip_;
  int port_;
  std::string method_;
  std::string url_;
  std::string data_;
  size_t content_length_ = 0;
};

#endif // _H_HTTPINFO_