// CTpsCalculater.h
// 2021.10.12 hsy

#ifndef H_CTpsCalculater_
#define H_CTpsCalculater_

#include "CSpinlock.h"

#include <thread>
#include <memory>
#include <map>

class CTpsCalculater
{
  using tpsMap = std::map<size_t, size_t, std::greater<size_t> >;
public:
  CTpsCalculater();
  virtual ~CTpsCalculater();

  bool    init_tps();
  void    add();
  size_t  get();
private:
  size_t get_hundred_ms(const size_t &pre_time = 0);
  tpsMap tps_map_;

  CSpinlock spin_;
};

#endif

