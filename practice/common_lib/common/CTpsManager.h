// CTpsCalcurator.h
// 2021.10.14 hsy

#ifndef H_CTpsManager_
#define H_CTpsManager_

#include "CTpsCalculater.h"

#include "CSingletone.h"
#include "CThread.h"
#include "CStreamLogger.h"

#include <memory>


class CTpsManager : public CSingletone<CTpsManager>
{
public:
  CTpsManager();
  virtual ~CTpsManager();

  bool register_calculater(CTpsCalculater &calculater);

  virtual void run();

private:

  std::map<std::thread::id, CTpsCalculater&> tls_tps_map_;
  CSpinlock spin_;
};

#define gCTpsManager CTpsManager::ins()

#endif
