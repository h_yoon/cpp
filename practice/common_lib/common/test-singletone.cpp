
#include <iostream>
#include <thread>
#include <unistd.h>

#include "CSingletone.h"


#define gtest Test::ins()

bool is_run = true;
class Test : public CSingletone<Test>
{
public:
  Test() = default;
  virtual ~Test() = default;

  void print() {
	std::cout << "test" << std::endl;
  }
};

static
void call()
{
   std::cout << "start:" << std::this_thread::get_id() << std::endl;
   while(is_run)
	gtest->print();
}

int main()
{

  std::thread thr1(call);
  std::thread thr2(call);
  std::thread thr3(call);
  std::thread thr4(call);
  std::thread thr5(call);


  usleep(10000000);
  is_run = false;
  thr1.join();
  thr2.join();
  thr3.join();
  thr4.join();
  thr5.join();
 std::cout  << "end" << std::endl;
}



	
