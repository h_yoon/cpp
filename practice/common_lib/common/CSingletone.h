/*
 * Singletone.h
 *
 *  Created on: 2021. 6. 1.
 *      Author: hsyoon
 */

#ifndef H_CSingletone_
#define H_CSingletone_

#include <mutex>

template <typename T>
class CSingletone
{
public:
	virtual ~CSingletone() {};

	static T* get()
	{
		return instance_;
	}

	static T* ins()
	{
		std::call_once(flag_, create_once);
		return instance_;
	}

	static void create_once() 
	{
		instance_ = new T;
	}

private:
	static std::once_flag flag_;
	static T* instance_;
	static std::mutex mtx_;

};

template <typename T>
T* CSingletone<T>::instance_ = nullptr;

template <typename T>
std::mutex CSingletone<T>::mtx_;

template <typename T>
std::once_flag CSingletone<T>::flag_;

#endif