// CThread.h
// 2021.07.15

#ifndef __CTHREAD__
#define __CTHREAD__

#include <iostream>

void* launch(void *arg); // 포인터 함수

class CThread
{
public:
  CThread() {};
  virtual ~CThread() {};

  void start();
  void stop();
  void join();
  bool is_run();
  virtual void run() = 0;
  
private:
  bool        is_run_   = false;
  pthread_t   thread_;

};

#endif