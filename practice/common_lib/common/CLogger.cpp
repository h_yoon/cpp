


#include "CLogger.h"
#include "CUtil.h"
#include <iostream>
#include <unistd.h>
#include <ctime> /// clock_t

bool
CLogger::init(const std::string &path, const std::string &filename, const Level &level)
{
  log_level_ = level;
  
  file_path_name_ = path + "/" + filename;
  file_.open(file_path_name_, std::ofstream::out | std::ofstream::app);

  if (file_.is_open() == true)
  {
    log(" ", "---- log open ---");
    return true;
  }
  else
  {
    std::cout << "log open failed" << std::endl;
  }

  return false;
}

void
CLogger::file_write(const std::string &msg)
{
  std::string current = CUtil::strftime_ms();

  if(file_.is_open()==false)
    file_.open(file_path_name_, std::ofstream::out | std::ofstream::app);

  file_ << CUtil::strftime_ms()<< msg << std::endl;

  file_.close();
}

void
CLogger::log(const std::string &head, const std::string &msg, const std::thread::id &tid)
{
  spin_.Lock();
  log_queue_.push_back(head + msg);
  spin_.unLock();
}

void
CLogger::run()
{
  std::multimap<clock_t, std::string> entire_buffer;
  std::deque<std::string> buffer;
  size_t count = 0;
  
  while(CThread::is_run())
  {
    buffer.clear();

    spin_.Lock();
    buffer.swap(log_queue_);
    spin_.unLock();

    for (const auto &iter : buffer)
    {
      file_write(iter);
    }
    
    count += buffer.size();
    
    if (count == 0) 
    {
      usleep(5000);
    }
    else
    {
      entire_buffer.clear();
      usleep(10);
    }
    count = 0;
  }
}

