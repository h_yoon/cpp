
#include "ObjectTimer.h"

#include <set>

int ObjectTimer::increase_key()
{
  int key;
  mtx_key_.lock();
  {
  key = ++key_;
  }
  mtx_key_.release();
  
  return key;
}


int ObjectTimer::add(const time_t &timeout_sec, TimerObject *object)
{
  int key;
  mtx_.lock();
  {
  object->expire_time = calc_expire_time(timeout_sec);
  key = increase_key(); 
  time_objects_.emplace(key, object);
  }
  mtx_.release();

  return key;
}

void ObjectTimer::remove(const int &key)
{
  mtx_.lock();
  {
  time_objects_.erase(key);
  }
  mtx_.release();
}

time_t ObjectTimer::calc_expire_time(const time_t &timeout_sec)
{
  return time(NULL) + timeout_sec;
}

bool ObjectTimer::calc_time(const time_t &expire_time, time_t current_time)
{
  if (expire_time <= current_time)
    return true;

  return false;
}

void ObjectTimer::run()
{
  mtx_.lock();
  time_t current_time = time(NULL);

  std::set<int> timeout_keys;
  for (const auto &object : time_objects_)
  {
    if (object.first == 0) 
      continue;

    TimerObject *target = object.second;
    if (calc_time(target->expire_time, current_time) == true)
    {
      target->timeout();
      timeout_keys.insert(object.first);
    }
  }
  for (const int &key : timeout_keys) {
    time_objects_.erase(key);
  }

  mtx_.release();
}
