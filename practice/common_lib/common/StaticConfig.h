//StaticConfig.h
// 2021.10.08 hsy

#ifndef H_StaticConfig_
#define H_StaticConfig_

#include "CUtil.h"

#include <iostream>
#include <string>
#include <map>

class StaticConfig
{
public:
  StaticConfig() = default;
  virtual ~StaticConfig() = default;
  bool init();

  bool get(const std::string &attr, std::string &value);
  bool get(const std::string &attr, int &value);
  bool get(const std::string &attr, bool &value);
  
  std::string get(const std::string &attr);
  
private:
  std::map<std::string,std::string> config_;
};

#endif
