// CUtil.h
// 2021.09.16 hsy

#ifndef CUtil_H_
#define CUtil_H_


#include <time.h>       /// strftime, struct tm
#include <sys/timeb.h>  /// ftime(),  struct timeb
#include <sys/stat.h>   /// stat
#include <sstream>
#include <deque>
#include <iostream>

class CUtil
{
public:
  static std::string strftime(const time_t &current_time, const std::string &format = "%Y%m%d-%H%M");
  static std::string strftime_ms(const std::string &format = "%H:%M:%S.");

  template <typename T> static std::deque<std::string>
  tokenizer(const std::string &str, const T &delimiter);

  static bool  is_file_exist(const std::string &file_path_name); // full path and name
};

inline std::string 
CUtil::strftime(const time_t &current_time, const std::string &format)
{
  struct tm *timeinfo;
  char buff[64] = {0,};

  timeinfo = localtime(&current_time);

  ::strftime(buff,64,format.c_str(),timeinfo);
  return buff;
}

inline std::string 
CUtil::strftime_ms(const std::string &format)
{
  struct timeb tb;
  struct tm    timeinfo;
  char buff[64] = {0,};
  
  std::ostringstream oss;
  ftime(&tb);
  localtime_r(&tb.time, &timeinfo);

  ::strftime(buff, 64, format.c_str(), &timeinfo);

  oss << buff << tb.millitm;
  return oss.str();
}

template <typename T> inline std::deque<std::string> 
CUtil::tokenizer(const std::string &_str, const T &delimiter)
{
  std::string::size_type length = _str.length();
  std::string str               = _str;
  std::deque<std::string> texts = {};
  
  std::string::size_type start  = 0;  /// start point
  std::string::size_type end    = 0;  /// result of str.find()

  while (end<length)
  {
    std::string buff = (str.c_str() + start);  /// important -> string starts at 'start'

    end = buff.find(delimiter);
    if (end == std::string::npos)
      return texts;

    texts.emplace_back(str.substr(start,end));
    
    end++;
    start += end; /// important -> start = start + end;
  }
  return texts;
}

inline bool
CUtil::is_file_exist(const std::string &file_path_name)
{
  if (file_path_name.length() == 0)
    return false;

  struct stat buffer;
  return (stat(file_path_name.c_str(), &buffer) == 0);
}
#endif