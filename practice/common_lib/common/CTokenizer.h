//tokenizer.h
// 2021.07.14 hsy

#ifndef H_CTokenizer_
#define H_CTokenizer_

#include <iostream>
#include <vector>

class CTokenizer
{
public:
  CTokenizer() = default;
  virtual ~CTokenizer() = default;

  template <typename T>
  bool get(T input, T delim, std::vector<T> &result);
};


template <typename T> bool
CTokenizer::get(T input, T delim, std::vector<T> &result)
{
  std::vector<T> list;

  // if string
  size_t total_len = input.size();
  size_t start = 0;
  size_t before = 0;
  T temp = input;
  std::string buff;

  while(start < total_len )
  {
    before += start;
    temp = input.c_str() + before;
    start = temp.find(delim);
    if (start >= 0)
    {
      buff = temp.substr(0, start);
      before++;
      result.push_back(buff);
    }
  }
  return result.size();
}

#endif
