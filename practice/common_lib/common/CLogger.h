// CLogger.h
// 2021.09.15 hsy


#ifndef H_CLogger_
#define H_CLogger_

#include "CThread.h"
#include "CSingletone.h"
#include "CSpinlock.h"

#include <thread>
#include <fstream>
#include <map>
#include <deque>
#include <ctime>

class CLogger : public CSingletone<CLogger>, 
                public CThread
{
public:
  enum Level{
    CRITICAL=0,
    ERROR,
    INFO,
    WARNNING,
    DEBUG    
  };

public:
  CLogger() = default;
  virtual ~CLogger() = default;
  
  bool init(const std::string &path, const std::string &filename,  const Level &level = Level::INFO);
  void log(const std::string &head, const std::string &msg, const std::thread::id &tid = std::this_thread::get_id());


protected:
  virtual void run();

private:
  void file_write(const std::string &message);

private:
  std::string file_path_name_;
  Level       log_level_    = Level::INFO;
  std::deque<std::string> log_queue_;
  std::ofstream  file_;
  std::string    head_;
  CSpinlock spin_;

};

#define gCLogger CLogger::ins()

#endif
