
#include "CThread.h"
#include "CStreamLogger.h"

void* launch(void *arg) // 포인터 함수
{
  void *ret = nullptr;
 CThread *obj = (CThread*)arg;
 obj->run(); 
 return ret;
}

bool
CThread::is_run()
{
  return is_run_;
}

void
CThread::start()
{
  is_run_ = true;
  if ( pthread_create(&thread_, NULL, launch, (void*)this) < 0 )
  {
    perror("error pthread creating:");
    ILOG << "error pthread creating:";
    is_run_ = false;
    return;
  }
  else 
  {
   ILOG << "start";
  }
  return;
}
void
CThread::stop()
{
  
  if ( is_run_ == true )
  {
    is_run_ = false;
    int status = 0;
    ILOG << "end thread" << status; 
    join();
    
  } 
  else
  {
    ILOG << "thread not running";
  }
  return;
}

void
CThread::join()
{
  int status = 0;
  pthread_join(thread_, (void**)&status);
}
