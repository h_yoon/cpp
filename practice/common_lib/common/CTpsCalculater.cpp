

#include "CTpsCalculater.h"
#include "CStreamLogger.h"

CTpsCalculater::CTpsCalculater()
{

}

CTpsCalculater::~CTpsCalculater()
{
 
}

bool
CTpsCalculater::init_tps()
{
  return true;
}

size_t
CTpsCalculater::get_hundred_ms(const size_t &pre_time)
{
  std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
  return ((ms.count()) - pre_time);
}

void
CTpsCalculater::add()
{
  size_t hundred_ms = get_hundred_ms(); 
  spin_.Lock();
  tps_map_[hundred_ms] += 1;
  spin_.unLock();
}

size_t
CTpsCalculater::get()
{
  size_t tps = 0;
  size_t start_ms = get_hundred_ms(10);  /// -100 ms
  size_t end_ms   = start_ms - (1000);   /// -1100 ms 
  
  spin_.Lock();
  auto iter_start = tps_map_.lower_bound(start_ms);
  auto iter_end   = tps_map_.lower_bound(end_ms);

  for ( auto iter=iter_start; iter!=iter_end; ++iter) 
  {
    tps += iter->second;
  }

  for (auto iter=iter_end; iter!=tps_map_.end(); )
  {
   tps_map_.erase(iter++);
  }
  
  spin_.unLock();
  return tps;
}

