// BlockingDequeThread.h 
// 2022.05.14 hsy

#ifndef H_BlockingDequeThread_
#define H_BlockingDequeThread_

#include <mutex>
#include <thread>
#include <deque>
#include <condition_variable>


template <typename T>
class BlockingDequeThread : public CThread
{
public:
  BlockingDeque() = default;
  ~BlockingDeque() = default;
  
  void    push(T &t);
  void    push(T *t); /// if possible

protected:
  virtual void    run();

private:
  size_t  pop();
  void    process();

  std::condition_variable cv_;
  std::mutex    qmtx_;
  std::mutex    cmtx_;
  std::deque<T> queue_;
  std::deque<T> buffer_;

  bool  waiting_ = false;
};

template <typename T>
void BlockingDequeThread<T>::push(T &t) 
{
  const std::lock_guard<std::mutex> lock(qmtx_);
  {
    queue_.emplace_back(t);
  }

  if(waiting_) {
    cv_.notify_one();
  }
}

template <typename T>
size_t BlockingDequeThread<T>::pop() 
{
  const std::lock_guard<std::mutex> lock(qmtx_);
  {
    buffer_.clear();
    queue_.swap(buffer_);
    return buffer_.size();
  }
}
template <typename T>
void BlockingDequeThread<T>::process() 
{
  for (const T t& : buffer_) {
    t.process();
  }
}

template <typename T>
void BlockingDequeThread<T>::run() {

  size_t cnt;

  while(::is_run()) 
  {
    if (queue_.empty() == true) 
    {
      std::unique_lock<std::mutex> lk(cmtx_);
      waiting_ = true;
      cv.wait(lk);

      waiting_ = false;
      cnt = pop();
      lk.unlock();
    }
    else 
    {
      wating_ = false;
      cnt = pop();
    }
    
    if(cnt) 
    {
      process();
    }
  }
  return;
}

#endif