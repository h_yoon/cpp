
#include <map>
#include <iostream>


int main()
{

	std::map<int, int, std::greater<int> > time_map;

	for (int i=0; i<100; ++i)
	{
		time_map[i*10] += 1; 
	}

	
	for (std::map<int,int,std::greater<int> >::iterator value = time_map.begin(); value != time_map.end(); value++)
		std::cout << value->first << std::endl;

	const std::map<int, int,std::greater<int> >::iterator &liter = time_map.lower_bound(66);
	const std::map<int, int,std::greater<int> >::iterator &uiter = time_map.upper_bound(66);

	std::cout << liter->first << " " <<  uiter->first << std::endl;
}


