

#include <iostream>
#include <chrono>

int main()
{

	time_t future = time(NULL)+1;
	time_t in_sec = time(NULL);
	size_t count = 0;
	std::chrono::milliseconds ms;
	while(true)
	{

		if (future == in_sec)
		{
			ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
			std::cout << ms.count()/100<< std::endl;
			future++;
		}
		in_sec= time(NULL);
	}
}

	
