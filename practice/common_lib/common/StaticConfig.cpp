
#include "StaticConfig.h"
#include <fstream>



bool
StaticConfig::init()
{
  
  char *env_result = getenv("STATIC_CONFIG");
  std::string file_path_name;
  if(env_result) 
  {
    std::cout << "config print " << env_result << std::endl;
    file_path_name = env_result;
  }
  else
  {
    std::cout << __FILE__ << " " << __LINE__ << " ERROR static file getenv() " <<  std::endl;
  }

  if (CUtil::is_file_exist(file_path_name) == false)
  {
    std::cout << __FILE__ << " " << __LINE__ << " static file not exist. file: " << file_path_name << std::endl;
    return false;
  }
  
  std::ifstream config_file(file_path_name.c_str(), std::ifstream::in);

  std::string line;
  std::string::size_type pos;

  while (getline(config_file, line)) 
  {
    pos = line.find("=");
    if(pos != std::string::npos)
    {
      config_.emplace(line.substr(0,pos), line.substr(pos+1,line.length()));
    }
    std::cout << line << std::endl;
  }
  std::cout << " " << std::endl;
  config_file.close();
  return true;
}

bool
StaticConfig::get(const std::string &attr, bool &value)
{
  const auto &iter = config_.find(attr);
  if (iter == config_.end())
    return false;
  
  std::string config_value = iter->second;

  if(config_value.length() < 4)
    return false;
    
  if(config_value.compare("true") == 0 || config_value.compare("TRUE") == 0)
    value = true;
  else if(config_value.compare("false") == 0 || config_value.compare("FALSE") == 0)
    value = false;
  else
    return false;

  return true;
}

bool
StaticConfig::get(const std::string &attr, int &value)
{
  const auto &iter = config_.find(attr);
  if (iter == config_.end())
    return false;
  else 
    value = std::stoi(iter->second);

  return true;
}

bool
StaticConfig::get(const std::string &attr, std::string &value)
{
  const auto &iter = config_.find(attr);
  if (iter == config_.end())
  {
    std::cout << __FILE__ << " " << __LINE__ << " " << "Cannot found "<< attr << " in static config" << std::endl;
    return false;
  }
  else 
  {
    value = iter->second;
    std::cout << __FILE__ << " " << __LINE__ << " " << "Config " << attr << ":" << value << std::endl;
  }

  return true;
}

std::string
StaticConfig::get(const std::string &attr)
{
  std::string value;
  if (get(attr,value) == true)
  {
    return value;
  }
  return "empty";
}