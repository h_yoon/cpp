// CMtxLockGuard.h
// 2021.08.13 hsy

#ifndef H_CMTXLOCK
#define H_CMTXLOCK

#include <thread>
#include <mutex>
#include <chrono>

class CMtxLockGuard
{
public:
    CMtxLockGuard() = default;
    virtual ~CMtxLockGuard() {
        release();
    }

    void lock();
    void release();
    void wait_ms(const time_t &wait_time);

private:
    std::timed_mutex mtx_;
};

#endif